#!/usr/bin/env ruby
# -*- coding: utf-8 -*-

require "#{File.dirname(__FILE__)}/lib/rodeo.rb"

dbname = File.basename(File.dirname(File.expand_path(__FILE__)))
sql = Rodeo::SQL.new(dbname,nil)
rodeo = Rodeo.new(__FILE__,nil,sql)
rodeo.main(nil,nil)

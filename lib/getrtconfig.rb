# -*- coding: utf-8 -*-

require 'net/telnet'
require 'timeout'
require 'pty'

class PTYssh
  #
  def initialize(addr,port)
    timeout(30) {|i|
      PTY.spawn("ssh -p #{port} #{addr}") {|r,w,pid|
        @input_stream = r
        @output_stream = w
        @child_pid = pid
        #PTY.protect_signal {
          yield(self)
        #}
      }
    }
  end
  #
  def waitfor(pat,str = nil)
    res = ""
    done = false
    while(not done)
      timeout(30) {|i|
        c = @input_stream.getc().chr
        yield(c) if(block_given?)
        res << c
        if(res =~ pat)
          cmd(str) if(str)
          done = true
        end
      }
    end
  end
  #
  def cmd(c)
    @output_stream.puts(c)
  end
end

def getconfig_telnet()
  config = ""
  begin
    su = Iconv.new("UTF8","CP932")
    telnet = Net::Telnet.new("Host"=>ARGV[0])
    telnet.waitfor(/Password[: ]*\z/n)
    telnet.cmd("String"=>ARGV[1],"Match"=>/\>[ ]*\z/n)
    telnet.cmd("String"=>"console character ascii","Match"=>/\>[ ]*\z/n)
    telnet.cmd("String"=>"console lines infinity","Match"=>/\>[ ]*\z/n)
    telnet.cmd("String"=>"console columns 200","Match"=>/\>[ ]*\z/n)
    telnet.cmd("String"=>"administrator","Match"=>/Password[: ]*\z/n)
    telnet.cmd("String"=>ARGV[2],"Match"=>/\#[ ]*\z/n)
    telnet.cmd("String"=>"show config","Match"=>/\#[ ]*\z/n) {|c| config << c }
    telnet.close
    config.sub!(/^show config.*?#/m,"")
    config.sub!(/^\#\s$/m,"")
    config = "#"+config
    config = su.iconv(config)
  rescue Exception
    config = nil
  end
  config
end

def getconfig_ssh()
  config = ""
  begin
    PTYssh.new(ARGV[0],5024) {|ssh|
      ssh.waitfor(/password:/,"a-shigi") #{|c| print c }
      ssh.waitfor(/>\s/,"console lines infinity") #{|c| print c }
      ssh.waitfor(/>\s/,"administrator") #{|c| print c }
      ssh.waitfor(/Password:/,"0559772799") #{|c| print c }
      ssh.waitfor(/#\s/,"show config") #{|c| print c }
      ssh.waitfor(/#\s/,"exit") #{|c| print c }
      ssh.waitfor(/>\s/,"exit") {|c| config << c }
      ssh.waitfor(/closed\./) #{|c| print c }
    }
  rescue Exception
    config = nil
  end
  config
end

config = getconfig_ssh()
if(config == nil or config == "")
  config = getconfig_telnet()
end
print config

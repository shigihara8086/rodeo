# -*- coding: utf-8 -*-

require 'stringio'
require 'tempfile'
require 'cgi'
require 'pp'

class RFobject
end

if(RUBY_VERSION < "1.9.0")
  class String
    def intern
      self
    end
  end
end

require "#{File.dirname(__FILE__)}/table.rb"
require "#{File.dirname(__FILE__)}/eruby.rb"

class Iconv
	def initialize(incode,outcode)
  end
  def iconv(data)
  	data
  end
end

class Rodeo
  attr :appname
  attr :db
  attr :sql
  attr :cgi
  attr :cgidata
  attr :line_height
  attr :offset
  attr :flist
  attr :flabel
  attr :user
  attr_accessor :cookie
  attr :user_record
  attr :admin
  attr_accessor :sort
  attr_accessor :sort_dir
  attr_accessor :where
  attr_accessor :session
  attr_accessor :table
  attr_accessor :field
  attr_accessor :roid
  attr :elems
  attr_accessor :fakeuser
  # 初期化
  def initialize(app,rdb=nil,exsql=nil)
    @appname = File.basename(app)
    @db = File.basename(File.dirname(File.expand_path(app)))
    @db = rdb if(rdb)
    @sql = exsql
    @user = nil
    @table = nil
    @session = nil
    @line_height = 17
    @where = nil
    @sort = "roid"
    @sort_dir = "asc"
    @offset = 0
    @records = 0
    @roid = 0
    @field = nil
    @flist = {}
    @flabel = {}
    @tips = 0
    @cookie = nil
    @user_record = nil
    @admin = false
    @@outbuf = ""
    @cgi = {}
    @elems = 10
    @fakeuser = nil
    # フィールド型の読み込み
    Dir.glob("#{File.dirname(__FILE__)}/field/*.rb").sort.each {|file|
      require file.untaint
      name = File.basename(file).gsub(/^[\d.-]+/,"").sub(/\.rb$/,"")
      index = File.basename(file).to_i - 2
      begin
        if(index >= 0)
          @flist[index] = {} unless(@flist[index])
          @flist[index]["RF"+name] = instance_eval("RF#{name}.name()".untaint)
        end
      rescue Exception
        print "Content-Type: text/plain\n\n"
        print "#{file}:#{name}\n"
        print $!.message
      end
    }
    # フィールド型ラベルの読み込み
    Dir.glob("#{File.dirname(__FILE__)}/field/*").sort.each {|file|
      name = File.basename(file)
      index = File.basename(file).to_i - 2
      if(name =~ /^(\d+)\.\-+(.*)$/)
        if(index >= 0)
          @flabel[index] = $2
        end
      end
    }
    @sql = Rodeo::SQL.new(@db) unless(@sql)
    # ユーザテーブルを開く(初期ユーザをwebに設定)
    @user_record = table_new("ユーザ")
    @user_record.find("アカウント_0","web")
    jfazzy = false
    @sql.exec(%Q(select proname from pg_proc where proname = 'jfazzy')) {|res|
      jfazzy = true if(res.num_tuples > 0)
    }
    if(not jfazzy)
      # 曖昧検索関数の定義
      func = %Q[CREATE or replace FUNCTION jfazzy (text) RETURNS text AS ' SELECT translate(translate(translate(translate(translate(upper($1),]
      func << %Q[''ａｂｃｄｅｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚ'',]
      func << %Q[''ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ''),]
      func << %Q[''０１２３４５６７８９ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ／ー！＃＄％＆（）?＝＾｜＠‘［］｛｝：＊；＋、＜．＞？＿?－‐～☆・　'',]
      func << %Q[''0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ/-!#\\$%&\\(\\)-=^|@`\\[\\]{}:*;+,<.>?_----__ ''),]
      func << %Q[''ｧｱｨｲｩｳｪｴｫｵｶｷｸｹｺｻｼｽｾｿﾀﾁｯﾂﾃﾄﾅﾆﾇﾈﾉﾊﾋﾌﾍﾎﾏﾐﾑﾒﾓｬﾔｭﾕｮﾖﾗﾘﾙﾚﾛﾜｦﾝ'',]
      func << %Q[''ァアィイゥウェエォオカキクケコサシスセソタチッツテトナニヌネノハヒフヘホマミムメモャヤュユョヨラリルレロワヲン''),]
      func << %Q[''あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやゆよらりるれろわんをがぎぐげござじずぜぞだぢづでどばびぶべぼぱぴぷぺぽぁぃぅぇぉっゃゅょ'',]
      func << %Q[''アイウエオカキクケコサシスセソタチツテトナニヌネノハヒフヘホマミムメモヤユヨラリルレロワンヲガギグゲゴザジズゼゾダヂヅデドバビブベボパピプペポァィゥェォッャュョ''),]
      func << %Q[''ァィゥェォッャュョー-_'',]
      func << %Q[''アイウエオツヤユヨ--'')]
      func << %Q[' LANGUAGE sql]
      @sql.exec(func)
    end
  end
  # CGIデータ取り込み
  def get_cgi_data(cgi,code)
    case code
    when "KTAI"
      @kcode = "KTAI"
      @ic = Iconv.new("UTF8","CP932")
      @oc = Iconv.new("CP932","UTF8")
    when "SJIS"
      @kcode = "Shift_JIS"
      @ic = Iconv.new("UTF8","CP932")
      @oc = Iconv.new("CP932","UTF8")
    when "EUC"
      @kcode = "EUC-JP"
      @ic = Iconv.new("UTF8","EUC-JP")
      @oc = Iconv.new("EUC-JP","UTF8")
    when "JIS"
      @kcode = "JIS"
      @ic = Iconv.new("UTF8","JIS")
      @oc = Iconv.new("JIS","UTF8")
    else
      @kcode = "UTF-8"
      @ic = Iconv.new("UTF8","UTF8")
      @oc = Iconv.new("UTF8","UTF8")
    end
    @cgidata = cgi
    @cgidata = CGI.new() if(cgi == nil)
    @cgidata.params.each {|k,v|
	    if(v[0].kind_of?(StringIO) or
	      v[0].kind_of?(File) or
	      v[0].kind_of?(Tempfile))
	      if(k =~ /_$/)
	      		# kはASCII-8BITエンコードのまま
	          @cgi[k] = v[0]
	      else
	        if(code)
	          @cgi[@ic.iconv(k).encode(__ENCODING__)] = @ic.iconv(v[0].read()).encode(__ENCODING__)
	        else
	          @cgi[k.encode(__ENCODING__)] = v[0].read().encode(__ENCODING__)
	        end
	      end
	    else
	      if(code)
	        @cgi[@ic.iconv(k).encode(__ENCODING__)] = @ic.iconv(v[0]).encode(__ENCODING__)
	      else
	        @cgi[k.encode(__ENCODING__)] = v[0].encode(__ENCODING__)
	      end
	    end
    }
    # 動作パラメータの設定
    @user = nil
    if(@cgidata.cookies['user'] != [])
      @user = @cgidata.cookies['user'].value[0]
    end
    @table = @cgi['table']                 if(html_valid?("table") and @cgi['table'] != "")
    @session = @cgi['session']             if(html_valid?("session") and @cgi['session'] != "")
    @where = @cgi['where']                 if(html_valid?("where") and @cgi['where'] != "")
    @sort = @cgi['sort']                   if(html_valid?("sort") and @cgi['sort'] != "")
    @sort_dir = @cgi['sort_dir']           if(html_valid?("sort_dir") and @cgi['sort_dir'] != "")
    @offset = @cgi['offset'].to_i          if(html_valid?("offset") and @cgi['offset'] != "")
    @records = @cgi['records'].to_i        if(html_valid?("records") and @cgi['records'] != "")
    @roid = @cgi['roid'].to_i              if(html_valid?("roid") and @cgi['roid'] != "")
    @field = @cgi['field']                 if(html_valid?("field") and @cgi['field'] != "")
    @elems = @cgi['elems']                 if(html_valid?("elems") and @cgi['elems'] != "")
    @fakeuser = @cgi['fakeuser']           if(html_valid?("fakeuser") and @cgi['fakeuser'] != "")
  end
  # CGIパラメータ取得
  def [](name)
    rc = @cgi[name]
    rc = "" if(not rc)
    rc
  end
  # CGIパラメータの設定
  def []=(name,value)
    @cgi[name] = value.to_s
  end
  # javascript
  def html_rodeo_javascript()
    eruby(__FILE__,"rodeo_javascript",binding)
  end
end

#
# メインループ
#

class Rodeo
  # CGI実行
  def run()
    begin
      error = nil
      disp = false
      table = nil
      table = table_new(@table) if(@table)
      table.select(@roid) if(@roid and @roid != 0)
      m = "action_#{@cgi['action']}"
      if(table)
        if(@field)
          if(table[@field].methods.include?(m.intern))
            instance_eval("table[@field].#{m}()".untaint)
            disp = true
          end
        end
        if(not disp)
          if(table.methods.include?(m.intern))
            instance_eval("table.#{m}()".untaint)
            disp = true
          end
        end
      end
      if(not disp)
        if(self.methods.include?(m.intern))
          instance_eval("#{m}(table)".untaint)
          disp = true
        end
      end
      if(not disp)
        eruby(__FILE__,"table_browser",binding)
        output()
      end
    rescue Exception
      error  = "Content-Type: text/html\r\n\r\n"
      error << "<html>\r\n<body>\r\n#{CGI::escapeHTML($!.message)}\r\n<hr><pre>"
      $@.each {|ent|
        error << ent+"\n"
      }
      print error
    end
  end
  # メイン
  def main(cgi,code)
    get_cgi_data(cgi,code)
    @user_record = table_new("ユーザ")
    ttl = @user_record['アカウント'].conf['session_ttl'].to_i
    if(@user_record['アカウント'].valid?(@cgidata.cookies['user'][0],@cgidata.cookies['session'][0]))
      cookie1 = CGI::Cookie.new('user'   ,@cgidata.cookies['user'][0]); cookie1.expires = Time.now + ttl
      cookie2 = CGI::Cookie.new('session',@cgidata.cookies['session'][0]); cookie2.expires = Time.now + ttl
      @cookie = [cookie1,cookie2]
      @admin = false
      @user = @cgidata.cookies['user'][0]
      group = table_new("グループ")
      @user_record['グループ'].value.each {|v|
        if(group.select(v))
          @admin = true if(group['管理者'].value == true)
        end
      }
      # fakeuser処理
      if(@fakeuser and @fakeuser != "")
        @user_record.select(@fakeuser.to_i)
      end
      run()
      return()
    else
      # 初回ログイン処理
      if(@cgi['id'] and @cgi['password'])
        session = @user_record['アカウント'].login(@cgi['id'],@cgi['password'])
        if(session)
          cookie1 = CGI::Cookie.new('user'   ,@cgi['id']); cookie1.expires = Time.now + ttl
          cookie2 = CGI::Cookie.new('session',session); cookie2.expires = Time.now + ttl
          @cookie = [cookie1,cookie2]
          @admin = false
          @user = @cgi['id']
          group = table_new("グループ")
          @user_record['グループ'].value.each {|v|
            if(group.select(v))
              @admin = true if(group['管理者'].value == true)
            end
          }
          eruby(__FILE__,"table_browser",binding)
          output()
          return()
        else
          error = %Q(<font color="red">ログインできませんでした。</font>)
          eruby(__FILE__,"login",binding)
          output()
          return()
        end
      else
        error = nil
        eruby(__FILE__,"login",binding)
        output()
        return()
      end
    end
    error = nil
    eruby(__FILE__,"login",binding)
    output()
  end
end

#
# イベントハンドラ
#

class Rodeo
  # ログアウト
  def action_logout(table)
    # ログアウト
    @user_record['アカウント'].logout(@cgidata.cookies['user'][0],@cgidata.cookies['session'][0])
    # テンポラリのクリーンナップ
    @sql.exec(%Q(delete from "temps" where uid = '#{user_record['アカウント'].id.value}'))
    # Cookieの消去
    ttl = @user_record['アカウント'].conf['session_ttl'].to_i
    cookie1 = CGI::Cookie.new('user'   ,""); cookie1.expires = Time.now - ttl
    cookie2 = CGI::Cookie.new('session',""); cookie2.expires = Time.now - ttl
    @cookie = [cookie1,cookie2]
    eruby(__FILE__,"logout",binding)
    output()
  end
  # リストのアップデート
  def action_update(table)
    eruby(__FILE__,"table_browser_list",binding)
    output()
  end
  # テーブルの作成
  def action_create(table)
    if(@cgi['name'] != "")
      table = table_new(@cgi['name'])
      if(table.exist)
        code = "ERR"
        rc = %Q(<font color="red">テーブル「#{@cgi['name']}」は既に存在します。</font>)
      else
        table.create()
        code = "OK"
        rc = %Q(<font color="red">テーブル「#{@cgi['name']}」を作成しました。</font>)
      end
    else
      code = "ERR"
      rc = %Q(<font color="red">テーブル名を入力してください。</font>)
    end
    Rodeo.puts "#{code}:#{rc}"
    output()
  end
  # テーブルの削除
  def action_delete(table)
    table = table_new(@cgi['name'])
    table.delete_table()
    code = "OK"
    rc = %Q(<font color="red">テーブル「#{@cgi['name']}」を削除しました。</font>)
    Rodeo.puts "#{code}:#{rc}"
    output()
  end
  # パスワード変更
  def action_user_password(table)
    eruby(__FILE__,"user_password",binding)
    output()
  end
  def action_do_user_password(table)
    done = @user_record['アカウント'].change_passwd(@cgi['password_old'],@cgi['password_new'],@cgi['password_chk'])
    if(done == nil)
      done = "パスワードを変更しました"
      eruby(__FILE__,"user_password_done",binding)
      output()
    else
      eruby(__FILE__,"user_password_done",binding)
      output()
    end
  end
  # テーブル一覧
  def action_table_list(table)
    eruby(__FILE__,"table_browser_list",binding)
    output()
  end
end

__END__

#### no_action

<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%html_rodeo_javascript()%>
  </head>
  <body>
    <%=@cgi['action']%>が見つかりません。<br>
  </body>
</html>

#### rodeo_javascript

<%#if(ENV['HTTP_USER_AGENT'] =~ /iphone/i)%>
<%#else%>
<link rel="stylesheet" href="lib/rodeo.css" type="text/css"></link>
<%#end%>
<script type="text/javascript" src="lib/browserinfo.js"></script>
<script type="text/javascript"><!--
  // ブラウザ判定
  var binfo = new BrowserInfo();
  // Load完了後のハンドラ
  on_load_fook = new Array();
  iframe_stack = new Array();
//  iframe_stack[iframe_stack.length] = "work_frame";
  // RodeoCGI引数
  rodeo_appname           = "<%=@appname%>";
  rodeo_user              = "<%=@user%>";
  rodeo_table             = "<%=@table%>";
  rodeo_session           = "<%=@session%>";
  <%if(@where)%>
    rodeo_where           = '<%=@where.gsub(/\'/,"\\\\\'")%>';
  <%else%>
    rodeo_where           = '';
  <%end%>
  rodeo_sort              = "<%=@sort%>";
  rodeo_sort_dir          = "<%=@sort_dir%>";
  rodeo_offset            = <%=@offset%>;
  rodeo_records           = <%=@records%>;
  rodeo_roid              = <%=@roid%>;
  rodeo_field             = "<%=@field%>";
  rodeo_elems             = <%=@elems%>;
  rodeo_fakeuser          = "<%=@fakeuser%>";
  rodeo_old_offset        = 0;
  rodeo_old_sort          = "roid";
  rodeo_old_sort_dir      = "asc";
//-->
</script>
<script type="text/javascript" src="lib/rodeo.js"></script>
<script type="text/javascript" src="lib/overlib.js"></script>
<script type="text/javascript" src="lib/tiny_mce/jscripts/tiny_mce/tiny_mce.js"></script>

#### rodeo_logo

<div id="rodeo_logo" class="header" style="width:100%;margin-bottom:<%=margin%>">
  <table>
    <tr>
      <td class="tools" colspan="5" nowrap>
        <span style="color:#590000;font-weight:bold;">Rodeo</span>&nbsp;[<%=@db%>]
        &nbsp;<span id="rodeo_message" onclick="message_clear()"><%=error%></span>
      </td>
      <td class="tools" width="100%" nowrap><br></td>
      <td class="tools" align="right" valign="top" nowrap>
        <font size="-1">
          Copyright (C) 2000-2007 <a href="mailto:a-shigi@sgmail.jp">A.Shigihara</a>. All Rights Reserved.
          Powered by <a href="http://www.ruby-lang.org/">Ruby</a> and <a href="http://www.postgresql.jp/">PostgreSQL</a>.
        </font>
      </td>
    </tr>
  </table>
</div>

#### login

<%#if(ENV['HTTP_USER_AGENT'] =~ /iphone/i)%>
<%#eruby(__FILE__,"iphone_login",binding)%>
<%#else%>
<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%html_rodeo_javascript()%>
    <title><%=@db%>/Login</title>
  </head>
  <body onload="document.getElementById('id').focus()" style="margin:16px 32px 16px 32px;">
    <%margin = 1; eruby(__FILE__,"rodeo_logo",binding)%>
    <%html_form("") {%>
      <div id="rodeo_logo" class="header" style="width:100%;margin-bottom:2">
        <table align="center">
          <tr>
            <%html_hidden("state","login")%>
            <%html_hidden("action",@cgi['action'] || "table_browser")%>
            <th class="login" style="text-align:left;" nowrap>ユーザID</th>
            <td class="login" nowrap>
              <%html_text("id",@cgi['id'],{"size"=>"20","width"=>"100%"})%>
            </td>
          </tr>
          <tr>
            <th class="login" style="text-align:left;" nowrap>パスワード</th>
            <td class="login" nowrap>
              <%html_password("password",@cgi['password'],{"size"=>"20","width"=>"100%"})%>
            </td>
          </tr>
        </table>
      </div>
      <div id="rodeo_logo" class="header" style="width:100%;margin-bottom:2">
        <table align="center">
          <tr>
            <td class="login" align="center" nowrap>
              <%html_submit("submit","ログイン")%>
            </td>
          </tr>
        </table>
      <%}%>
    </div>
  </body>
</html>
<%#end%>

#### logout

<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_load_func() {
        document.location = "<%=@appname%>";
      }
    </script>
  </head>
  <body onload="on_load_func()">
  </body>
</html>

#### table_browser

<%#if(ENV['HTTP_USER_AGENT'] =~ /iphone/i)%>
<%#eruby(__FILE__,"iphone_table_browser",binding)%>
<%#else%>
<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%html_rodeo_javascript()%>
    <title><%=@db%>/テーブル一覧</title>
    <script type="text/javascript">
      function calc_elems() {
        var doch,logo;
        var line;
        doc_h = document.body.clientHeight;
//        alert(doc_h);
        line = document.getElementById("table0").offsetHeight;
        logo = document.getElementById("rodeo_logo").offsetHeight;
        rodeo_elems = Math.floor((doc_h - logo) / line) - 7;
//        alert(line+","+rodeo_elems);
      }
      function open_default_table() {
        <%if(@user_record.fields['デフォルトテーブル'] and @user_record['デフォルトテーブル'].value)%>
          <%if(table_new(@user_record['デフォルトテーブル'].value).name)%>
            make_iframe("<%=table_new(@user_record['デフォルトテーブル'].value).name%>");
          <%end%>
        <%end%>
      }
    </script>
  </head>
  <body onload="calc_elems(); open_default_table();" onresize="calc_elems();">
    <div id="rodeo_logo" class="header" style="margin-bottom:1px;">
      <table class="table_tools">
        <tr>
          <td class="tools" nowrap><!-- img src="image/rodeo.png" --><span style="color:#590000;font-weight:bold;">Rodeo</span>&nbsp;</td>
          <td class="tools" nowrap><%=@db%>/<%=@user%></td>
          <td class="tools" width="100%" nowrap><br></td>
          <td class="record_status" nowrap>
            <font size="-1">
            Copyright (C) 2000-2009 <a href="mailto:a-shigi@sgmail.jp">A.Shigihara</a>. All Rights Reserved.
            Powered by <a href="http://www.ruby-lang.org/">Ruby</a> and <a href="http://www.postgresql.jp/">PostgreSQL</a>.
            </font>
          </td>
        </tr>
      </table>
      <table class="table_tools">
        <tr>
          <td class="tools" nowrap>
            <%html_imglink("javascript:rodeo_help()","stock_help-agent.png","取扱説明書",nil,true)%>
            &nbsp;
          </td>
          <td class="tools" nowrap>
            <%html_imglink("javascript:user_password()","preferences-system.png","パスワード変更",nil,true)%>
            &nbsp;
          </td>
          <td class="tools" nowrap>
            <%html_imglink("javascript:user_logout()","gnome-shutdown.png","ログアウト",nil,true)%>
            &nbsp;
          </td>
          <td class="tools" style="vertical-align:top; width:100%;" nowrap>
            <style type="text/css">
              div.popup {
                visibility: hidden;
                position: absolute;
                background-color: ivory;
                border-style: solid;
                border-color: gray;
                border-width: 1px;
                color: red;
                padding: 2px 4px 2px 4px;
              }
            </style>
            <div class="popup" id="rodeo_message" onclick="message_clear();"><%=error%></div>
          </td>
          <td class="tools" nowrap>
            <%eruby(__FILE__,"table_browser_tools",binding)%>
          </td>
        </tr>
      </table>
    </div>
    <div id="table_browser_list" class="table_tools" style="border:0px;">
      <%eruby(__FILE__,"table_browser_list",binding)%>
    </div>
  </body>
</html>
<%#end%>

#### table_browser_list

<table id="if_tables" class="table_list" width="100%">
  <%n = 0%>
  <!-- アプリケーションモジュール -->
  <%@sql.exec(%Q(select "tablename" from "pg_tables" where tablename~'^rc_*' order by tablename)) {|res|%>
    <%menu = []%>
    <%res.each {|ent|%>
      <%
        name = ent['tablename'].sub(/^rc_/,"")
#        name.force_encoding(__ENCODING__) if(RUBY_VERSION >= "1.9.0")
        table = table_new(name)
      %>
      <%table.farray.each {|fld|%>
        <%if(fld['fobj'].class.superclass == RFPlugin and fld['fobj'].icon() != nil)%>
          <%if(fld['fobj'].conf['priority'].to_i >= 0)%>
            <%menu << {'prio'=>fld['fobj'].conf['priority'].to_i,'table'=>name,'field'=>fld['name'],'icon'=>fld['fobj'].icon(),'fobj'=>fld['fobj']}%>
          <%end%>
        <%end%>
      <%}%>
    <%}%>
    <%menu.sort{|a,b|a['prio']<=>b['prio']}.each {|h|%>
      <tr>
        <%atr = (n % 2 == 0) ? "table_list_odd" : "table_list_even"%>
        <%if(h['fobj'].exec)%>
          <th class="<%=atr%>" nowrap>
            <%html_imglink("javascript:exec_iframe('#{h['table']}','#{h['field']}','#{h['table']}')",h['icon'],nil,nil,true)%>
            <img src="image/enable/blank.png" style="width:1px;height:<%=@line_height%>px;vertical-align:middle">
          </th>
          <td class="<%=atr%>" width="100%" nowrap>
            <a href="javascript:exec_iframe('<%=h['table']%>','<%=h['field']%>','<%=h['table']%>');"><%=h['table']%>:<%=h['field']%></a>
          </td>
        <%else%>
          <th class="<%=atr%>" nowrap>
            <%html_imglink("javascript:exec_iframe('#{h['table']}','#{h['field']}','#{h['table']}')",h['icon'],nil,nil,false)%>
            <img src="image/enable/blank.png" style="width:1px;height:<%=@line_height%>px;vertical-align:middle">
          </th>
          <td class="<%=atr%>" width="100%" nowrap>
            <%=h['field']%>
          </td>
        <%end%>
      </tr>
      <%n += 1%>
    <%}%>
  <%}%>
  <!-- テーブル一覧 -->
  <%@sql.exec(%Q(select "tablename" from "pg_tables" where tablename~'^rc_*' order by tablename)) {|res|%>
    <%count = 0%>
    <%res.each {|ent|%>
      <%
        table_alias = name = ent['tablename'].sub(/^rc_/,"")
        table = table_new(name)
        if(table['roid'].conf['table_alias'] != "")
          table_alias = table['roid'].conf['table_alias']
        end
        disp = false
        if(@admin)
          disp = true
        else
          if(table['roid'].conf['access_account'] & @user_record['グループ'].value != [])
            disp = true
          end
        end
      %>
      <%if(disp)%>
        <tr>
          <%atr = (n % 2 == 0) ? "table_list_odd" : "table_list_even"%>
          <th class="<%=atr%>" nowrap>
            <%html_imglink("javascript:table_delete('#{name}')","stock_data-sources-delete.png",nil,nil,@admin)%>
            <img src="image/enable/blank.png" style="width:1px;height:<%=@line_height%>px;vertical-align:middle">
          </th>
          <td id="table<%=count%>" class="<%=atr%>" width="100%" nowrap>
            <a href="javascript:make_iframe('<%=name%>');"><%=table_alias%></a>
          </td>
        </tr>
        <%n += 1%>
        <%count += 1%>
      <%end%>
    <%}%>
  <%}%>
  <%if(n == 0)%>
    <tr>
      <%atr = (n % 2 == 0) ? "table_list_odd" : "table_list_even"%>
      <th class="<%=atr%>" nowrap>
        <%html_imglink(nil,nil,"削除",nil,false)%>
      </th>
      <td class="<%=atr%>" width="100%" nowrap>
        <tt>アクセスできるテーブルがありません</tt>
      </td>
    </tr>
  <%end%>
</table>

#### table_browser_tools

<table class="table_tools">
  <tr>
    <%html_form("javascript:table_create('create_form'); void(0)",:id=>"create_form") {%>
      <td class="tools" nowrap>
        <%
          if(@admin)
            html_text("name","",{"size"=>"20"})
            html_submit("submit","T追加")
          else
            html_text("name","",{"size"=>"20","disabled"=>"true"})
            html_submit("submit","T追加",{"disabled"=>"true"})
          end
        %>
      </td>
    <%}%>
  </tr>
</table>

#### user_password

<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%html_rodeo_javascript()%>
  </head>
  <body style="margin:0px; padding:1px; border-width:1px; border-style:solid; border-color:gray;">
    <%html_form(@appname) {%>
      <%html_hidden("action","do_user_password")%>
      <table class="record_spec" width="100%" style="margin-bottom:1px">
        <tr>
          <th class="record_spec" width="100%"><%=@user%>のパスワード変更</th>
          <th class="record_spec" nowrap><%html_submit("submit","実行")%></th>
        </tr>
      </table>
      <table class="record_spec" width="100%" style="margin-bottom:1px">
        <tr>
          <th class="record_spec" style="padding:1px;" nowrap>パスワード</th>
          <td class="record_spec" style="padding:1px;" width="100%" nowrap>
            旧パスワード&nbsp;<%html_password("password_old","")%>
            新パスワード&nbsp;<%html_password("password_new","")%>&nbsp;(確認用)&nbsp;<%html_password("password_chk","")%>
          </td>
        </tr>
      </table>
      <table class="record_spec" width="100%">
        <tr>
          <th class="record_spec" width="100%"></th>
          <th class="record_spec"><%html_submit("submit","実行")%></th>
        </tr>
      </table>
    <%}%>
  </body>
</html>

#### user_password_done

<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%html_rodeo_javascript()%>
  </head>
  <body onload="parent.user_password_done('<%=done%>');">
  </body>
</html>

#### iphone_login

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title><%=@db%>/Login</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;"/>
    <link rel="apple-touch-icon" href="lib/iui/iui-logo-touch-icon.png" />
    <meta name="apple-touch-fullscreen" content="YES" />
    <style type="text/css" media="screen">@import "lib/iui/iui.css";</style>
    <script type="application/x-javascript" src="lib/iui/iui.js"></script>
    <%html_rodeo_javascript()%>
  </head>
  <body>
    <div class="toolbar">
      <h1 id="pageTitle">Rodeo Login</h1>
      <a class="button blueButton" type="submit" href="javascript:document.getElementById('login').submit();">ログイン</a>
    </div>
    <%html_form(@appname,{"id"=>"login","class"=>"panel","selected"=>"true","target"=>"_self"}) {%>
      <%html_hidden("state","login")%>
      <%html_hidden("action",@cgi['action'])%>
      <fieldset>
        <div class="row">
          <label>ユーザID</label>
          <%html_text("id",@cgi['id'])%>
        </div>
        <div class="row">
          <label>パスワード</label>
          <%html_password("password",@cgi['password'])%>
        </div>
      </fieldset>
    <%}%>
  </body>
</html>

#### iphone_table_browser

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title><%=@db%>/テーブル一覧</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;"/>
    <link rel="apple-touch-icon" href="lib/iui/iui-logo-touch-icon.png" />
    <meta name="apple-touch-fullscreen" content="YES" />
    <style type="text/css" media="screen">@import "lib/iui/iui.css";</style>
    <script type="application/x-javascript" src="lib/iui/iui.js"></script>
    <%html_rodeo_javascript()%>
  </head>
  <body>
    <div class="toolbar">
      <h1 id="pageTitle">テーブル一覧</h1>
      <a class="button blueButton" type="submit" href="javascript:javascript:user_logout();">ログアウト</a>
    </div>
    <ul id="tables" selected="true">
      <!-- テーブル一覧 -->
      <%@sql.exec(%Q(select "tablename" from "pg_tables" where tablename~'^rc_*' order by tablename)) {|res|%>
        <%res.each {|ent|%>
          <%
            name = ent[0].sub(/^rc_/,"")
            table = table_new(name)
            disp = false
            if(@admin)
              disp = true
            else
              if(table['roid'].conf['access_account'] & @user_record['グループ'].value != [])
                disp = true
              end
            end
          %>
          <%if(disp)%>
            <%url = ""%>
            <%url << "#{@appname}?"%>
            <%url << "user=#{CGI::escape(@user||"")}&"%>
            <%url << "session=#{CGI::escape(@session||"")}&"%>
            <%url << "table=#{CGI::escape(name||"")}&"%>
            <%url << "action=rodeo_browser"%>
            <li><a href="#{url}" target="<%=CGI::escape(name)%>"><%=name%></a></li>
          <%end%>
        <%}%>
      <%}%>
    </ul>
  </body>
</html>


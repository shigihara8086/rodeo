# -*- coding: utf-8 -*-

#
# ID型
#

class RFid < RFstring
  def self.name(); "ID" end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
    @conf['cols'] = 16
  end
  # CGIからの取り込み
  def from_cgi()
    super()
  end
end

__END__


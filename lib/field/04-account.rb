# -*- coding: utf-8 -*-

#
# アカウント型
#

class RFaccount < RFBase
  attr_accessor :id
  attr_accessor :passwd
  attr_accessor :session
  def self.name(); "アカウント" end
  def self.type(); [RFid.type(),RFpasswd.type(),RFsession.type()] end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
    @conf['session_ttl'] = 86400 unless(@conf['session_ttl'])
    @conf['id'] = {} unless(@conf['id'])
    @conf['passwd'] = {} unless(@conf['passwd'])
    @conf['session'] = {} unless(@conf['session'])
    @id = RFid.new(name+"_0",table,@conf['id'])
    @passwd = RFpasswd.new(name+"_1",table,@conf['passwd'])
    @session = RFsession.new(name+"_2",table,@conf['session'])
  end
  #
  def value()
    @id.value
  end
  #
  def cgi_name()
    @id.cgi_name()
  end
  #
  def clear()
    @id.clear()
    @passwd.clear()
    @session.clear()
  end
  #
  def order()
    @id.db_name()
  end
  # 設定
  def config_entry(su=true)
    super(true) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      @passwd.config_entry(false)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @conf['session_ttl'] = @table.rodeo["#{@fid}.session_ttl"].to_i
    @passwd.eval_config()
  end
  # 絞りこみ
  def where_entry(su=true)
    @id.where_entry(su)
  end
  # 絞りこみの評価
  def eval_where()
  	@id.eval_where()
  end
  # 絞りこみの評価
  def eval_where_marked(roid)
  	@id.eval_where_marked(roid)
  end
  # DBフィールド名
  def db_name()
    @id.db_name()
  end
  # データベースからの取り込み
  def from_db(db)
    @id.from_db(db)
    @passwd.from_db(db)
    @session.from_db(db)
  end
  # データベースへの書き込みデータの作成
  def to_db()
    [@id.to_db(),@passwd.to_db(),@session.to_db()]
  end
  # CGIからの取り込み
  def from_cgi()
    super() {
      @id.from_cgi()
      @passwd.from_cgi()
      @session.from_cgi()
      @value = @id.value
      @error = @id.error || @passwd.error
    }
  end
  def from_csv(data)
    if(data)
      @id.value,@passwd.value,@session.value = data.split(/,/,3)
    end
  end
  def to_csv()
    %Q("#{@id.value},#{@passwd.value},#{@session.value}")
  end
  # 入力
  def entry(opt={})
    opt = {} unless(opt)
    opt['accesskey'] = @conf['accesskey'] if(@conf['accesskey'] != "")
    super() {
      @table.rodeo.eruby(__FILE__,"entry",binding)
    }
  end
  # 表示
  def display(list=false)
    super(list) {|list|
      @id.display(list)
      if(@passwd.value)
        Rodeo.print "/"
        @passwd.display(list)
      end
    }
  end
  def to_s()
    @id.to_s()
  end
  # アカウント認証
  def login(id,passwd)
    session = nil
    roid = @table.find(@name+"_0",id)
    if(roid)
      if(@passwd.conf['plain_passwd'])
        if(@passwd.value != nil and passwd == @passwd.value)
          now = Time.now()
          session = now.to_i.to_s.crypt(@passwd.value)
          @table.rodeo.sql.exec(%Q(delete from "session" where "time" < '#{(now - @conf['session_ttl']).strftime("%Y-%m-%d %H:%M:%S")}'))
          @table.rodeo.sql.exec(%Q(insert into "session" ("id","session","time") values ('#{id}','#{session}','#{now.strftime("%Y-%m-%d %H:%M:%S")}')))
          if(@table.rodeo.user_record.name == @table.name)
            @table.find(@name+"_0",id)
          end
        end
      else
        if(@passwd.value != nil and passwd.crypt(@passwd.value) == @passwd.value)
          now = Time.now()
          session = now.to_i.to_s.crypt(@passwd.value)
          @table.rodeo.sql.exec(%Q(delete from "session" where "time" < '#{(now - @conf['session_ttl']).strftime("%Y-%m-%d %H:%M:%S")}'))
          @table.rodeo.sql.exec(%Q(insert into "session" ("id","session","time") values ('#{id}','#{session}','#{now.strftime("%Y-%m-%d %H:%M:%S")}')))
          if(@table.rodeo.user_record.name == @table.name)
            @table.find(@name+"_0",id)
          end
        end
      end
    end
    session
  end
  # セッションチェック
  def valid?(id,session)
    valid = false
    @table.rodeo.sql.exec(%Q(select "id" from "session" where "session" = '#{session}')) {|res|
      if(res.num_tuples > 0 and res[0]['id'] == id)
        @table.find(@name+"_0",id)
        valid = true
      end
    }
    valid
  end
  # ログアウト
  def logout(id,session)
    @table.rodeo.sql.exec(%Q(delete from "session" where "session" = '#{session}'))
  end
  # パスワード変更
  def change_passwd(pw_old,pw_new,pw_chk)
    done = "パスワードの変更が出来ませんでした"
    if(pw_new.size > 0 and pw_new == pw_chk)
      if(@passwd.conf['plain_passwd'])
        if(pw_old == @passwd.value)
          @passwd.set_passwd(pw_new)
          @table.update(@table.roid)
          done = nil
        end
      else
        if(pw_old.crypt(@passwd.value) == @passwd.value)
          @passwd.set_passwd(pw_new)
          @table.update(@table.roid)
          done = nil
        end
      end
    else
      done = "パスワードが正しくありません"
    end
    done
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf">セッションの保存期間(秒)</th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.session_ttl",@conf['session_ttl'],{"size"=>10})%></td>
</tr>

#### entry

<table>
  <tr>
    <td>ユーザID</td>
    <td colspan="2"><%@id.entry()%></td>
  </tr>
  <tr>
    <td>パスワード</td>
    <td><%@passwd.entry("1")%></td>
    <td><%@passwd.entry("2")%></td>
  </tr>
</table>


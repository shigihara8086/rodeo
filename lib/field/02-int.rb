# -*- coding: utf-8 -*-

#
# 整数型
#

class RFint < RFInt
  def self.name(); "整数" end
  def self.type(); RFInt.type() end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
    @conf['default'] = "" unless(@conf['default'])
    @conf['cols'] = 10 unless(@conf['cols'])
    @conf['comma'] = false unless(@conf['comma'])
    @conf['serial'] = false unless(@conf['serial'])
  end
  # クリア
  def clear()
    @value = @conf['default'].to_i if(@conf['default'] and @conf['default'] != "")
  end
  # 設定
  def config_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @conf['default'] = @table.rodeo["#{@fid}.default"]
    @conf['cols']    = @table.rodeo["#{@fid}.cols"]
    @conf['comma']   = @table.rodeo.html_bool("#{@fid}.comma")
    @conf['serial']  = @table.rodeo.html_bool("#{@fid}.serial")
  end
  # 絞りこみ
  def where_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"where_entry",binding)
      yield() if(block_given?)
    }
  end
  # 絞りこみの評価
  def eval_where()
    where = nil
    if(@table.rodeo["#{@fid}.data"] =~ /^[0-9+-]+$/)
      case @table.rodeo["#{@fid}.operator"]
      when "="  then where = %Q("#{@name}" =  #{@table.rodeo["#{@fid}.data"]})
      when "!=" then where = %Q("#{@name}" != #{@table.rodeo["#{@fid}.data"]})
      when "<"  then where = %Q("#{@name}" <  #{@table.rodeo["#{@fid}.data"]})
      when "<=" then where = %Q("#{@name}" <= #{@table.rodeo["#{@fid}.data"]})
      when ">"  then where = %Q("#{@name}" >  #{@table.rodeo["#{@fid}.data"]})
      when ">=" then where = %Q("#{@name}" >= #{@table.rodeo["#{@fid}.data"]})
      end
    else
      @error = "絞り込み条件には整数を入力してください。"
    end
    where
  end
  # 絞りこみの設定
  def eval_where_marked(roid)
    where = nil
    @table.select(roid)
    if(@value)
      where = %Q("#{@name}" = #{to_db()})
    end
    where
  end
  # CGIからの取り込み
  def from_cgi()
    super() {
      if(@table.rodeo.html_valid?(cgi_name()))
        if(@table.rodeo[cgi_name()] != "")
          @value = @table.rodeo[cgi_name()].tr("０-９","0-9").to_i
        else
          @value = nil
        end
      end
    }
  end
  # 入力
  def entry(opt={})
    super() {
      if(not @conf['serial'])
        opt['size'] = @conf['cols']
        opt['accesskey'] = @conf['accesskey'] if(@conf['accesskey'] != "")
        if(ENV['HTTP_USER_AGENT'] =~ /iphone/i)
          @table.rodeo.html_text(cgi_name(),@value.to_s)
        else
          @table.rodeo.html_number(cgi_name(),@value.to_s,opt)
        end
      else
        if(@value)
          @table.rodeo.html_text(cgi_name(),@value.to_s,opt.merge({"readonly"=>"true"}))
        else
          nextv = 0
          @table.rodeo.sql.exec(%Q(select max("#{@name}") from "rd_#{@table.name}")) {|res|
            nextv = res[0]['max'].to_i + 1
          }
          @table.rodeo.html_text(cgi_name(),nextv.to_s,opt.merge({"readonly"=>"true"}))
        end
      end
    }
  end
  # 表示
  def display(list=false)
    super(list) {|list|
      if(@value)
        if(@conf['comma'])
          Rodeo.puts @value.to_s.reverse.gsub(/(\d{3})(?=\d)/,'\1,').reverse
        else
          Rodeo.puts @value.to_s
        end
      end
    }
  end
  # 文字列化
  def to_s()
    @value.to_s
  end
  # DB書き込み
  def to_db()
    if(@value)
      @value.to_s
    else
      if(@conf['serial'])
        nextv = 0
        @table.rodeo.sql.exec(%Q(select max("#{@name}") from "rd_#{@table.name}")) {|res|
          nextv = res[0]['max'].to_i + 1
        }
        nextv.to_s
      else
        nil
      end
    end
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("初期値")%></th>
  <td class="field_conf"><%@table.rodeo.html_number("#{@fid}.default",@conf['default'],{"size"=>10})%></td>
</tr>
<tr>
  <th class="field_conf">
    <%@table.rodeo.html_tooltip("シリアル番号","この項目をシリアル番号にします。")%>
  </th>
  <td class="field_conf">
    <%
      @table.rodeo.html_radio("#{@fid}.serial",@conf['serial']) {
        @table.rodeo.html_item(true,"シリアル番号として扱う")
        @table.rodeo.html_item(false,"通常の入力項目として扱う")
      }
    %>
  </td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("入力桁数")%></th>
  <td class="field_conf"><%@table.rodeo.html_number("#{@fid}.cols",@conf['cols'],{"size"=>10})%></td>
</tr>
<tr>
  <th class="field_conf">
    <%@table.rodeo.html_tooltip("カンマ編集",
      "この項目を表示する時３桁毎にカンマ(,)を入れるかどうかを設定します。")%>
  </th>
  <td class="field_conf">
    <%
      @table.rodeo.html_radio("#{@fid}.comma",@conf['comma']) {
        @table.rodeo.html_item(true,"する")
        @table.rodeo.html_item(false,"しない")
      }
    %>
  </td>
</tr>

#### where_entry

<td class="field_conf">
  <%@table.rodeo.html_number("#{@fid}.data","",{"size"=>@conf['cols']})%>
  <%
    @table.rodeo.html_radio("#{@fid}.operator","=") {
      @table.rodeo.html_item("=" ,"等しい")
      @table.rodeo.html_item("!=","等しくない")
      @table.rodeo.html_item("<" ,"小さい")
      @table.rodeo.html_item("<=","以下")
      @table.rodeo.html_item(">" ,"大きい")
      @table.rodeo.html_item(">=","以上")
    }
  %>
</td>


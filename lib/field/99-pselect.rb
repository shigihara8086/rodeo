#
# パチンコ選択型
#

class RFpselect < RFIntArray
  def self.name(); "キーワード選択" end
  def self.type(); RFIntArray.type() end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
    @conf['select_type']   = "select" unless(@conf['select_type'])
    @conf['master_table']  = nil      unless(@conf['master_table'])
    @conf['master_field']  = nil      unless(@conf['master_field'])
    @conf['master_sort']   = nil      unless(@conf['master_sort'])
    @conf['master_sub']    = "roid"   unless(@conf['master_sub'])
    @conf['link_field']    = []       unless(@conf['link_field'])
    @conf['table_layout']  = false    if(@conf['table_layout'] == nil)
    @conf['table_columns'] = 1        unless(@conf['table_columns'])
    @conf['with_other']    = false    if(@conf['with_other'] == nil)
    @conf['where']         = nil      unless(@conf['where'])
    @conf['default']       = "その他" unless(@conf['default'])
    @value = []
  end
  # ソートフィールド名
  def order()
    db_name()
  end
  # 設定
  def config_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @conf['select_type']   = @table.rodeo["#{@fid}.select_type"]
    @conf['master_table']  = @table.rodeo["#{@fid}.master_table"]
    @conf['master_field']  = @table.rodeo["#{@fid}.master_field"]
    @conf['master_sort']   = @table.rodeo["#{@fid}.master_sort"]
    @conf['master_sub']    = @table.rodeo["#{@fid}.master_sub"]
    @conf['link_field'] = []
    @table.rodeo.cgi.each {|k,v|
      if(k =~ /^#{@fid}.link_field\.(.+)$/)
        @conf['link_field'] << $1
      end
    }
    @conf['table_layout']  = (@table.rodeo["#{@fid}.table_layout"] == "true") ? true : false
    @conf['table_columns'] = @table.rodeo["#{@fid}.table_columns"].to_i
    @conf['with_other']    = (@table.rodeo["#{@fid}.with_other"] == "true") ? true : false
    @conf['default']       = @table.rodeo["#{@fid}.default"]
  end
  # 絞りこみ
  def where_entry(su=true)
    super(su) {
      opt = {}
      @table.rodeo.eruby(__FILE__,"select_entry",binding)
      yield() if(block_given?)
    }
  end
  # 絞りこみの評価
  def eval_where()
    from_cgi()
    if(@value and @value[0])
      where = %Q(#{@value[0]} = ANY("#{@name}"))
    else
      where = nil
    end
    where
  end
  # 絞りこみの設定
  def eval_where_marked(roid)
    ary = []
    @table.select(roid)
    if(@value)
      @value.each {|v|
        ary << %Q(#{v} = ANY("#{db_name()}"))
      }
    end
    if(ary.size > 0)
      where = "(" + ary.join(" or ") + ")"
    else
      where = nil
    end
    where
  end
  # CGIからの取り込み
  def from_cgi_single()
    @value = []
    if(@table.rodeo[cgi_name()])
      if(@table.rodeo[cgi_name()] != "")
        @value = [@table.rodeo[cgi_name()].to_i]
      end
    end
  end
  def from_cgi_code()
    @value = []
    if(@table.rodeo[cgi_name()])
      if(@table.rodeo[cgi_name()] != "")
        master = @table.rodeo.table_new(@conf['master_table'])
        if(master.find(@conf['master_field'],@table.rodeo[cgi_name()]))
          @value = [master['roid'].value]
        else
          @error = "該当するレコードが見つかりません"
        end
      end
    end
  end
  def from_cgi_multi()
    name = cgi_name()
    @value = []
    @table.rodeo.cgi.each {|k,v|
      if(k =~ /^#{name}\.\d+$/)
        @value << v.to_i
      end
    }
  end
  def from_cgi()
    super() {
      if(@conf['master_table'] != "")
        from_cgi_single()
        if(@conf['valid'] and @value.size == 0)
          @error = "#{@name}は必須項目です"
        end
      else
        @error = "フィールドの設定を行なってください"
      end
    }
  end
  #
  def to_csv()
    @master = @table.rodeo.table_new(@conf['master_table']) unless(@master)
    item = []
    @value.each {|roid|
      if(@master.select(roid))
        item << @master[@conf['master_field']].value.to_s
      end
    }
    item.join(",")
  end
  #
  def from_csv(data)
    @master = @table.rodeo.table_new(@conf['master_table']) unless(@master)
    idx = []
    if(data)  #<------------- < GR0010 2008.08.21 Add as >
      data.split(/,/).each {|v|
        where = %Q("#{@conf['master_field']}" = '#{v}')
        @master.fetch(0,@master.count(where),where,%Q("roid")) {|roid|
          idx << roid
          break
        }
      }
    end       #<------------- < GR0010 2008.08.21 Add as >
    @value = idx
  end
  # 入力
  def entry(opt={})
    opt = {} unless(opt)
    opt['accesskey'] = @conf['accesskey'] if(@conf['accesskey'] != "")
    super() {
      @table.rodeo.eruby(__FILE__,"select_entry",binding)
    }
  end
  # 表示
  def display(list=false)
    super(list) {|list|
      @table.rodeo.eruby(__FILE__,"select_display",binding)
    }
  end
  #
  def to_s()
    master = @table.rodeo.table_new(@conf['master_table'])
    if(@value and @value[0])
      master.select(@value[0])
      master[@conf['master_field']].to_s()
    else
      ""
    end
  end
end

class RFpselect < RFIntArray
  # フィールド一覧リロード用
  def action_select_field_update()
    master = @table.rodeo.table_new(@table.rodeo['master'])
    Rodeo.puts %Q(var sel = document.getElementById("#{@table.rodeo['element']}");)
    Rodeo.puts %Q(sel.length = 0;)
    i = 0
    master.farray.each {|h|
      if(h['sort'] != 0)
        Rodeo.puts %Q(sel.length ++;)
        Rodeo.puts %Q(sel.options[#{i}].value = "#{h['name']}";)
        Rodeo.puts %Q(sel.options[#{i}].text = "#{h['name']}";)
        i += 1
      end
    }
    @table.rodeo.output()
  end
  # select再表示
  def action_select_refresh()
    master = @table.rodeo.table_new(@conf['master_table'])
    w = []
    @table.rodeo['where'].split(/[\s　]+/).each {|s|
      code = s.gsub(/\'/,"''")
      code.gsub!(/\^/,"\\\\^")
      code.gsub!(/\$/,"\\\\$")
      code.gsub!(/\[/,"\\\\[")
      code.gsub!(/\]/,"\\\\]")
      code.gsub!(/\./,"\\\\.")
      code.gsub!(/\*/,"\\\\*")
      code.gsub!(/\+/,"\\\\+")
      code.gsub!(/\-/,"\\\\-")
      code.gsub!(/\\/,"\\\\\\")
      code.gsub!(/\?/,"\\\\?")
      code.gsub!(/\(/,"\\\\(")
      code.gsub!(/\)/,"\\\\)")
      code.gsub!(/\'/,"\\\\'")
      w << %Q(jfazzy("#{@conf['master_field']}") ~ jfazzy('#{code}'))
    }
    where = w.join(" and ")
    Rodeo.puts %Q(var sel = document.getElementById("#{cgi_name()}");)
    Rodeo.puts %Q(var msg = document.getElementById("#{cgi_name("message")}");)
    Rodeo.puts %Q(sel.length = 0;)
    i = 0
    if(where != "")
      count = master.count(where)
      if(count < 300)
        if(count > 0)
          master.fetch(0,master.count(where),where,%Q("#{master[@conf['master_sort']].order()}" asc)) {|roid|
            Rodeo.puts %Q(sel.length ++;)
            Rodeo.puts %Q(sel.options[#{i}].value = #{roid};)
            if(@conf['master_sub'] != "roid")
              Rodeo.puts %Q(sel.options[#{i}].text = "#{master[@conf['master_field']].to_s()}/#{master[@conf['master_sub']].to_s()}";)
            else
              Rodeo.puts %Q(sel.options[#{i}].text = "#{master[@conf['master_field']].to_s()}";)
            end
            i += 1
          }
          Rodeo.puts %Q(msg.innerHTML = "<font color='blue'>#{i}個見つかりました</font>";)
        else
          Rodeo.puts %Q(msg.innerHTML = "<font color='orange'>候補が見つかりませんでした</font>";)
        end
      else
        Rodeo.puts %Q(sel.length ++;)
        Rodeo.puts %Q(sel.options[0].value = "";)
        Rodeo.puts %Q(sel.options[0].text = "←検索ワード";)
        Rodeo.puts %Q(msg.innerHTML = "<font color='gray'>#{master.count(where)}個見つかりました。候補が多すぎます</font>";)
      end
    else
      Rodeo.puts %Q(sel.length ++;)
      Rodeo.puts %Q(sel.options[0].value = "";)
      Rodeo.puts %Q(sel.options[0].text = "←検索ワード";)
      Rodeo.puts %Q(msg.innerHTML = "検索ワードを入力してください";)
    end
    @table.rodeo.output()
  end
  # カスケード先の検索
  def find_slave(obj)
    slave = []
    obj.table.farray.each_index {|i|
      if(obj.table.farray[i+1])
        # 次のフィールド
        nfld = obj.table.farray[i+1]['fobj']
        # マスターテーブルがある？
        if(nfld.conf['master_table'])
          if(nfld.conf['link_field'].include?(obj.name))
            mas = obj.table.rodeo.table_new(nfld.conf['master_table'])
            # マスターテーブルを検索
            mas.farray.each {|m|
              if(m['fobj'].conf['master_table'] == obj.conf['master_table'])
                if(m['name'] == obj.name)
                  if(obj.value and obj.value.size > 0)
                    v = obj.value[0]
                  else
                    mm = obj.table.rodeo.table_new(obj.conf['master_table'])
                    mm.fetch(0,1,obj.conf['where'],%Q("#{mm[obj.conf['master_sort']].order()}" asc)) {|roid|
                      v = roid
                    }
                  end
                  nfld.conf['where'] = %Q(#{v} = ANY("#{obj.name}"))
                  slave << nfld
                end
              end
            }
          end
        end
      end
    }
    slave
  end
end

__END__

#### config_entry

<script type="text/javascript"><!--
// フィールドリストの更新
function setup_field_<%=@fid%>(obj) {
  var arg = sysargs({
    "action":"select_field_update",
    "element":"<%=@fid%>.master_field",
    "master":obj.value
  });
  postFormFunc(rodeo_appname,arg,function(resp) { eval(resp.responseText); });
  var arg = sysargs({
    "action":"select_field_update",
    "element":"<%=@fid%>.master_sort",
    "master":obj.value
  });
  postFormFunc(rodeo_appname,arg,function(resp) { eval(resp.responseText); });
}
//-->
</script>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("参照テーブル","マスタ関連の設定を行います。")%></th>
  <td>
    <table>
      <tr>
        <td><%@table.rodeo.html_tooltip("マスタテーブル","マスタとして参照するテーブルを選択します。")%></td>
        <td>
          <%
            first = nil
            @table.rodeo.sql.exec(%Q(select "tablename" from "pg_tables" where tablename~'^rc_*' order by tablename)) {|res|
              @table.rodeo.html_select(
                "#{@fid}.master_table",@conf['master_table'],:onchange=>"setup_field_#{@fid}(this)") {
                res.each {|ent|
                  name = ent['tablename'].sub(/^rc_/,"")
                  first = name unless(first)
                  @table.rodeo.html_item(name,name)
                }
              }
            }
            master = @table.rodeo.table_new(@conf['master_table'] || first)
          %>
        </td>
      </tr>
      <tr>
        <td><%@table.rodeo.html_tooltip("選択項目","マスタの項目の内どの項目を選択肢として表示するかを選択します。")%></td>
        <td>
          <%
            value = @conf['master_field']
            @table.rodeo.html_select("#{@fid}.master_field",value) {
              master.farray.each {|h|
                if(h['sort'] != 0)
                  @table.rodeo.html_item(h['name'],h['name'])
                end
              }
            }
          %>
        </td>
      </tr>
      <tr>
        <td><%@table.rodeo.html_tooltip("並べ替え/関連情報","選択肢を表示するとき、どの項目順に並べるかを選択します。\\
           通常は「選択項目」と同じ項目を選択してください。")%></td>
        <td>
          <%
            value = @conf['master_sort']
            @table.rodeo.html_select("#{@fid}.master_sort",value) {
              master.farray.each {|h|
                @table.rodeo.html_item(h['name'],h['name'])
              }
            }
          %>
        </td>
      </tr>
      <tr>
        <td><%@table.rodeo.html_tooltip("サブ表示フィールド","選択肢を表示するとき、メイン項目に加えて表示する項目を選択します。\\
         roidを指定した場合は表示しません。")%></td>
        <td>
          <%
            value = @conf['master_sub']
            @table.rodeo.html_select("#{@fid}.master_sub",value) {
              master.farray.each {|h|
                @table.rodeo.html_item(h['name'],h['name'])
              }
            }
          %>
        </td>
      </tr>
    </table>
  </td>
</tr>
<tr>
  <th class="field_conf">
    <%@table.rodeo.html_tooltip("関連項目","自動制限機能を有効にするフィールドを選んでください。\\
      チェックを入れたフィールドに該当する項目だけを選択肢として表示するようになります。")%>
  </th>
  <td class="field_conf">
    <%@table.rodeo.html_check("#{@fid}.link_field",@conf['link_field']) {%>
      <%
        @table.farray.each {|h|
          if(h['type'] == "RFselect" and h['fobj'].name != @name)
            @table.rodeo.html_item(h['name'],h['name'])
            Rodeo.print "<br>"
          end
        }
      %>
    <%}%>
  </td>
</tr>
<tr>
  <th class="field_conf">
    <%@table.rodeo.html_tooltip("その他","選択肢を表示する時、「その他」もしくは「選択なし」のような特殊値を表示するかどうかを設定します。")%>
  </th>
  <td class="field_conf">
    <%@table.rodeo.html_radio("#{@fid}.with_other",@conf['with_other']) {%>
      <%@table.rodeo.html_item(true, "その他を付ける")%>
      <%@table.rodeo.html_item(false,"その他を付けない")%>
    <%}%>
    &nbsp;その他に表示するラベル&nbsp;<%@table.rodeo.html_text("#{@fid}.default",@conf['default'],{"size"=>30})%>
  </td>
</tr>
<tr>
  <th class="field_conf">
    <%@table.rodeo.html_tooltip("レイアウト","選択肢を表示する時、\\
    テーブルを使ってレイアウトする場合には「レイアウトする」を選んでください。\\
    「カラム数」には横に並べる個数を設定します。")%>
  </th>
  <td class="field_conf">
    <%@table.rodeo.html_radio("#{@fid}.table_layout",@conf['table_layout']) {%>
      <%@table.rodeo.html_item(false,"レイアウトしない")%>
      <%@table.rodeo.html_item(true, "レイアウトする")%>
    <%}%>
    &nbsp;カラム数&nbsp;<%@table.rodeo.html_number("#{@fid}.table_columns",@conf['table_columns'],{"size"=>3})%>&nbsp;列
  </td>
</tr>

#### select_entry

<%if(@conf['master_table'])%>
  <%slave = find_slave(self)%>
  <script type="text/javascript">
    function select_refresh_<%=@fid%>(where) {
      var arg = sysargs({
        "action":"select_refresh",
        "field":"<%=@name%>",
        "where":where
      });
      document.body.style.cursor = "wait";
      postFormFunc(rodeo_appname,arg,function(resp) {
        document.body.style.cursor = "default";
        eval(resp.responseText);
        <%if(slave.size > 0)%>
          <%slave.each {|s|%>
            select_refresh_<%=s.fid%>(document.getElementById("<%=cgi_name()%>").value + '= ANY("<%=@name%>")');
          <%}%>
        <%end%>
      });
    }
  </script>
  <span id="master_list_<%=@fid%>">
  <%@table.rodeo.eruby(__FILE__,"select_entry_list",binding)%>
  </span>
<%else%>
  <font color="red">フィールドの設定を行なってください</font>
<%end%>

#### select_entry_list

<%if(slave.size > 0)%>
  <script type="text/javascript">
    function select_<%=@fid%>(obj) {
      <%slave.each {|s|%>
        select_refresh_<%=s.fid%>(obj.value + '= ANY("<%=@name%>")');
      <%}%>
    }
  </script>
<%else%>
  <script type="text/javascript">
    function select_<%=@fid%>(obj) {}
  </script>
<%end%>
<%master = @table.rodeo.table_new(@conf['master_table'])%>
<%
  @table.rodeo.html_text(cgi_name("where"),@conf['where'],opt.merge({"size"=>"10","onchange"=>"select_refresh_#{@fid}(this.value);"}))
  @table.rodeo.html_select(cgi_name(),@value[0]) {
    if(@value and master.select(@value[0]))
      if(@conf['master_sub'] != "roid")
        @table.rodeo.html_item(@value[0],"#{master[@conf['master_field']].to_s()}/#{master[@conf['master_sub']].to_s()}")
      else
        @table.rodeo.html_item(@value[0],"#{master[@conf['master_field']].to_s()}")
      end
      @table.rodeo.html_item("","-- 消去する --")
    else
      @table.rodeo.html_item("","←検索ワード")
    end
  }
%>
<span id="<%=cgi_name("message")%>"></span>

#### select_display

<script type="text/javascript">
  function refer(table,roid) {
    if(parent) {
      parent.record_refer(table,roid);
    } else {
      record_refer(table,roid);
    }
  }
</script>
<%
  master = @table.rodeo.table_new(@conf['master_table'])
  if(@value[0])
    if(master.select(@value[0]))
      if(@conf['master_sub'] != "roid")
#        if(list)
          Rodeo.puts %Q(<a href="javascript:refer('#{@conf['master_table']}',#{@value[0]});">#{master[@conf['master_field']].to_s()}</a>/)
          master[@conf['master_sub']].display(list)
#        else
#          Rodeo.puts %Q(#{master[@conf['master_field']].to_s()}/)
#          master[@conf['master_sub']].display(list)
#        end
      else
#        if(list)
          Rodeo.puts %Q(<a href="javascript:refer('#{@conf['master_table']}',#{@value[0]});">#{master[@conf['master_field']].to_s()}</a>)
#        else
#          Rodeo.puts %Q(#{master[@conf['master_field']].to_s()})
#        end
      end
    end
  else
    Rodeo.puts "#{@conf['default']}" if(@conf['with_other'])
  end
%>

#### where_entry

<td class="field_conf" style="white-space:normal;">
  <%
    count = 0
    @table.rodeo.html_check("#{@fid}.operator",[]) {
      master = @table.rodeo.table_new(@conf['master_table'])
      master.fetch(0,master.count(@conf['where']),@conf['where'],%Q("#{master[@conf['master_sort']].order()}" asc)) {|roid|
        if(@conf['master_sub'] != "roid")
          @table.rodeo.html_item(roid,master[@conf['master_field']].value.to_s+"/"+master[@conf['master_sub']].value.to_s)
        else
          @table.rodeo.html_item(roid,master[@conf['master_field']].value.to_s)
        end
        Rodeo.puts "<br>"
        count += 1
      }
      @table.rodeo.html_item("","どれにも該当しない")
      Rodeo.puts "<br>"
    }
  %>
</td>


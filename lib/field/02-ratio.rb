# -*- coding: utf-8 -*-

#
# 比率型
#

class RFratio < RFReal
  def self.name(); "比率" end
  def self.type(); RFReal.type() end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
    @conf['default'] = "" unless(@conf['default'])
    @conf['ratio_base'] = "100" unless(@conf['ratio_base'])
  end
  # クリア
  def clear()
    @value = @conf['default'].to_f if(@conf['default'] and @conf['default'] != "")
  end
  # 設定
  def config_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @conf['default'] = @table.rodeo["#{@fid}.default"]
    @conf['ratio_base'] = @table.rodeo["#{@fid}.ratio_base"]
  end
  # 絞りこみ
  def where_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"where_entry",binding)
      yield() if(block_given?)
    }
  end
  # 絞りこみの評価
  def eval_where()
    where = nil
    case @table.rodeo["#{@fid}.operator"]
    when "="  then where = %Q("#{@name}" =  #{@table.rodeo["#{@fid}.data"]})
    when "!=" then where = %Q("#{@name}" != #{@table.rodeo["#{@fid}.data"]})
    when "<"  then where = %Q("#{@name}" <  #{@table.rodeo["#{@fid}.data"]})
    when "<=" then where = %Q("#{@name}" <= #{@table.rodeo["#{@fid}.data"]})
    when ">"  then where = %Q("#{@name}" >  #{@table.rodeo["#{@fid}.data"]})
    when ">=" then where = %Q("#{@name}" >= #{@table.rodeo["#{@fid}.data"]})
    end
    where
  end
  # 絞りこみの設定
  def eval_where_marked(roid)
    where = nil
    @table.select(roid)
    if(@value)
      where = %Q("#{@name}" = #{to_db()})
    end
    where
  end
  # CGIからの取り込み
  def from_cgi()
    super() {
      if(@table.rodeo[cgi_name()])
        if(@table.rodeo[cgi_name()] != "")
          @value = @table.rodeo[cgi_name()].to_f / @conf['ratio_base'].to_f
        else
          @value = nil
        end
      else
        @value = nil
      end
    }
  end
  # 入力
  def entry(opt={})
    opt = {} unless(opt)
    opt['size'] = "10"
    opt['accesskey'] = @conf['accesskey'] if(@conf['accesskey'] != "")
    super() {
      str = ""
      str = (@value.to_f * @conf['ratio_base'].to_f).to_s if(@value)
      @table.rodeo.html_number(cgi_name(),str,opt)
    }
  end
  # 表示
  def display(list=false)
    super(list) {|list|
      if(@value)
        Rodeo.puts((@value * @conf['ratio_base'].to_f).to_s)
      end
    }
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("初期値")%></th>
  <td class="field_conf"><%@table.rodeo.html_number("#{@fid}.default",@conf['default'],{"size"=>10})%></td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("形式")%></th>
  <td class="field_conf">
    <%@table.rodeo.html_radio("#{@fid}.ratio_base",@conf['ratio_base']) {%>
      <%@table.rodeo.html_item("10",     "割")%><br>
      <%@table.rodeo.html_item("10.0",   "掛")%><br>
      <%@table.rodeo.html_item("100",    "100分率(％)")%><br>
      <%@table.rodeo.html_item("1000",   "1000分率(‰)")%><br>
      <%@table.rodeo.html_item("10000",  "万分率")%><br>
      <%@table.rodeo.html_item("1000000","ppm")%>
    <%}%>
  </td>
</tr>

#### where_entry

<td class="field_conf">
  <%@value = 0 unless(@value)%>
  <%@value = 0 if(@value == "")%>
  <%@table.rodeo.html_number("#{@fid}.data",(@value.to_f * @conf['ratio_base'].to_f).to_s,{"size"=>10})%>
  <%
    @table.rodeo.html_radio("#{@fid}.operator","=") {
      @table.rodeo.html_item("=" ,"等しい")
      @table.rodeo.html_item("!=","等しくない")
      @table.rodeo.html_item("<" ,"小さい")
      @table.rodeo.html_item("<=","以下")
      @table.rodeo.html_item(">" ,"大きい")
      @table.rodeo.html_item(">=","以上")
    }
  %>
</td>


# -*- coding: utf-8 -*-

#
# 座標型
#

class RFPoint < RFBase
  def self.name(); "座標" end
  def self.type(); "point" end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
    @value = nil
  end
  # データのクリア
  def clear()
    @value = nil
  end
  # ソート用フィールド名を返す
  def order()
    db_name()
  end
  # 設定
  def config_entry(su=true)
    super(su) {
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
  end
  # データベースからの取り込み
  def from_db(db)
    if(db[db_name()])
      @value = instance_eval(db[db_name()].sub(/^\(/,"[").sub(/\)$/,"]").untaint)
    else
      @value = nil
    end
  end
  # データベースへの書き込みデータの作成
  def to_db()
    if(@value)
      %Q(point(#{@value[0]},#{@value[1]}))
    else
      nil
    end
  end
  # CGIからの取り込み
  def from_cgi()
    super() {
      if(@table.rodeo[cgi_name("hidden")] != "")
        @value = @table.rodeo[cgi_name("hidden")].split(/,/).collect{|n| n.to_f }
      else
        yield() if(block_given?)
      end
		}
  end
  # データベースからの取り込み
  def from_csv(data)
    @value = instance_eval(("["+data+"]").untaint)
  end
  def to_csv()
    %Q("#{@value[0]},#{@value[1]}")
  end
  # 隠しフィールドの出力
  def hidden()
    @table.rodeo.html_hidden(cgi_name("hidden"),"#{@value[0]},#{@value[1]}")
  end
end

__END__


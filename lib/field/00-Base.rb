# -*- coding: utf-8 -*-

#
# 基底型
#

class RFBase < RFobject
  attr :name
  attr :plugin
  attr :exec
  attr :pseudo
  attr :style
  attr_accessor :value
  attr_accessor :conf
  attr_accessor :table
  attr_accessor :fid
  attr_accessor :error
  def self.name(); "基底" end
  def self.type(); nil end
  # 構築子
  def initialize(name,table,conf)
    @name = name
    @fid = name.unpack('H*')[0]
#    @fid = @table[name]['sort']
    @table = table
    @conf  = conf
    @error = nil
    @value = nil
    @plugin = false
    @exec = false
    @pseudo = false
    @style = nil
    @conf = {} unless(@conf)
    @conf['valid']        = false if(@conf['valid'] == nil)
    @conf['duplicate']    = true  if(@conf['duplicate'] == nil)
    @conf['can_entry']    = true  if(@conf['can_entry'] == nil)
    @conf['field_alias']  = ""    if(@conf['field_alias'] == nil)
    @conf['prefix']       = ""    if(@conf['prefix'] == nil)
    @conf['postfix']      = ""    if(@conf['postfix'] == nil)
    @conf['description']  = ""    if(@conf['description'] == nil)
    @conf['style']        = ""    if(@conf['style'] == nil)
    @conf['where_enable'] = false if(@conf['where_enable'] == nil)
    @conf['accesskey']    = ""    if(@conf['accesskey'] == nil)
    @conf['stats']        = false if(@conf['stats'] == nil)
    @conf['complete']     = false if(@conf['complete'] == nil)
    @style = @conf['style']
  end
  # 初期化
  def init()
  end
  # to_s
  def to_s()
    @value.to_s
  end
  # データのクリア
  def clear()
    @value = nil
  end
  # 実行用アイコン
  def icon()
    nil
  end
  # DBフィールド名を得る
  def db_name(suffix=nil)
    if(suffix)
      @name+"_#{suffix}"
    else
      @name
    end
  end
  # CGI名を得る
  def cgi_name(suffix=nil)
    if(suffix)
      "rd.#{@table.tid}.#{@name}.#{suffix}"
    else
      "rd.#{@table.tid}.#{@name}"
    end
  end
  # ソート用フィールド名を得る
  def order()
    nil
  end
  # ツールチップ
  def tooltip()
    if(@conf['description'] != nil and @conf['description'] != "")
      @table.rodeo.html_tooltip(@name,@conf['description'])
    end
  end
  # 設定画面の表示
  def config_entry(su=false)
    if(su)
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
    end
    yield() if(block_given?)
  end
  # 絞りこみ画面の表示
  def where_entry(su=false)
    if(su)
      @table.rodeo.eruby(__FILE__,"where_entry",binding)
    end
    yield() if(block_given?)
  end
  # 設定の評価
  def eval_config()
    @conf['valid'] = @table.rodeo.html_bool("#{@fid}.valid")
    @conf['duplicate'] = @table.rodeo.html_bool("#{@fid}.duplicate")
    @conf['description'] = @table.rodeo["#{@fid}.description"]
    @conf['field_alias'] = @table.rodeo["#{@fid}.field_alias"]
    @conf['prefix'] = @table.rodeo["#{@fid}.prefix"]
    @conf['postfix'] = @table.rodeo["#{@fid}.postfix"]
    @conf['style'] = @table.rodeo["#{@fid}.style"]
    @conf['where_enable'] = @table.rodeo.html_bool("#{@fid}.where_enable")
    @conf['accesskey'] = @table.rodeo["#{@fid}.accesskey"]
    @conf['stats'] = @table.rodeo.html_bool("#{@fid}.stats")
    @conf['complete'] = @table.rodeo.html_bool("#{@fid}.complete")
  end
  # CGIデータの評価
  def from_cgi()
    yield() if(block_given?)
    # 必須項目チェック
    @error = "#{@name}は必須項目です" if(@conf['valid'] == true and @value == nil)
    # 重複許可チェック
    if(@conf['duplicate'] == false)
      if(@table.insert_mode)
        where = %Q("#{db_name()}" = '#{@value}')
      else
        where = %Q("roid" != '#{@table['roid'].value}' and "#{db_name()}" = '#{@value}')
      end
      if(@table.count(where) > 0)
        @error = "#{@name}は既に登録されています"
      end
    end
  end
  # データベースからの読み込み
  def from_db(db)
  end
  # 計算型の計算実行
  def calc()
  end
  # 入力
  def entry()
    Rodeo.print @conf['prefix']
    yield() if(block_given?)
    Rodeo.print @conf['postfix']
    @table.farray.each {|h|
      if(h['fobj'].plugin == true)
        if(h['fobj'].conf['plugin_field'] == @name)
          h['fobj'].entry()
        end
      end
    }
  end
  # 追加フック
  def inserted(roid)
  end
  # 編集フック
  def updated(roid)
  end
  # 削除フック
  def deleted(roid)
  end
  # 表示
  def display(list=false)
    Rodeo.print @conf['prefix'] if(@value)
    yield(list) if(block_given?)
    Rodeo.print @conf['postfix'] if(@value)
    if(not list)
      @table.farray.each {|h|
        if(h['fobj'].plugin == true)
          if(h['fobj'].conf['plugin_field'] == @name)
            h['fobj'].display(list)
          end
        end
      }
    end
  end
  # 絞り込み条件
  def eval_where()
    nil
  end
  # 絞り込み条件
  def eval_where_marked(roid)
    nil
  end
  # 隠し項目の出力
  def hidden()
  end
  # 統計(sum)
  def sum()
    nil
  end
  def max()
    nil
  end
  def min()
    nil
  end
  def ave()
    nil
  end
  def val()
    nil
  end
  def nval()
    nil
  end
end

#
# エラー表示用
#

class RFerror < RFBase
end

#
# プラグイン型
#

class RFPlugin < RFBase
  attr_accessor :main
  def self.plugin(); true end
  # 構築子
  def initialize(name,table,conf)
    super(name,table,conf)
    @conf['priority'] = 0 unless(@conf['priority'])
    @plugin = true
    @exec = true
    @main = nil
  end
  # 設定
  def config_entry(su=true,enable=nil)
    super(su) {
      @table.rodeo.eruby(__FILE__,"plugin_config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @conf['plugin_field']  = @table.rodeo["#{@fid}.plugin_field"]
    @conf['priority'] = @table.rodeo["#{@fid}.priority"]
  end
  # アプリケーション画面の表示
  def action_app_exec()
    yield() if(block_given?)
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf">
    <%@table.rodeo.html_tooltip("必須項目",
      "この項目が必須項目かどうかを設定します。\\n「必須」にすると入力が無い場合にはエラーになります。")%>
  </th>
  <td class="field_conf">
    <%
      @table.rodeo.html_radio("#{@fid}.valid",@conf['valid']) {
        @table.rodeo.html_item(true,"必須")
        @table.rodeo.html_item(false,"任意")
      }
    %>
  </td>
</tr>
<tr>
  <th class="field_conf">
    <%@table.rodeo.html_tooltip("重複許可",
      "この項目が重複するデータを許可するかどうかを設定します。\\n「する」の場合データが重複しているとエラーになります。")%>
  </th>
  <td class="field_conf">
    <%
      @table.rodeo.html_radio("#{@fid}.duplicate",@conf['duplicate']) {
        @table.rodeo.html_item(true,"する")
        @table.rodeo.html_item(false,"しない")
      }
    %>
  </td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("フィールド名エイリアス","フィールド名の表示を変更します。DB上のテーブル名は変更しません。")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.field_alias",@conf['field_alias'],{"style"=>"width:100%"})%></td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("前置子","データの前に表示する文字列です。")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.prefix",@conf['prefix'],{"style"=>"width:100%"})%></td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("後置子","データの後に表示する文字列です。")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.postfix",@conf['postfix'],{"style"=>"width:100%"})%></td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("スタイル","テーブルの一覧表示の時、TABLEタグに設定するstyle属性です。")%></th>
  <td class="field_conf"><tt><%@table.rodeo.html_text("#{@fid}.style",@conf['style'],{"style"=>"width:100%"})%></tt></td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("アクセスキー","ショートカットキーを設定します。")%></th>
  <td class="field_conf"><tt><%@table.rodeo.html_text("#{@fid}.accesskey",@conf['accesskey'],{"style"=>"size:2"})%></tt></td>
</tr>
<tr>
  <th class="field_conf">
    <%@table.rodeo.html_tooltip("一覧表示",
      "この項目が一覧表示の条件設定に使用できるかどうかを設定します。")%>
  </th>
  <td class="field_conf">
    <%
      @table.rodeo.html_radio("#{@fid}.where_enable",@conf['where_enable']) {
        @table.rodeo.html_item(true,"使う")
        @table.rodeo.html_item(false,"使わない")
      }
    %>
  </td>
</tr>
<tr>
  <th class="field_conf">
    <%@table.rodeo.html_tooltip("統計情報表示","統計情報を表示するかどうかを設定します。")%>
  </th>
  <td class="field_conf">
    <%
      @table.rodeo.html_radio("#{@fid}.stats",@conf['stats']) {
        @table.rodeo.html_item(true,"表示する")
        @table.rodeo.html_item(false,"表示しない")
      }
    %>
  </td>
</tr>
<tr>
  <th class="field_conf">
    <%@table.rodeo.html_tooltip("補完機能","補完機能を有効/無効を設定します。")%>
  </th>
  <td class="field_conf">
    <%
      @table.rodeo.html_radio("#{@fid}.complete",@conf['complete']) {
        @table.rodeo.html_item(true,"有効")
        @table.rodeo.html_item(false,"無効")
      }
    %>
  </td>
</tr>
<tr>
  <th class="field_conf">
    <%@table.rodeo.html_tooltip("項目の説明",
      "<span class=\"help\"><tt>[?]</tt></span>にマウスを置いた時にこの項目を表示します。説明文にはHTMLのタグを書くことができます。")%>
  </th>
  <td class="field_conf"><%@table.rodeo.html_textarea("#{@fid}.description",@conf['description'],0,2,{"style"=>"width:100%"})%></td>
</tr>

#### where_entry

<!-- where -->

#### plugin_config_entry

<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("表示順","メニューに表示する時の表示順を設定します。")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.priority",@conf['priority'],{"size"=>5})%></td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("接続フィールド","プラグインを追加するフィールドを選択します。")%></th>
  <td class="field_conf">
    <%
      @table.rodeo.html_select("#{@fid}.plugin_field",@conf['plugin_field']) {
        if(enable)
          @table.farray.each {|h|
            if(enable)
              if(h['sort'] != 0 and enable.include?(h['type']))
                @table.rodeo.html_item(h['name'],h['name'])
              end
            else
              @table.rodeo.html_item(h['name'],h['name'])
            end
          }
        else
          @table.farray.each {|h|
            if(h['sort'] != 0)
              @table.rodeo.html_item(h['name'],h['name'])
            end
          }
        end
      }
    %>
  </td>
</tr>

#### exec_entry

<!-- exec entry -->


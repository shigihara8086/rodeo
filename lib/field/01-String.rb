# -*- coding: utf-8 -*-

#
# 文字列型
#

class RFString < RFBase
  def self.name(); "文字列" end
  def self.type(); "text" end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
  end
  # ソート用フィールド名を返す
  def order()
    db_name()
  end
  # データベースからの取り込み
  def from_db(db)
    @value = db[db_name()]
  end
  # データベースへの書き込みデータの作成
  def to_db()
    if(@value)
      %Q('#{@value.gsub(/\'/,"''").gsub(/\\/,"\\\\\\\\")}')
    else
      nil
    end
  end
  # CSVからの取り込み
  def from_csv(data)
    @value = data.to_s
  end
  # CGIからの取り込み
  def from_cgi()
    super() {
      if(@table.rodeo[cgi_name("hidden")] != "")
        @value = @table.rodeo[cgi_name("hidden")]
      else
        yield() if(block_given?)
      end
		}
  end
  # データベースからの取り込み
  def from_csv(data)
    @value = data
  end
  def to_csv()
    %Q("#{@value}")
  end
  # 隠しフィールドの出力
  def hidden()
    @table.rodeo.html_hidden(cgi_name("hidden"),@value)
  end
end

__END__


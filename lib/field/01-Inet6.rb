# -*- coding: utf-8 -*-

#
# IPv6アドレス型
#

class IP6Addr
  def initialize(str)
    @ip = nil
    @mlen = nil
    i,m = str.split(/\//)
    is = i.split(/:/)
    case i
    when /^::$/
      @ip = Array.new(8,"")
    when /^::/
      @ip = Array.new(8 - is.size,"") + is
    when /::$/
      @ip = is + Array.new(8 - is.size,"")
    when /^.+::.+$/
      is[is.index("")] = Array.new(9 - is.size,"")
      @ip = is.flatten
    else
      @ip = is
    end
    if(m.nil?)
      mlen = 128
    else
      mlen = m.to_i
      raise ArgumentError if(mlen > 128)
    end
    @mlen = mlen
    @ip.each {|e|
      raise ArgumentError if(e !~ /^[0-9a-fA-F]{0,4}$/)
    }
    raise ArgumentError if(@ip.size != 8)
    @ip.collect! {|n| n.hex }
  end
  def to_astr
    r = @ip.collect {|n| n.to_s(16) }.join(":").gsub(/:(0:)+/,"::").sub(/^0::0$/,"::")
    if(@mlen != 128)
      r << "/" + @mlen.to_s
    end
    r
  end
  def to_str
    @ip.collect {|n| n.to_s(16) }.join(":")
  end
end

class RFInet6 < RFBase
  def self.name(); "IPv6アドレス" end
  def self.type(); "inet" end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
  end
  # ソート用フィールド名を返す
  def order()
    db_name()
  end
  # 設定
  def config_entry(su=true)
    super(su) {
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
  end
  # データベースからの取り込み
  def from_db(db)
    if(db[db_name])
      @value = IP6Addr.new(db[db_name()])
    else
      @value = nil
    end
  end
  # データベースへの書き込みデータの作成
  def to_db()
    if(@value)
      %Q('#{@value.to_astr}')
    else
      nil
    end
  end
  # CGIからの取り込み
  def from_cgi()
    super() {
      if(@table.rodeo[cgi_name("hidden")] != "")
        @value = IPAddr.new(@table.rodeo[cgi_name("hidden")])
      else
        yield() if(block_given?)
      end
		}
  end
  # データベースからの取り込み
  def from_csv(data)
    @value = IP6Addr.new(data)
  end
  def to_csv()
    if(@value)
      %Q("#{@value.to_astr}")
    else
      %Q("")
    end
  end
  #
  def hidden()
    if(@value)
      @table.rodeo.html_hidden(cgi_name("hidden"),@value.to_astr)
    end
  end
end

__END__


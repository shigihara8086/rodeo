# -*- coding: utf-8 -*-

#
# メール送信型
#

require "#{File.dirname(__FILE__)}/../email.rb"

class RFlogmailsend < RFPlugin
  def self.name(); "ログメール送信" end
  # 実行用アイコン
  def icon()
    "applications-system.png"
  end
  # 設定
  def config_entry(su=true)
    super(su,[]) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @conf['mail_title'] = @table.rodeo["#{@fid}.mail_title"]
    @conf['to_address'] = @table.rodeo["#{@fid}.to_address"]
    @conf['from_field'] = @table.rodeo["#{@fid}.from_field"]
  end
  # アプリケーション画面の表示
  def action_app_exec()
    super()
    @table.rodeo.output()
  end
  def updated(roid)
    inserted(roid)
  end
  def inserted(roid)
    mail = Email.new()
    mail.smtpServer = "smtp.sgmail.jp"
    mail['To'] = (@conf['to_address']).untaint
    mail['From'] = (@table[@conf['from_field']].value).untaint
    mail['Subject'] = @conf['mail_title'].mime_encode
    mail['Content-Type'] = "text/plain; charset=ISO-2022-JP"
    @table.farray.each {|h|
      if(h['name'] != "roid" and h['type'] != "RFmemo" and h['fobj'].class.superclass != RFPlugin)
        Rodeo.clear()
        h['fobj'].display(true)
        mail << "#{h['name']}：#{Rodeo.gettext()}".tojis
      end
    }
    Rodeo.clear()
    @table.farray.each {|h|
      if(h['type'] == "RFmemo")
        if(h['fobj'].value)
          mail << ""
          mail << "[[ #{h['name']} ]]".tojis
          h['fobj'].value.each_line {|line|
            line.chomp!
            line.sub!(/\r/,"")
            mail << line.tojis
          }
          mail << ""
        end
      end
    }
    mail.send()
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("送信タイトル","メールのタイトルを設定します。")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.mail_title",@conf['mail_title'],:size=>60)%></td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("送信アドレス","メールの宛先を設定します。")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.to_address",@conf['to_address'],:size=>60)%></td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("Fromフィールド","メールを送信する時のFromアドレスとして使用するフィールドを選択します。")%></th>
  <td class="field_conf">
    <%
      @table.rodeo.html_select("#{@fid}.from_field",@conf['from_field']) {
        @table.farray.each {|h|
          if(h['type'] == "RFstring")
            @table.rodeo.html_item(h['name'],h['name'])
          end
        }
      }
    %>
  </td>
</tr>


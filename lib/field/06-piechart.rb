# -*- coding: utf-8 -*-

#
# パイチャート型
#

class RFpiechart < RFPlugin
  def self.name(); "パイチャート" end
  # 実行用アイコン
  def icon()
    "applications-system.png"
  end
  # 設定
  def config_entry(su=true)
    super(su,nil) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
  end
  # 表示
  def display(list)
  end
  # アプリケーション画面の表示
  def action_app_exec()
    super() {
      if(@table.rodeo['flags'] != "")
        # メニューから
        @table.rodeo.eruby(__FILE__,"where_config",binding)
      else
        # テーブル一覧から
        @java_prefix = "parent.parent."
        @table.rodeo.eruby(__FILE__,"app_exec",binding)
      end
      @table.rodeo.output()
    }
  end
  # 表示
  def action_do()
    @java_prefix = "parent."
    where = []
    @table.farray.each {|f|
      if(f['name'] != "roid" and f['fobj'].class.superclass != RFPlugin)
        if(@table.rodeo["#{f['name']}"] != "")
          w = f['fobj'].eval_where()
          where << w if(w)
        end
      end
    }
    @table.rodeo.where = where.join(" and ")
    @table.rodeo.where = nil if(@table.rodeo.where == "")
    @table.rodeo.eruby(__FILE__,"app_exec",binding)
    @table.rodeo.output()
  end
  # 設定の出力
  def action_app_settings()
    @table.rodeo.eruby(__FILE__,"app_settings",binding)
    @table.rodeo.output({'type'=>"text/xml"})
  end
  # データの出力
  def action_app_data_select()
    master = @table.rodeo.table_new(@main.conf['master_table'])
    Rodeo.puts %Q(<?xml version="1.0" encoding="UTF-8"?>)
    Rodeo.puts %Q(<pie>)
    count = 0
    hash = {}
    master.fetch(0,master.count(),nil,nil) {|roid|
      where  = %Q(#{roid}=ANY("#{@conf['plugin_field']}"))
      where << %Q( and #{@table.rodeo.where}) if(@table.rodeo.where)
      @table.rodeo.sql.exec(%Q(select count(*) from "rd_#{@table.name}" where #{where})) {|res|
        if(res.num_tuples > 0 and res[0]['count'].to_i > 0)
          hash[master[@main.conf['master_field']].value] = res[0]['count'].to_i
          count += res[0]['count'].to_i
        end
      }
    }
    hash.sort{|a,b| b[1] <=> a[1]}.each {|k,v|
      Rodeo.print %Q(<slice title="#{k}">#{v}</slice>)
    }
    other = @table.count(@table.rodeo.where) - count
    if(other > 0)
      Rodeo.print %Q(<slice title="その他">#{other}</slice>)
    end
    Rodeo.puts %Q(</pie>)
    @table.rodeo.output({'type'=>"text/xml"})
  end
  def action_app_data_selwother()
    master = @table.rodeo.table_new(@main.conf['select']['master_table'])
    Rodeo.puts %Q(<?xml version="1.0" encoding="UTF-8"?>)
    Rodeo.puts %Q(<pie>)
    count = 0
    hash = {}
    master.fetch(0,master.count(),nil,nil) {|roid|
      where  = %Q(#{roid}=ANY("#{@conf['plugin_field']}_0"))
      where << %Q( and #{@table.rodeo.where}) if(@table.rodeo.where)
      @table.rodeo.sql.exec(%Q(select count(*) from "rd_#{@table.name}" where #{where})) {|res|
        if(res.num_tuples > 0 and res[0]['count'].to_i > 0)
          hash[master[@main.conf['select']['master_field']].value] = res[0]['count'].to_i
          count += res[0]['count'].to_i
        end
      }
    }
    hash.sort{|a,b| b[1] <=> a[1]}.each {|k,v|
      Rodeo.print %Q(<slice title="#{k}">#{v}</slice>)
    }
    other = @table.count(@table.rodeo.where) - count
    if(other > 0)
      Rodeo.print %Q(<slice title="その他">#{other}</slice>)
    end
    Rodeo.puts %Q(</pie>)
    @table.rodeo.output({'type'=>"text/xml"})
  end
  def action_app_data_selpref()
    hash = {}
    count = 0
    Rodeo.puts %Q(<?xml version="1.0" encoding="UTF-8"?>)
    Rodeo.puts %Q(<pie>)
    [
      [ 1,"北海道"  ],[ 2,"青森県"  ],[ 3,"岩手県"  ],[ 4,"宮城県"  ],[ 5,"秋田県"  ],
      [ 6,"山形県"  ],[ 7,"福島県"  ],[ 8,"茨城県"  ],[ 9,"栃木県"  ],[10,"群馬県"  ],
      [11,"埼玉県"  ],[12,"千葉県"  ],[13,"東京都"  ],[14,"神奈川県"],[15,"新潟県"  ],
      [16,"富山県"  ],[17,"石川県"  ],[18,"福井県"  ],[19,"山梨県"  ],[20,"長野県"  ],
      [21,"岐阜県"  ],[22,"静岡県"  ],[23,"愛知県"  ],[24,"三重県"  ],[25,"滋賀県"  ],
      [26,"京都府"  ],[27,"大阪府"  ],[28,"兵庫県"  ],[29,"奈良県"  ],[30,"和歌山県"],
      [31,"鳥取県"  ],[32,"島根県"  ],[33,"岡山県"  ],[34,"広島県"  ],[35,"山口県"  ],
      [36,"徳島県"  ],[37,"香川県"  ],[38,"愛媛県"  ],[39,"高知県"  ],[40,"福岡県"  ],
      [41,"佐賀県"  ],[42,"長崎県"  ],[43,"熊本県"  ],[44,"大分県"  ],[45,"宮崎県"  ],
      [46,"鹿児島県"],[47,"沖縄県"  ],
    ].each {|pref|
      where  = %Q(#{pref[0]}=ANY("#{@conf['plugin_field']}"))
      where << %Q( and #{@table.rodeo.where}) if(@table.rodeo.where)
      @table.rodeo.sql.exec(
        %Q(select count(*) from "rd_#{@table.name}" where #{where})
      ) {|res|
        if(res.num_tuples > 0 and res[0]['count'].to_i > 0)
          hash[pref[1]] = res[0]['count'].to_i
          count += res[0]['count'].to_i
        end
      }
    }
    hash.sort{|a,b| b[1] <=> a[1]}.each {|k,v|
      Rodeo.print %Q(<slice title="#{k}">#{v}</slice>)
    }
    other = @table.count(@table.rodeo.where) - count
    if(other > 0)
      Rodeo.print %Q(<slice title="その他">)
      Rodeo.print other
      Rodeo.puts %Q(</slice>)
    end
    Rodeo.puts %Q(</pie>)
    @table.rodeo.output({'type'=>"text/xml"})
  end
  # データの出力
  def action_app_data()
    case @table[@conf['plugin_field']].class.to_s
    when "RFselect"    then action_app_data_select()
    when "RFselwother" then action_app_data_selwother()
    when "RFselpref"   then action_app_data_selpref()
    else
      Rodeo.puts %Q(<?xml version="1.0" encoding="UTF-8"?>)
      Rodeo.puts %Q(<pie>)
      Rodeo.print %Q(<slice title="全体">)
      Rodeo.print "100"
      Rodeo.puts %Q(</slice>)
      Rodeo.puts %Q(</pie>)
      @table.rodeo.output({'type'=>"text/xml"})
    end
  end
end

__END__

#### config_entry

<!-- config -->

#### where_config

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%@table.rodeo.html_rodeo_javascript()%>
  </head>
  <body class="margin0">
    <%@table.rodeo.html_form(@table.rodeo.appname) {%>
      <%@table.rodeo.html_hidden("action","do")%>
      <table class="record_spec" width="100%" style="margin-bottom:1px">
        <th class="record_spec" width="100%">表示の条件を設定してください。</th>
        <th class="record_spec"><%@table.rodeo.html_submit("exec","表示")%></th>
      </table>
      <table class="record_spec" width="100%" style="margin-bottom:1px">
        <%@table.farray.each {|f|%>
          <%if(f['name'] != "roid" and f['fobj'].conf['where_enable'] == true and f['fobj'].class.superclass != RFPlugin)%>
            <tr>
              <th class="record_spec" width="0%" nowrap>
                <%@table.rodeo.html_checkbox("#{f['name']}","",false)%>
                <%=f['name']%>
              </th>
              <%f['fobj'].where_entry(false)%>
            </tr>
          <%end%>
        <%}%>
      </table>
      <table class="record_spec" width="100%" style="margin-bottom:1px">
        <th class="record_spec" width="100%"></th>
        <th class="record_spec"><%@table.rodeo.html_submit("exec","表示")%></th>
      </table>
    <%}%>
  </body>
</html>

#### app_exec

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%@table.rodeo.html_rodeo_javascript()%>
    <script type="text/javascript" src="lib/chart/swfobject.js"></script>
    <script type="text/javascript">
      function on_close() {
        parent.list_shrink(null);
        parent.document.getElementById("rodeo_work").src = "blank.html";
      }
    </script>
  </head>
  <body style="margin:0px;">
    <table class="record_spec" width="100%"><tr>
      <th class="record_spec" width="100%">
    	  <%
    	    url  = "http://#{ENV['SERVER_NAME']}:#{ENV['SERVER_PORT']}#{ENV['REQUEST_URI'].sub(/admin.rbx/,"piechart.rbx")}"
    	    url << "?action=app_exec"
    	    url << "&table=#{CGI::escape(@table.name)}"
    	    url << "&field=#{CGI::escape(@name)}"
    	    if(@table.rodeo.where)
    	      url << "&where=#{CGI::escape(@table.rodeo.where)}"
    	    end
    	  %>
    	  <%if(@table.rodeo.appname != "piechart.rbx")%>
    	    <a href="<%=url%>"><%=@name%>(<%=@table.rodeo.where%>)</a><br>
    	  <%end%>
      </th>
      <th class="record_spec" nowrap><%@table.rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
      <th class="record_spec"><%@table.rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
    </tr></table>
	  <div id="piechart_<%=@fid%>">
		  <strong>You need to upgrade your Flash Player</strong>
	  </div>
	  <script type="text/javascript">
    fr = document.body;
	  var w = fr.clientWidth - 20;
    var h = fr.clientHeight - 50;
    var so = new SWFObject("lib/chart/ampie/ampie.swf","ampie",w,h,"0","#FFFFFF");
    so.addVariable("path", "lib/chart/ampie/");
    var arg = sysargs({
      "action":"app_settings",
      "table":"<%=@table.name%>",
      "field":"<%=@name%>"
    });
    so.addVariable("settings_file",escape(rodeo_appname+"?"+arg));
    var arg = sysargs({
      "action":"app_data",
      "table":"<%=@table.name%>",
      "field":"<%=@name%>"
    });
    so.addVariable("data_file",escape(rodeo_appname+"?"+arg));
    so.addVariable("preloader_color", "#999999");
    so.write("piechart_<%=@fid%>");
    </script>
    <table class="record_spec" width="100%"><tr>
      <th class="record_spec" width="100%"></th>
      <th class="record_spec" nowrap><%@table.rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
      <th class="record_spec"><%@table.rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
    </tr></table>
  </body>
</html>

#### app_settings

<?xml version="1.0" encoding="UTF-8"?>
<!-- Value between [] brackets, for example [#FFFFFF] shows default value which is used if this parameter is not set -->
<!-- This means, that if you are happy with this value, you can delete this line at all and reduce file size         -->
<!-- value or explanation between () brackets shows the range or type of values you should use for this parameter    -->
<!-- the top left corner has coordinates x = 0, y = 0                                                                -->

<settings> 
  <data_type>xml</data_type>                                  <!-- [xml] (xml / csv) -->
  <csv_separator>;</csv_separator>                            <!-- [;] (string) csv file data separator (you need it only if you are using csv file for your data) -->     
  <skip_rows>1</skip_rows>                                    <!-- [0] (Number) if you are using csv data type, you can set the number of rows which should be skipped here -->
  <font>ＭＳ　ゴシック</font>                                 <!-- [Arial] (font name) use device fonts, such as Arial, Times New Roman, Tahoma, Verdana... -->
  <text_size>11</text_size>                                   <!-- [11] (Number) text size of all texts. Every text size can be set individually in the settings below -->
  <text_color>#000000</text_color>                            <!-- [#000000] (hex color code) main text color. Every text color can be set individually in the settings below-->
  <decimals_separator>.</decimals_separator>                  <!-- [,] (string) decimal separator. Note, that this is for displaying data only. Decimals in data xml file must be separated with a dot -->
  <thousands_separator>,</thousands_separator>                <!-- [ ] (string) thousand separator -->
  <digits_after_decimal></digits_after_decimal>               <!-- [] (Number) if your value has less digits after decimal then is set here, zeroes will be added -->
  <reload_data_interval></reload_data_interval>               <!-- [0] (Number) how often data should be reloaded (time in seconds) -->
  <preloader_on_reload></preloader_on_reload>                 <!-- [false] (true / false) Whether to show preloaded when data or settings are reloaded -->
  <redraw></redraw>                                           <!-- [false] (true / false) if your chart's width or height is set in percents, and redraw is set to true, the chart will be redrawn then screen size changes -->
  <add_time_stamp>false</add_time_stamp>                      <!-- [false] (true / false) if true, a unique number will be added every time flash loads data. Mainly this feature is useful if you set reload _data_interval -->
  <precision>2</precision>                                    <!-- [2] (Number) shows how many numbers should be shown after comma for calculated values (percents) -->
  <exclude_invisible></exclude_invisible>                     <!-- [false] (true / false) whether to exclude invisible slices (where alpha=0) then calculating percent values or not -->
                                                                
  <pie>
    <x></x>                                                   <!-- [](Number) If left empty, will be positioned in the center -->
    <y></y>                                                   <!-- [](Number) If left empty, will be positioned in the center - 20px -->
    <radius></radius>                                         <!-- [] (Number) If left empty, will be 25% of your chart smaller side -->
    <inner_radius>30</inner_radius>                           <!-- [0] (Number) the radius of the hole (if you want to have donut, use > 0) -->
    <height>10</height>                                       <!-- [0] (Number) pie height (for 3D effect) -->
    <angle>20</angle>                                         <!-- [0] (0 - 90) lean angle (for 3D effect) -->
    <outline_color></outline_color>                           <!-- [#FFFFFF] (hex color code) -->    
    <outline_alpha></outline_alpha>                           <!-- [0] (Number) -->
    <base_color></base_color>                                 <!-- [] (hex color code) color of first slice -->
    <brightness_step></brightness_step>                       <!-- [20] (-100 - 100) if base_color is used, every next slice is filled with lighter by brightnessStep % color. Use negative value if you want to get darker colors -->
    <colors></colors>                                         <!-- [0xFF0F00,0xFF6600,0xFF9E01,0xFCD202,0xF8FF01,0xB0DE09,0x04D215,0x0D8ECF,0x0D52D1,0x2A0CD0,0x8A0CCF,0xCD0D74] (hex color codes separated by comas) -->
    <link_target></link_target>                               <!-- [] (_blank, _top...) If pie slice has a link this is link target -->
    <alpha></alpha>                                           <!-- [100] (0 - 100) slices alpha. You can set individual alphas for every slice in data file. If you set alpha to 0 the slice will be inactive for mouse events and data labels will be hidden. This allows you to make not full pies and donuts. -->
  </pie>
  
  <animation>
    <start_time>2</start_time>                                <!-- [0] (Number) fly-in time in seconds. Leave 0 to appear instantly -->
    <start_effect>bounce</start_effect>                       <!-- [bounce] (bounce, regular, strong) -->
    <start_radius></start_radius>                             <!-- [] (Number) if left empty, will use pie.radius * 5 -->
    <start_alpha></start_alpha>                               <!-- [0] (Number) -->                
    <pull_out_on_click></pull_out_on_click>                   <!-- [true] (true / false) whether to pull out slices when user clicks on them (or on legend entry) -->
    <pull_out_time>1.5</pull_out_time>                        <!-- [0] (number) pull-out time (then user clicks on the slice) -->
    <pull_out_effect>Bounce</pull_out_effect>                 <!-- [bounce] (bounce, regular, strong) -->
    <pull_out_radius></pull_out_radius>                       <!-- [] (Number) how far pie slices should be pulled-out then user clicks on them (if left empty, uses 20% of pie radius) -->
    <pull_out_only_one></pull_out_only_one>                   <!-- [false] (true / false) if set to true, when you click on any slice, all other slices will be pushed in -->
  </animation>
  
  <data_labels>
    <radius></radius>                                         <!-- [30] (Number) distance of the labels from the pie. Use negative value to place labels on the pie -->
    <text_color></text_color>                                 <!-- [text_color] (hex color code) -->
    <text_size></text_size>                                   <!-- [text_size] (Number) -->
    <max_width></max_width>                                   <!-- [120] (Number) -->
    <show>
       <![CDATA[{title}:{percents}%]]>                        <!-- [] ({value} {title} {percents}) You can format any data label: {value} - will be replaced with value and so on. You can add your own text or html code too. -->
    </show>
    <show_lines></show_lines>                                 <!-- [true] (true / false) whether to show lines from slices to data labels or not -->                                                                                              
    <line_color></line_color>                                 <!-- [#000000] (hex color code) -->
    <line_alpha></line_alpha>                                 <!-- [15] (Number) -->
    <hide_labels_percent>0</hide_labels_percent>              <!-- [0] data labels of slices less then skip_labels_percent% will be hidden (to avoid label overlapping if there are many small pie slices)-->                                       
  </data_labels>

  <group>
    <percent></percent>
    <color></color>                                           <!-- [] (hex color code) color of "The others" slice -->                                       
    <title></title>                                           <!-- [Others] title of "The others" slice -->
    <url></url>                                               <!-- [] url of "The others" slice -->
    <description></description>                               <!-- [] description of "The others" slice -->        
    <pull_out></pull_out>                                     <!-- [false] (true / false) whether to pull out the other slice or not --> 
  </group>

  <background>                                                <!-- BACKGROUND -->
    <color></color>                                           <!-- [#FFFFFF] (hex color code) -->
    <alpha></alpha>                                           <!-- [0] (0 - 100) use 0 if you are using custom swf or jpg for background -->
    <border_color></border_color>                             <!-- [#FFFFFF] (hex color code) -->
    <border_alpha></border_alpha>                             <!-- [0] (0 - 100) -->
    <file></file>                                             <!-- [] (filename) swf or jpg file of a background. Do not use progressive jpg file, it will be not visible with flash player 7 -->
                                                              <!-- The chart will look for this file in path folder (path is set in HTML) -->
  </background>
  
  <balloon>                                                   <!-- BALLOON -->
    <enabled></enabled>                                       <!-- [true] (true / false) -->
    <color></color>                                           <!-- [] (hex color code) balloon background color. If empty, slightly darker then current slice color will be used -->
    <alpha>80</alpha>                                         <!-- [80] (0 - 100) -->
    <text_color></text_color>                                 <!-- [0xFFFFFF] (hex color code) -->
    <text_size></text_size>                                   <!-- [text_size] (Number) -->    
    <show>
       <![CDATA[{title}: {value} ({percents}%)]]>              <!-- [] ({value} {title} {percents}) You can format any data label: {value} - will be replaced with value and so on. You can add your own text or html code too. -->
    </show>
  </balloon>
    
  <legend>                                                    <!-- LEGEND -->
    <enabled></enabled>                                       <!-- [true] (true / false) -->
    <x></x>                                                   <!-- [40] (Number) -->
    <y></y>                                                   <!-- [] (Number) if empty, will be below the pie -->
    <width></width>                                           <!-- [] (Number) if empty, will be equal to flash width-80 -->
    <color>#FFFFFF</color>                                    <!-- [#FFFFFF] (hex color code) background color -->
    <max_columns></max_columns>                               <!-- [] (Number) the maximum number of columns in the legend -->
    <alpha>0</alpha>                                          <!-- [0] (0 - 100) background alpha -->
    <border_color>#000000</border_color>                      <!-- [#000000] (hex color code) border color -->
    <border_alpha>20</border_alpha>                           <!-- [0] (0 - 100) border alpha -->
    <text_color></text_color>                                 <!-- [text_color] (hex color code) -->   
    <text_size></text_size>                                   <!-- [text_size] (Number) -->
    <spacing>5</spacing>                                      <!-- [10] (Number) vertical and horizontal gap between legend entries -->
    <margins>5</margins>                                      <!-- [0] (Number) legend margins (space between legend border and legend entries, recommended to use only if legend border is visible or background color is different from chart area background color) -->    
    <key>                                                     <!-- KEY (the color box near every legend entry) -->
      <size>16</size>                                         <!-- [16] (Number) key size-->
      <border_color></border_color>                           <!-- [] (hex color code) leave empty if you don't want to have border -->
    </key>
    <values>                                                  <!-- VALUES -->          
      <enabled></enabled>                                     <!-- [false] (true / false) whether to show values near legend entries or not -->
      <width></width>                                         <!-- [] (Number) width of value text (use it if you want to align all values to the right, othervise leave empty) -->
      <text><![CDATA[]]></text>                               <!-- [{percents}%] ({value} {percents}) -->
     </values>    
  </legend>  
  
  <export_as_image>                                           <!-- export_as_image feature works only on a web server -->
    <file></file>
    <target></target>                                         <!-- [] (_blank, _top ...) target of a window in which export file must be called -->
    <x></x>                                                   <!-- [0] (Number) x position of "Collecting data" text -->
    <y></y>
    <color></color>                                           <!-- [#BBBB00] (hex color code) background color of "Collecting data" text -->
    <alpha></alpha>                                           <!-- [0] (0 - 100) background alpha -->
    <text_color></text_color>                                 <!-- [text_color] (hex color code) -->
    <text_size></text_size>                                   <!-- [text_size] (Number) -->
  </export_as_image>
  
  <error_messages>
    <enabled></enabled>                                       <!-- [true] (true / false) -->
    <x></x>                                                   <!-- [] (Number) x position of error message. If not set, will be aligned to the center -->
    <y></y>                                                   <!-- [] (Number) y position of error message. If not set, will be aligned to the center -->
    <color></color>                                           <!-- [#BBBB00] (hex color code) background color of error message -->
    <alpha></alpha>                                           <!-- [100] (0 - 100) background alpha -->
    <text_color></text_color>                                 <!-- [#FFFFFF] (hex color code) -->
    <text_size></text_size>                                   <!-- [text_size] (Number) -->
  </error_messages>    
  
  <strings>
    <no_data></no_data>                                       <!-- [No data for selected period] (text) if data is missing, this message will be displayed -->
    <export_as_image></export_as_image>                       <!-- [Export as image] (text) text for right click menu -->
    <collecting_data></collecting_data>                       <!-- [Collecting data] (text) this text is displayed while exporting chart to an image -->
  </strings>  
  
  <labels>                                                    <!-- LABELS -->
                                                              <!-- you can add as many labels as you want -->
    <label>
      <x>0</x>                                                <!-- [0] (Number) -->
      <y>10</y>                                               <!-- [0] (Number) -->
      <rotate>false</rotate>                                  <!-- [false] (true / false) -->
      <width></width>                                         <!-- [] (Number) if empty, will stretch from left to right untill label fits -->
      <align>center</align>                                   <!-- [left] (left / center / right) -->  
      <text_color></text_color>                               <!-- [text_color] (hex color code) button text color -->
      <text_size>12</text_size>
      <text>
        <![CDATA[<b><%=@name%></b>]]>
      </text>
    </label>
  </labels>
</settings>


# -*- coding: utf-8 -*-

#
# シリアル型
#

class RFSerial < RFBase
  def self.name(); "シリアル" end
  def self.type(); "serial" end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
  end
  # ソート用フィールド名を返す
  def order()
    db_name()
  end
  # データベースからの取り込み
  def from_db(db)
    @value = db[db_name()].to_i
    @table.oid = @value
  end
  # データベースの書き込みデータ
  def to_db()
    if(@value)
      @value.to_s
    else
      "DEFAULT"
    end
  end
end

__END__


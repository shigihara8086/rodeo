# -*- coding: utf-8 -*-

#
# 郵便番号型
#

class RFzipcode < RFString
  def self.name(); "郵便番号" end
  def self.type(); RFString.type() end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
  end
  # 設定
  def config_entry(su=true)
    super(su) {
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
  end
  # 絞りこみ
  def where_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"where_entry",binding)
      yield() if(block_given?)
    }
  end
  # 絞りこみの評価
  def eval_where()
    where = nil
    case @table.rodeo["#{@fid}.operator"]
    when "~"  then where = %Q("#{@name}" ~  '#{@table.rodeo["#{@fid}.data"].gsub(/-/,"").gsub(/\'/,"''")}')
    when "!~" then where = %Q("#{@name}" !~ '#{@table.rodeo["#{@fid}.data"].gsub(/-/,"").gsub(/\'/,"''")}')
    when "^"  then where = %Q("#{@name}" ~  '^#{@table.rodeo["#{@fid}.data"].gsub(/-/,"").gsub(/\'/,"''")}')
    when "$"  then where = %Q("#{@name}" ~  '#{@table.rodeo["#{@fid}.data"].gsub(/-/,"").gsub(/\'/,"''")}$')
    when "="  then where = %Q("#{@name}" =  '#{@table.rodeo["#{@fid}.data"].gsub(/-/,"").gsub(/\'/,"''")}')
    when "!=" then where = %Q("#{@name}" != '#{@table.rodeo["#{@fid}.data"].gsub(/-/,"").gsub(/\'/,"''")}')
    end
    where
  end
  #
  def eval_where_marked(roid)
    @table.select(roid)
    if(@value)
      where = %Q("#{@name}" = #{to_db(@value)})
    else
      where = nil
    end
    where
  end
  # CGIからの取り込み
  def from_cgi()
    super() {
      if(@table.rodeo.html_valid?(cgi_name()))
        z = @table.rodeo[cgi_name()].gsub(/-/,"")
        if(z != "")
          if(z.size == 7)
            @value = z
          else
            @error = "郵便番号は７桁で入力してください。"
          end
        else
          @value = nil
        end
      end
    }
  end
  # 入力
  def entry(opt={})
    opt = {} unless(opt)
    opt['accesskey'] = @conf['accesskey'] if(@conf['accesskey'] != "")
    super() {
      @table.rodeo.eruby(__FILE__,"zipcode_entry",binding)
    }
  end
  # 表示
  def display(list=false)
    super(list) {|list|
      if(@value)
        Rodeo.print @value[0..2]
        Rodeo.print "-"
        Rodeo.print @value[3..6]
      end
    }
  end
end

__END__

#### zipcode_entry

<script type="text/javascript">
  function keycheck_first_<%=@fid%>(e,ent) {
    var char = e.charCode;
    var key = e.keyCode;
    //------------------------------------------------- < GR0041 2008.09.11 Add Start tr >
    if(binfo.ie){
      if(char == undefined && key == 13) {
        document.getElementById("<%=cgi_name("1")%>").focus();
        event.returnValue = false;
      }
    }else{
    //------------------------------------------------- < GR0041 2008.09.11 Add End   tr >
      if(char == 0 && key == 13) {
        document.getElementById("<%=cgi_name("1")%>").focus();
        e.preventDefault();undefined
      }
    } //----------------------------------------------- < GR0041 2008.09.11 Add tr >
  }
  function keycheck_last_<%=@fid%>(e,ent) {
    var char = e.charCode;
    var key = e.keyCode;
    //------------------------------------------------- < GR0041 2008.09.11 Add Start tr >
    if(binfo.ie){
      if(char == undefined && key == 13) {
        var selection = document.selection.createRange();
        selection.setEndPoint("EndToStart", selection);
        selection.select();                           // 郵便番号のselectを解除
        for(var i = 0; i < ent.form.elements.length; i ++) {
          if(ent.form.elements[i] == ent) {
            ent.form.elements[i+1].focus();
          }
        }
        event.returnValue = false;
      }
    }else{
    //------------------------------------------------- < GR0041 2008.09.11 Add End   tr >
      if(char == 0 && key == 13) {
        for(var i = 0; i < ent.form.elements.length; i ++) {
          if(ent.form.elements[i] == ent) {
            ent.form.elements[i+1].focus();
          }
        }
        e.preventDefault();
      }
    } //----------------------------------------------- < GR0041 2008.09.11 Add tr >
  }
</script>
<%if(@value)%>
<!--
  <%@table.rodeo.html_text(cgi_name("0"),@value[0..2],{"size"=>3,"maxlength"=>3,"onkeypress"=>"keycheck_first_#{@fid}(event,this);"})%>-<%@table.rodeo.html_text(cgi_name("1"),@value[3..6],{"size"=>4,"maxlength"=>4,"onkeypress"=>"keycheck_last_#{@fid}(event,this);"})%>
-->
  <%@table.rodeo.html_text(cgi_name(),@value,{"size"=>8,"maxlength"=>8,"onkeypress"=>"keycheck_last_#{@fid}(event,this);"})%>
<%else%>
<!--
  <%@table.rodeo.html_text(cgi_name("0"),"",{"size"=>3,"maxlength"=>3,"onkeypress"=>"keycheck_first_#{@fid}(event,this);"})%>-<%@table.rodeo.html_text(cgi_name("1"),"",{"size"=>4,"maxlength"=>4,"onkeypress"=>"keycheck_last_#{@fid}(event,this);"})%>
-->
  <%@table.rodeo.html_text(cgi_name(),"",{"size"=>8,"maxlength"=>8,"onkeypress"=>"keycheck_last_#{@fid}(event,this);"})%>
<%end%>


#### where_entry

<td class="field_conf">
  <%@table.rodeo.html_text("#{@fid}.data","",{"size"=>8})%>
  <%
    @table.rodeo.html_radio("#{@fid}.operator","~") {
      @table.rodeo.html_item("~"  ,"を含む")
      @table.rodeo.html_item("!~" ,"を含まない")
      @table.rodeo.html_item("^"  ,"で始まる")
      @table.rodeo.html_item("$"  ,"で終わる")
      @table.rodeo.html_item("="  ,"に一致する")
      @table.rodeo.html_item("!=" ,"に一致しない")
    }
  %>
</td>


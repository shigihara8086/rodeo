# -*- coding: utf-8 -*-

#
# テンプレート入力
#

class RFtempentry < RFPlugin
  def self.name(); "テンプレート入力" end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
    @conf['template'] = "" unless(@conf['template'])
  end
  # 実行用アイコン
  def icon()
    "applications-system.png"
  end
  # 設定
  def config_entry(su=true)
    super(su,nil) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @conf['template'] = @table.rodeo["#{@fid}.template"]
  end
  # 表示
  def display(list)
    if(list)
      Rodeo.puts %Q(<a href="javascript:app_exec('#{@name}',#{@table.roid})">編集</a>)
    end
  end
  # アプリケーション画面の表示
  def action_app_exec()
    super() {
      @table.select(@table.rodeo.roid) if(@table.rodeo.roid and @table.rodeo.roid != 0)
      @error = "項目を入力してください" unless(@error)
      tmp = Tempfile.open("temp")
      tmp.puts("#### start")
      File.open("./template/#{@conf['template']}_entry.html","r") {|fd|
        fd.each_line {|line|
          # 入力項目の置換
          line.gsub!(/(%[^%]*%)/) {
            elem = $1[1..-2]
            if(elem.size > 0)
              if(@table.fields[elem])
                %Q(<%@table['#{elem}'].entry()%>)
              else
                case elem
                when /message/i
                  %Q(<span style="color:red;">#{@error}</span>)
                when /cancel/i
                  %Q(<%@table.rodeo.html_button("submit","キャンセル",{"style"=>"width:100%; height:100%;","onclick"=>"on_close();"})%>)
                when /ok/i
                  %Q(<%@table.rodeo.html_submit("submit","確認",{"style"=>"width:100%; height:100%;"})%>)
                else
                  %Q(<span style="color:red;">#{elem}</span>)
                end
              end
            else
              "%"
            end
          }
          # javascript挿入
          line.sub!(/(<\/head>)/i) {
          text  = %Q(\n<%@table.rodeo.html_rodeo_javascript()%>\n)
          text << %Q(<script type="text/javascript">\n)
          text << %Q(function on_close() {\n)
          text << %Q(  try {\n)
          text << %Q(    parent.list_shrink(null);\n)
          text << %Q(    parent.document.getElementById("rodeo_work").src = "blank.html";\n)
          text << %Q(    return(true);\n)
          text << %Q(  } catch(e) {\n)
          text << %Q(    window.close();\n)
          text << %Q(  }\n)
          text << %Q(}\n)
          text << %Q(</script>\n)
          text << %Q(</head>)
          text
          }
          line.sub!(/(<body[^>]*>)/i) {
            %Q(#{$1}\n<%@table.rodeo.html_form() {%>\n<%@table.rodeo.html_hidden("action","temp_confirm")%>)
          }
          line.sub!(/(<\/body>)/i) {
            %Q(<%}%>\n#{$1})
          }
          tmp.print(line)
        }
      }
      tmp.rewind()
      @table.rodeo.eruby(tmp.path,"start",binding)
      @table.rodeo.output()
      tmp.close(true)
    }
  end
  # 確認
  def action_temp_confirm()
    action = ""
    @table.clear()
    if(@table.rodeo.roid and @table.rodeo.roid != 0)
      @table.select(@table.rodeo.roid)
      action = "更新"
    else
      action = "登録"
    end
    if(@table.eval_cgi() > 0)
      @error = @table.error
      action_app_exec()
    else
      tmp = Tempfile.open("temp")
      tmp.puts("#### start")
      File.open("./template/#{@conf['template']}_confirm.html","r") {|fd|
        fd.each_line {|line|
          # 入力項目の置換
          line.gsub!(/(%[^%]*%)/) {
            elem = $1[1..-2]
            if(elem.size > 0)
              if(@table.fields[elem])
                %Q(<%@table['#{elem}'].display()%>\n<%@table['#{elem}'].hidden()%>)
              else
                case elem
                  when /message/i
                    %Q(<span style="color:red;">項目を確認してください</span>)
                when /cancel/i
                  %Q(<%@table.rodeo.html_button("submit","キャンセル",{"style"=>"width:100%; height:100%;","onclick"=>"on_close();"})%>)
                when /ok/i
                  %Q(<%@table.rodeo.html_submit("submit",action,{"style"=>"width:100%; height:100%;"})%>)
                else
                  %Q(<span style="color:red;">#{elem}</span>)
                end
              end
            else
              "%"
            end
          }
          # javascript挿入
          line.sub!(/(<\/head>)/i) {
            text  = %Q(\n<%@table.rodeo.html_rodeo_javascript()%>\n)
            text << %Q(<script type="text/javascript">\n)
            text << %Q(function on_close() {\n)
            text << %Q(  try {\n)
            text << %Q(    parent.list_shrink(null);\n)
            text << %Q(    parent.document.getElementById("rodeo_work").src = "blank.html";\n)
            text << %Q(    return(true);\n)
            text << %Q(  } catch(e) {\n)
            text << %Q(    window.close();\n)
            text << %Q(  }\n)
            text << %Q(}\n)
            text << %Q(</script>\n)
            text << %Q(</head>)
            text
          }
          line.sub!(/(<body[^>]*>)/i) {
            %Q(#{$1}\n<%@table.rodeo.html_form() {%>\n<%@table.rodeo.html_hidden("action","temp_done")%>)
          }
          line.sub!(/(<\/body>)/i) {
            %Q(<%}%>\n#{$1})
          }
          tmp.print(line)
        }
      }
      tmp.rewind()
      @table.rodeo.eruby(tmp.path,"start",binding)
      @table.rodeo.output()
      tmp.close(true)
    end
  end
  # 確認
  def action_temp_done()
    msg = ""
    @table.clear()
    if(@table.rodeo.roid and @table.rodeo.roid != 0)
      @table.select(@table.rodeo.roid)
      @table.eval_cgi()
      @table.update(@table.rodeo.roid)
      msg = "更新しました"
    else
      @table.insert_mode = true
      @table.eval_cgi()
      @table.insert()
      msg = "登録しました"
    end
    tmp = Tempfile.open("temp")
    tmp.puts("#### start")
    File.open("./template/#{@conf['template']}_done.html","r") {|fd|
      fd.each_line {|line|
        # 入力項目の置換
        line.gsub!(/(%[^%]*%)/) {
          elem = $1[1..-2]
          if(elem.size > 0)
            if(@table.fields[elem])
              %Q(<%@table['#{elem}'].display()%>)
            else
              case elem
                when /message/i
                  msg
              when /ok/i
                %Q(<%@table.rodeo.html_button("submit","閉じる",{"style"=>"width:100%; height:100%;","onclick"=>"on_close();"})%>)
              else
                %Q(<span style="color:red;">#{elem}</span>)
              end
            end
          else
            "%"
          end
        }
        # javascript挿入
        line.sub!(/(<\/head>)/i) {
          text  = %Q(\n<%@table.rodeo.html_rodeo_javascript()%>\n)
          text << %Q(<script type="text/javascript">\n)
          text << %Q(function on_close() {\n)
          text << %Q(  try {\n)
          text << %Q(    parent.list_shrink(null);\n)
          text << %Q(    parent.document.getElementById("rodeo_work").src = "blank.html";\n)
          text << %Q(    return(true);\n)
          text << %Q(  } catch(e) {\n)
          text << %Q(    window.close();\n)
          text << %Q(  }\n)
          text << %Q(}\n)
          text << %Q(</script>\n)
          text << %Q(</head>)
          text
        }
        line.sub!(/(<body[^>]*>)/i) {
          %Q(#{$1}\n<%@table.rodeo.html_form() {%>\n<%@table.rodeo.html_hidden("action","temp_done")%>)
        }
        line.sub!(/(<\/body>)/i) {
          %Q(<%}%>\n#{$1})
        }
        tmp.print(line)
      }
    }
    tmp.rewind()
    @table.rodeo.eruby(tmp.path,"start",binding)
    @table.rodeo.output()
    tmp.close(true)
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("テンプレート名","使用するテンプレートを設定します。")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.template",@conf['template'])%></td>
</tr>


# -*- coding: utf-8 -*-

#
# 整数型
#

class RFinet6 < RFInet6
  def self.name(); "IPv6アドレス" end
  def self.type(); RFInet6.type() end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
  end
  # クリア
  def clear()
    @value = nil
  end
  # 設定
  def config_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
  end
  # 絞りこみ
  def where_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"where_entry",binding)
      yield() if(block_given?)
    }
  end
  # 絞りこみの評価
  def eval_where()
    where = nil
    begin
      addr = IP6Addr.new(@table.rodeo["#{@fid}.data"])
      case @table.rodeo["#{@fid}.operator"]
      when "="  then where = %Q("#{@name}" =  '#{addr.to_astr}')
      when "!=" then where = %Q("#{@name}" != '#{addr.to_astr}')
      when "<"  then where = %Q("#{@name}" <  '#{addr.to_astr}')
      when "<=" then where = %Q("#{@name}" <= '#{addr.to_astr}')
      when ">"  then where = %Q("#{@name}" >  '#{addr.to_astr}')
      when ">=" then where = %Q("#{@name}" >= '#{addr.to_astr}')
      end
    rescue Exception
    end
    where
  end
  # 絞りこみの設定
  def eval_where_marked(roid)
    where = nil
    @table.select(roid)
    if(@value)
      where = %Q("#{@name}" = #{to_db()})
    end
    where
  end
  # CGIからの取り込み
  def from_cgi()
    super() {
      if(@table.rodeo[cgi_name()])
        if(@table.rodeo[cgi_name()] != "")
          begin
            @value = IP6Addr.new(@table.rodeo[cgi_name()])
          rescue Exception
            @error = "IPアドレスが間違っています"
          end
        else
          @value = nil
        end
      else
        @value = nil
      end
    }
  end
  # 入力
  def entry(opt={})
    opt = {} unless(opt)
    opt['size'] = "45"
    opt['accesskey'] = @conf['accesskey'] if(@conf['accesskey'] != "")
    super() {
      s = ""
      s = @value.to_astr if(@value)
      @table.rodeo.html_text(cgi_name(),s,opt)
    }
  end
  # 表示
  def display(list=false)
    super(list) {|list|
      if(@value)
        Rodeo.print "<tt>#{@value.to_astr}</tt>"
      end
    }
  end
  #
  def to_s()
    res = nil
    res = @value.to_astr if(@value)
    res
  end
end

__END__

#### config_entry

#### where_entry

<td class="field_conf">
  <%@table.rodeo.html_text("#{@fid}.data","",{"size"=>45})%>
  <%
    @table.rodeo.html_radio("#{@fid}.operator","=") {
      @table.rodeo.html_item("=" ,"等しい")
      @table.rodeo.html_item("!=","等しくない")
      @table.rodeo.html_item("<" ,"小さい")
      @table.rodeo.html_item("<=","以下")
      @table.rodeo.html_item(">" ,"大きい")
      @table.rodeo.html_item(">=","以上")
    }
  %>
</td>


# -*- coding: utf-8 -*-

#
# 画像型
#

class RFimage < RFBase
  def self.name(); "画像" end
  def self.type(); ["text"] end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
    @conf['path'] = "" unless(@conf['path'])
    @path = "upload"
    @path = "upload/#{@conf['path']}" if(@conf['path'] != "")
    @value = nil
  end
  # 設定
  def config_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @conf['path'] = @table.rodeo["#{@fid}.path"]
  end
  # CGIデータの評価
  def from_cgi()
    super() {
      if(@table.rodeo[cgi_name()] != "")
        @value = @table.rodeo[cgi_name()]
      else
        @value = nil
      end
    }
  end
  # データベースからの取り込み
  def from_db(db)
    if(db[db_name("0")])
      @value = db[db_name("0")]
    else
      @value = nil
    end
  end
  # データベースへの書き込みデータの作成
  def to_db()
    if(@value != nil and @value != "")
      ["'"+@value.gsub(/\'/,"''")+"'"]
    else
      [nil]
    end
  end
  # CSV
  def to_csv()
    %Q("#{@value}")
  end
  #
  def from_csv(data)
    @value = data
  end
  # 入力
  def entry(opt={})
    opt = {} unless(opt)
    opt['accesskey'] = @conf['accesskey'] if(@conf['accesskey'] != "")
    super() {
      @table.rodeo.eruby(__FILE__,"image_entry",binding)
    }
  end
  # 表示
  def display(list=false)
    super(list) {|list|
      if(list)
        @table.rodeo.eruby(__FILE__,"image_list",binding)
      else
        @table.rodeo.eruby(__FILE__,"image_display",binding)
      end
    }
  end
end

class RFimage < RFBase
  # アップローダ
  def action_image_uploader()
    file = nil
    name = @table.rodeo['value']
    @table.rodeo.eruby(__FILE__,"image_uploader",binding)
    @table.rodeo.output()
  end
  # アップローダ
  def action_do_image_upload()
    file = @table.rodeo[cgi_name("__image__").force_encoding("ASCII-8BIT")]
    name = file.original_filename.force_encoding(__ENCODING__)
    if(name =~ /\\/)
      name = name.split(/\\/)[-1]
    end
    name = "img_"+name
    if(name != nil and name != "")
      if(@conf['path'] != "")
        if(not File.exist?("upload/#{@conf['path']}"))
          `mkdir -p upload/#{@conf['path']}`
        end
      end
      # 名前の重複をチェック
      while File.exist?("#{@path}/#{name.untaint}")
        ext = File.extname(name)
        base = File.basename(name,ext)
        if(base !~ /.*?[0-9]+$/)
          base << "_00"
        else
          base.succ!
        end
        name = base + ext
      end
      File.open("#{@path}/#{name.untaint}","w") {|fd|
        fd.write(file.read())
        fd.chmod(0664)
      }
    end
    @table.rodeo.eruby(__FILE__,"image_uploader",binding)
    @table.rodeo.output()
  end
  # アップローダ
  def action_do_image_clear()
    file = nil
    name = ""
    @table.rodeo.eruby(__FILE__,"image_uploader",binding)
    @table.rodeo.output()
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf">保存先</th>
  <td class="field_conf">
    <%@table.rodeo.html_text("#{@fid}.path",@conf['path'],{"style"=>"width:100%"})%><br>
  </td>
</tr>

#### image_entry

<%if(ENV['HTTP_USER_AGENT'] =~ /iphone/i)%>
<%@table.rodeo.html_hidden(cgi_name(),@value)%>
<%else%>
<%@table.rodeo.html_hidden(cgi_name(),@value)%>
<img id="image_area_<%=@fid%>" src="image/enable/blank.png">
<iframe class="uploader" id="image_uploader_<%=@fid%>" frameborder="0"></iframe>
<script type="text/javascript">
  var f = function() {
    var arg = sysargs({
      "action":"image_uploader",
      "field":"<%=@name%>",
      "fid":"<%=@fid%>",
      "value":"<%=@value%>"
    });
    frame = document.getElementById("image_uploader_<%=@fid%>");
    frame.src = rodeo_appname+"?"+arg;
  }
  on_load_fook[on_load_fook.length] = f;
</script>
<%end%>

#### image_uploader

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@table.rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_load_func() {
        frame = parent.document.getElementById("image_uploader_<%=@fid%>");
        try {
          h = document.body.offsetHeight;
          if(h == 0) {
            h = document.body.scrollHeight + 4;
          }
          frame.style.height = h;
        } catch(e) {
        }
        img = parent.document.getElementById("image_area_<%=fid%>");
        val = parent.document.getElementById("<%=cgi_name()%>");
        name = "<%=name%>";
        if(name != "") {
          img.src = "<%=@path%>/"+name;
          val.value = name;
        } else {
          img.src = "image/enable/blank.png";
          val.value = "";
        }
      }
      function auto_submit() {
        form = document.getElementById("form_#{@fid}").submit();
        form.submit();
      }
      function image_clear() {
        img = parent.document.getElementById("image_area_<%=@fid%>");
        val = parent.document.getElementById("<%=cgi_name()%>");
        img.src = "image/enable/blank.png";
        val.value = "";
      }
    </script>
  </head>
  <body class="margin0" onload="on_load_func();">
    <table class="margin0">
      <tr>
        <td>
          <%@table.rodeo.html_form(@table.rodeo.appname,
            {"id"=>"form_#{@fid}","enctype"=>"multipart/form-data","style"=>"margin:0px;"}) {%>
            <%@table.rodeo.html_hidden("action","do_image_upload")%>
            <%@table.rodeo.html_hidden("fid",@table.rodeo['fid'])%>
            <%@table.rodeo.html_file(cgi_name("__image__"),"",
              {"size"=>50,"onchange"=>"document.getElementById('form_#{@fid}').submit();"})%>
          <%}%>
        </td>
        <td>
          <%@table.rodeo.html_form(@table.rodeo.appname) {%>
            <%@table.rodeo.html_hidden("action","do_image_clear")%>
            <%@table.rodeo.html_button("image_submit","消去",{"onclick"=>"image_clear();"})%>
          <%}%>
        </td>
      </tr>
    </table>
  </body>
</html>

#### image_list

<%if(@value)%>
<a href="<%=@path%>/<%=@value%>">
  <img class="icon"
   src="image/enable/camera-photo.png"
   onclick="return false;"
   onMouseOver="return overlib('<img src=\\'<%=@path%>/<%=@value%>\\'',WIDTH,1,HEIGHT,1,FGCOLOR,'#FFFFFF')"
   onMouseOut="return nd()"></a><%=@value%>
<%end%>

#### image_display

<%if(ENV['HTTP_USER_AGENT'] =~ /iphone/i)%>
  <img src="<%=@path%>/<%=@value%>" title="<%=@value%>" height="32">
<%else%>
<%if(@value)%>
  <img src="<%=@path%>/<%=@value%>" title="<%=@value%>">
<%end%>
<%end%>


# -*- coding: utf-8 -*-

#
# 文字列型
#

class RFstring < RFString
  def self.name(); "文字列" end
  def self.type(); RFString.type() end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
    @conf['default'] = nil    unless(@conf['default'])
    @conf['cols']    = "100%" unless(@conf['cols'])
    @conf['jfazzy']  = false  unless(@conf['jfazzy'])
=begin
    this = self
    eval(File.open("#{__FILE__}.mod/default.rb","r").read().untaint,binding)
    if(name == "test")
      eval(File.open("#{__FILE__}.mod/bold.rb","r").read().untaint,binding)
    end
=end
  end
  # データのクリア
  def clear()
    @value = @conf['default']
  end
  # 設定
  def config_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @conf['default'] = @table.rodeo["#{@fid}.default"]
    @conf['cols'] = @table.rodeo["#{@fid}.cols"]
    @conf['jfazzy'] = (@table.rodeo["#{@fid}.jfazzy"] == "true") ? true : false
  end
  # 絞りこみ
  def where_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"where_entry",binding)
      yield() if(block_given?)
    }
  end
  # 絞りこみの評価
  def eval_where()
    where = nil
    if(@conf['jfazzy'] == true)
      code = @table.rodeo["#{@fid}.data"].gsub(/\'/,"''")
      code.gsub!(/\^/,"\\^")
      code.gsub!(/\$/,"\\$")
      code.gsub!(/\[/,"\\[")
      code.gsub!(/\]/,"\\]")
      code.gsub!(/\./,"\\.")
      code.gsub!(/\*/,"\\*")
      code.gsub!(/\+/,"\\+")
      code.gsub!(/\-/,"\\-")
      code.gsub!(/\\/,"\\\\")
      code.gsub!(/\?/,"\\?")
      code.gsub!(/\(/,"\\(")
      code.gsub!(/\)/,"\\)")
      code.gsub!(/\'/,"\\'")
      case @table.rodeo["#{@fid}.operator"]
      when "~"  then where = %Q(jfazzy("#{@name}") ~  jfazzy('#{code}'))
      when "!~" then where = %Q(jfazzy("#{@name}") !~ jfazzy('#{code}'))
      when "^"  then where = %Q(jfazzy("#{@name}") ~  jfazzy('^#{code}'))
      when "$"  then where = %Q(jfazzy("#{@name}") ~  jfazzy('#{code}$'))
      when "="  then where = %Q(jfazzy("#{@name}") =  jfazzy('#{code}'))
      when "!=" then where = %Q(jfazzy("#{@name}") != jfazzy('#{code}'))
      end
    else
      code = @table.rodeo["#{@fid}.data"].gsub(/\'/,"''")
      code.gsub!(/\^/,"\\^")
      code.gsub!(/\$/,"\\$")
      code.gsub!(/\[/,"\\[")
      code.gsub!(/\]/,"\\]")
      code.gsub!(/\./,"\\.")
      code.gsub!(/\*/,"\\*")
      code.gsub!(/\+/,"\\+")
      code.gsub!(/\-/,"\\-")
      code.gsub!(/\\/,"\\\\")
      code.gsub!(/\?/,"\\?")
      code.gsub!(/\(/,"\\(")
      code.gsub!(/\)/,"\\)")
      code.gsub!(/\'/,"\\'")
      case @table.rodeo["#{@fid}.operator"]
      when "~"  then where = %Q("#{@name}" ~  '#{code}')
      when "!~" then where = %Q("#{@name}" !~ '#{code}')
      when "^"  then where = %Q("#{@name}" ~  '^#{code}')
      when "$"  then where = %Q("#{@name}" ~  '#{code}$')
      when "="  then where = %Q("#{@name}" =  '#{code}')
      when "!=" then where = %Q("#{@name}" != '#{code}')
      end
    end
    where
  end
  # 絞りこみの設定
  def eval_where_marked(roid)
    @table.select(roid)
    if(@value)
      where = %Q("#{@name}" = #{to_db()})
    else
      where = nil
    end
    where
  end
  # CGIからの取り込み
  def from_cgi()
    super() {
	    if(@table.rodeo[cgi_name()])
	      @value = @table.rodeo[cgi_name()]
	    end
	    @value = nil if(@value == "")
	    yield() if(block_given?)
    }
  end
  # 入力
  def entry(opt={})
    opt = {} unless(opt)
    super() {
      if(@conf['cols'] =~ /\%/)
        opt['style'] = "width:#{@conf['cols']}"
      else
        opt['size'] = "#{@conf['cols']}"
      end
      if(@conf['accesskey'] !="")
        opt['accesskey'] = @conf['accesskey']
      end
      @table.rodeo.html_text(cgi_name(),@value,opt)
    }
  end
  # 表示
  def display(list=false)
    super(list) {|list|
      if(@value)
        Rodeo.print CGI::escapeHTML(@value)
      end
    }
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("初期値")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.default",@conf['default'],{"size"=>10})%></td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("入力桁数","\"%\"で指定した場合、\"style\"属性のwidth設定になります。")%></th>
  <td class="field_conf"><%@table.rodeo.html_number("#{@fid}.cols",@conf['cols'],{"size"=>10})%></td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("あいまい検索")%></th>
  <td class="field_conf">
    <%
      @table.rodeo.html_radio("#{@fid}.jfazzy",@conf['jfazzy']) {
        @table.rodeo.html_item(true ,"する")
        @table.rodeo.html_item(false,"しない")
      }
    %>
  </td>
</tr>

#### where_entry

<td class="field_conf">
  <%@table.rodeo.html_text("#{@fid}.data","",{"size"=>20})%>
  <%
    @table.rodeo.html_radio("#{@fid}.operator","~") {
      @table.rodeo.html_item("~"  ,"を含む")
      @table.rodeo.html_item("!~" ,"を含まない")
      @table.rodeo.html_item("^"  ,"で始まる")
      @table.rodeo.html_item("$"  ,"で終わる")
      @table.rodeo.html_item("="  ,"に一致する")
      @table.rodeo.html_item("!=" ,"に一致しない")
    }
  %>
</td>


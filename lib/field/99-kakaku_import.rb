# -*- coding: utf-8 -*-

#
# 価格.comインポート型
#

require 'csv'

class RFkakaku_import < RFPlugin
  def self.name(); "価格.comインポート" end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
  end
  # 実行用アイコン
  def icon()
    "applications-system.png"
  end
  # 設定
  def config_entry(su=true)
    super(su,[]) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # アプリケーション画面の表示
  def action_app_exec()
    super() {
      @table.rodeo.eruby(__FILE__,"app_exec",binding)
      @table.rodeo.output()
    }
  end
  # テーブルの更新
  def csv_update(rec)
    if(rec['roid'])
      @table.select(rec['roid'])
      rec.each {|k,v|
        if(k != "roid")
          v = nil if(v == "")
          @table[k].from_csv(v)
        end
      }
      @table.insert_mode = false
      @table.update(rec['roid'])
      @message << "roid:#{rec['roid']}を更新しました。<br>\n"
    else
      @table.clear()
      rec.each {|k,v|
        if(k != "roid")
          v = nil if(v == "")
          @table[k].from_csv(v)
        end
      }
      @table.insert_mode = true
      roid = @table.insert()
      @message << "roid:#{roid}を追加しました。<br>\n"
    end
  end
  # CSV取り込み
  def action_csv_upload()
    @error = ""
    @message = ""
    @maker = {}
    @item = []
    su = Iconv.new("UTF8","CP932")
    file = @table.rodeo[cgi_name("__image__")]
    name = file.original_filename
    if(name =~ /\\/)
      name = name.split(/\\/)[-1]
    end
    if(name != nil and name != "")
      File.open("upload/#{name.untaint}","w") {|fd|
        fd.write(su.iconv(file.read))
      }
      first = true
      CSV.open("upload/#{name.untaint}","r") {|row|
        if(first)
          row.each {|col| @item << col }
          first = false
        else
          data = {}
          row.each_index {|i|
            data[@item[i]] = row[i]
          }
          @maker[data['メーカー']] = {} unless(@maker[data['メーカー']])
          if(data['価格.comの製品名・型番'])
            # 通常のサイズ表記
            if(data['価格.comの製品名・型番'] =~ /^(.*\s+\S*)(\d{3})\/(\d{2})([ZR]+)(\d{2}.*)\s+(\d{2,3}.)(\s*.*)$/)
              series = $1.strip
              width = $2.to_i
              ratio = $3.to_i
              type = $4
              size = $5.to_i
              liss = "#{$6}#{$7}"
              series.sub!(/\sP$/,"")
              @maker[data['メーカー']][series] = [] unless(@maker[data['メーカー']][series])
              @maker[data['メーカー']][series] << {
                '商品名'       =>data['価格.comの製品名・型番'],
                'サイズ'       =>[width,ratio,type,size,liss],
                'JAN'          =>data['JAN'],
                '登録価格'     =>data['登録価格'],
                '送料'         =>data['送料'],
                '在庫・発送'   =>data['在庫・発送'],
                '店頭'         =>data['店頭'],
                'カテゴリ'     =>data['カテゴリ'],
                '製品名・型番' =>data['製品名・型番'],
                'リンク先URL'  =>data['リンク先URL'],
                '画像URL'      =>data['画像URL'],
                'ポイント'     =>data['ポイント'],
                '一言コメント' =>data['一言コメント'],
              }
            # バン・小型トラック用のサイズ表記
            elsif(data['価格.comの製品名・型番'] =~ /^(.*\s+\S*)(\d{3})([ZR]+)(\d{2})\s+(\d{1,2}PR)$/)
              series = $1.strip
              width = $2.to_i
              ratio = 0
              type = $3
              size = $4.to_i
              liss = $5
              @maker[data['メーカー']][series] = [] unless(@maker[data['メーカー']][series])
              @maker[data['メーカー']][series] << {
                '商品名'       =>data['価格.comの製品名・型番'],
                'サイズ'       =>[width,ratio,type,size,liss],
                'JAN'          =>data['JAN'],
                '登録価格'     =>data['登録価格'],
                '送料'         =>data['送料'],
                '在庫・発送'   =>data['在庫・発送'],
                '店頭'         =>data['店頭'],
                'カテゴリ'     =>data['カテゴリ'],
                '製品名・型番' =>data['製品名・型番'],
                'リンク先URL'  =>data['リンク先URL'],
                '画像URL'      =>data['画像URL'],
                'ポイント'     =>data['ポイント'],
                '一言コメント' =>data['一言コメント'],
              }
            # インチ幅のサイズ表記
            elsif(data['価格.comの製品名・型番'] =~ /^(.*\s+\S*)([\d\.]{3,4})\-(\d{2})\s+(\d{1,2}PR)$/)
              series = $1.strip
              width = ($2.to_f * 2.54).to_i
              ratio = 0
              type = "-"
              size = $3.to_i
              liss = $4
              @maker[data['メーカー']][series] = [] unless(@maker[data['メーカー']][series])
              @maker[data['メーカー']][series] << {
                '商品名'       =>data['価格.comの製品名・型番'],
                'サイズ'       =>[width,ratio,type,size,liss],
                'JAN'          =>data['JAN'],
                '登録価格'     =>data['登録価格'],
                '送料'         =>data['送料'],
                '在庫・発送'   =>data['在庫・発送'],
                '店頭'         =>data['店頭'],
                'カテゴリ'     =>data['カテゴリ'],
                '製品名・型番' =>data['製品名・型番'],
                'リンク先URL'  =>data['リンク先URL'],
                '画像URL'      =>data['画像URL'],
                'ポイント'     =>data['ポイント'],
                '一言コメント' =>data['一言コメント'],
              }
            else
              @error << "解析エラー：#{data['メーカー']},#{data['価格.comの製品名・型番']}<br>\n"
            end
          end
        end
      }
    end
    if(@error != "")
      @table.rodeo.eruby(__FILE__,"upload_error",binding)
      @table.rodeo.output()
    else
      @table.delete_all(nil)
      tmaker = @table.rodeo.table_new("タイヤ-メーカー")
      tseries = @table.rodeo.table_new("タイヤ-シリーズ")
      zaiko = @table.rodeo.table_new("タイヤ-在庫")
      radial = @table.rodeo.table_new("タイヤ-ラジアル構造")
      # メーカー
      @maker.each {|maker,shash|
        troid = nil
        unless((troid = tmaker.find("タイヤメーカー",maker)))
          tmaker.insert_mode = true
          tmaker['roid'].value = nil
          tmaker['タイヤメーカー'].value = maker
          troid = tmaker.insert()
        end
        # シリーズ
        shash.each {|series,tarray|
          sroid = nil
          unless((sroid = tseries.find("タイヤシリーズ",series)))
            tseries.insert_mode = true
            tseries['roid'].value = nil
            cat = 1
            tarray.each {|data|
              cat = 2 if(data['カテゴリ'] =~ /スタッドレス/)
              cat = 3 if(data['カテゴリ'] =~ /オール/)
            }
            tseries['種別'].value           = [cat]
            tseries['タイヤメーカー'].value = [troid]
            tseries['タイヤシリーズ'].value = series
            sroid = tseries.insert()
          end
          # 各種サイズ
          tarray.each {|data|
            spec = data['サイズ'] # [width,ratio,type,size,liss]
            where  = %Q("タイヤメーカー" = '{#{troid}}' and "タイヤシリーズ" = '{#{sroid}}' and)
            where << %Q("タイヤ幅" = #{spec[0]} and "偏平率" = #{spec[1]} and "リム径" = #{spec[3]})
            if(@table.count(where) == 0)
              @table.insert_mode = true
              @table['roid'].value = nil
              @table['掲載'].value = true
              @table['価格com品名'].value = data['商品名']
              @table['タイヤメーカー'].value = [troid]
              @table['タイヤシリーズ'].value = [sroid]
              @table['タイヤ幅'].value = spec[0]
              @table['偏平率'].value = spec[1]
              @table['ラジアル構造'].value = nil
              if(radial.find("ラジアル構造",spec[2]))
                @table['ラジアル構造'].value = [radial.roid]
              end
              @table['リム径'].value = spec[3]
              @table['LI/SS'].value = spec[4]
              @table['リム幅'].value = nil
              @table['外径'].value = nil
              @table['単価'].value = data['登録価格'].to_i
              @table['在庫'].value = nil
              if(zaiko.find("価格comコード",data['在庫・発送']))
                @table['在庫'].value = [zaiko.roid]
              end
              @table['送料'].value = data['送料'].to_i
              @table['コメント'].value = data['一言コメント']
              @table['リンク先URL'].value = data['リンク先URL']
              @table['画像URL'].value = data['画像URL']
              @table['JAN'].value = data['JAN']
              droid = @table.insert()
            else
            end
          }
        }
      }
      @message = nil
      @table.rodeo.eruby(__FILE__,"upload_done",binding)
      @table.rodeo.output()
    end
  end
end

__END__

#### config_entry

<!-- config -->

#### app_exec

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%@table.rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_close() {
        parent.list_shrink(null);
        parent.document.getElementById("rodeo_work").src = "blank.html";
      }
    </script>
  </head>
  <body>
    <table class="record_spec" width="100%"><tr>
      <th class="record_spec" width="100%"></th>
      <th class="record_spec" nowrap><%@table.rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
      <th class="record_spec"><%@table.rodeo.html_submit("exec","実行(R)",{"accesskey"=>"R"})%></th>
    </tr></table>
    CSVファイルを取り込みます。<br>
    CSVファイルを選択してください。<br>
    <%@table.rodeo.html_form(@table.rodeo.appname,{"enctype"=>"multipart/form-data","style"=>"margin:0px;"}) {%>
      <%@table.rodeo.html_hidden("action","csv_upload")%>
      <%@table.rodeo.html_file(cgi_name("__image__"),"",{"size"=>60})%>
      <table class="record_spec" width="100%"><tr>
        <th class="record_spec" width="100%"></th>
        <th class="record_spec" nowrap><%@table.rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
        <th class="record_spec"><%@table.rodeo.html_submit("exec","実行(R)",{"accesskey"=>"R"})%></th>
      </tr></table>
    <%}%>
  </body>
</html>

#### upload_done

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%@table.rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_close() {
        parent.list_shrink(null);
        parent.document.getElementById("rodeo_work").src = "blank.html";
      }
    </script>
  </head>
  <body onload="parent.record_list_update(true,true,true,true,null);">
    <table class="record_spec" width="100%"><tr>
      <th class="record_spec" width="100%"></th>
      <th class="record_spec" nowrap><%@table.rodeo.html_button("submit","閉じる(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
    </tr></table>
    CSVファイルの取り込みを完了しました。<br>
    <%=@message%>
    <table class="record_spec" width="100%"><tr>
      <th class="record_spec" width="100%"></th>
      <th class="record_spec" nowrap><%@table.rodeo.html_button("submit","閉じる(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
    </tr></table>
  </body>
</html>

#### upload_error

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%@table.rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_close() {
        parent.list_shrink(null);
        parent.document.getElementById("rodeo_work").src = "blank.html";
      }
    </script>
  </head>
  <body>
    <table class="record_spec" width="100%"><tr>
      <th class="record_spec" width="100%"></th>
      <th class="record_spec" nowrap><%@table.rodeo.html_button("submit","閉じる(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
    </tr></table>
    CSVファイルの取り込みに失敗しました。<br>
    [[[ エラー内容 ]]]<br>
    <font color="red"><%=@error%></font><br>
    <table class="record_spec" width="100%"><tr>
      <th class="record_spec" width="100%"></th>
      <th class="record_spec" nowrap><%@table.rodeo.html_button("submit","閉じる(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
    </tr></table>
  </body>
</html>


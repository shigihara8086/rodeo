# -*- coding: utf-8 -*-

#
# 日付時刻型
#

require 'time'

class RFTimestamp < RFBase
  def self.name(); "日付時刻" end
  def self.type(); "timestamp" end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
  end
  # 並び順
  def order()
    db_name()
  end
  # 設定の評価
  def eval_config()
    super()
  end
  # データベースからの取り込み
  def from_db(db)
    if(db[db_name()] =~ /^(\d{4})-(\d{2})-(\d{2})\s+(\d{2}):(\d{2}):(\d{2})$/)
      @value = Time.local($1.to_i,$2.to_i,$3.to_i,$4.to_i,$5.to_i,$6.to_i)
    end
  end
  # データベースへの書き込みデータの作成
  def to_db()
    if(@value)
      %Q('#{@value.strftime("%Y-%m-%d %H:%M:%S")}')
    else
      nil
    end
  end
  # CGIからの取り込み
  def from_cgi()
    super() {
      if(@table.rodeo[cgi_name("hidden")] =~ /^(\d{4})-(\d{2})-(\d{2})\s+(\d{2}):(\d{2}):(\d{2})$/)
        begin
          @value = Time.local($1.to_i,$2.to_i,$3.to_i,$4.to_i,$5.to_i,$6.to_i)
        rescue Exception
          if(not @conf['valid_nil'])
            @error = "日付が正しくありません。"
          else
            @value = nil
          end
        end
      else
        yield() if(block_given?)
      end
		}
  end
  # データベースからの取り込み
  def from_csv(data)
    if(data =~ /^(\d{4})-(\d{2})-(\d{2})\s+(\d{2}):(\d{2}):(\d{2})$/)
      @value = Time.local($1.to_i,$2.to_i,$3.to_i,$4.to_i,$5.to_i,$6.to_i)
    end
  end
  def to_csv()
    if(@value)
      %Q("#{@value.strftime("%Y-%m-%d %H:%M:%S")}")
    else
      %Q("")
    end
  end
  #
  def hidden()
    if(@value)
      @table.rodeo.html_hidden(cgi_name("hidden"),@value.strftime("%Y-%m-%d %H:%M:%S"))
    end
  end
  #
  def max()
    @value = nil
    where = ""
    if(@table.where_string()) then where = " where #{@table.where_string()}"; end
    @table.rodeo.sql.exec(%Q(select max("#{@name}") from "rd_#{@table.name}"#{where})) {|res|
      if(res[0]['max'])
        if(res[0]['max'] =~ /^(\d{4})-(\d{2})-(\d{2})\s+(\d{2}):(\d{2}):(\d{2})$/)
          @value = Time.local($1.to_i,$2.to_i,$3.to_i,$4.to_i,$5.to_i,$6.to_i)
        end
      end
    }
    @value
  end
  def min()
    @value = nil
    where = ""
    if(@table.where_string()) then where = " where #{@table.where_string()}"; end
    @table.rodeo.sql.exec(%Q(select min("#{@name}") from "rd_#{@table.name}"#{where})) {|res|
      if(res[0]['min'])
        if(res[0]['min'] =~ /^(\d{4})-(\d{2})-(\d{2})\s+(\d{2}):(\d{2}):(\d{2})$/)
          @value = Time.local($1.to_i,$2.to_i,$3.to_i,$4.to_i,$5.to_i,$6.to_i)
        end
      end
    }
    @value
  end
end

__END__


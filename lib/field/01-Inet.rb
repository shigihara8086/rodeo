# -*- coding: utf-8 -*-

#
# IPアドレス型
#

class IPAddr
  def initialize(str)
    ips, mlens = str.split(/\//)
    raise ArgumentError if(ips !~ /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/)
    ip = ips.split(/\./).inject(0) {|r, n| r * 256 + n.to_i }
    if(mlens.nil?)
      mlen = 32
#      mlen = case
#             when (ip >> 24) & 0x80 == 0x00; 8
#             when (ip >> 24) & 0xc0 == 0x80; 16
#             when (ip >> 24) & 0xf0 == 0xc0; 24
#             else; 0
#             end
    else
      mlen = mlens.to_i
    end
    nmask = (0 ... mlen).inject(0) {|r,n| (1 << 31) | (r >> 1) }
    bcast = (ip & nmask) | ~nmask
    net   = ip & nmask
    @ip = ip
    @mlen = mlen
    @nmask = nmask
    @bcast = bcast
    @net = net
  end
  def addrs(n)
    [
      (n >> 24) & 0xff,
      (n >> 16) & 0xff,
      (n >>  8) & 0xff,
      (n >>  0) & 0xff
    ]
  end
  def to_astr
    if(@mlen == 32)
      addrs(@ip).map {|i| i.to_s }.join('.')
    else
      addrs(@ip).map {|i| i.to_s }.join('.')+"/"+@mlen.to_s
    end
  end
end

class RFInet < RFBase
  def self.name(); "IPアドレス" end
  def self.type(); "inet" end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
  end
  # ソート用フィールド名を返す
  def order()
    db_name()
  end
  # 設定
  def config_entry(su=true)
    super(su) {
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
  end
  # データベースからの取り込み
  def from_db(db)
    if(db[db_name])
      @value = IPAddr.new(db[db_name()])
    else
      @value = nil
    end
  end
  # データベースへの書き込みデータの作成
  def to_db()
    if(@value)
      %Q('#{@value.to_astr}')
    else
      nil
    end
  end
  # CGIからの取り込み
  def from_cgi()
    super() {
      if(@table.rodeo[cgi_name("hidden")] != "")
        @value = IPAddr.new(@table.rodeo[cgi_name("hidden")])
      else
        yield() if(block_given?)
      end
		}
  end
  # データベースからの取り込み
  def from_csv(data)
    @value = IPAddr.new(data)
  end
  def to_csv()
    if(@value)
      %Q("#{@value.to_astr}")
    else
      %Q("")
    end
  end
  #
  def hidden()
    if(@value)
      @table.rodeo.html_hidden(cgi_name("hidden"),@value.to_astr)
    end
  end
end

__END__


# -*- coding: utf-8 -*-

#
# 設問回答型
#

require "#{File.dirname(__FILE__)}/../email.rb"

class RFexam < RFPlugin
  def self.name(); "設問回答" end
  # 実行用アイコン
  def icon()
    "applications-system.png"
  end
  # 設定
  def config_entry(su=true)
    super(su,[]) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @conf['enq_title'] = @table.rodeo["#{@fid}.enq_title"]
    @conf['enq_time'] = @table.rodeo["#{@fid}.enq_time"].to_i
  end
  # アプリケーション画面の表示
  def action_app_exec()
    super() {
      @table.clear()
#      @table.find("回答者",%Q({#{@table.rodeo.user_record['roid'].value}}))
      @table.rodeo.eruby(__FILE__,"exam_start",binding)
      @table.rodeo.output()
    }
  end
  # アプリケーション画面の表示
  def action_exam_done()
=begin
    roid = @table.find("回答者",%Q({#{@table.rodeo.user_record['roid'].value}}))
    if(roid)
      @table.eval_cgi()
      @table['回答者'].value = [@table.rodeo.user_record['roid'].value]
      point = 0
      total = 0
      exam = @table.rodeo.table_new(@table.name+"問題")
      exam.fetch(0,exam.count(),nil,%Q("設問ID")) {|roid2|
        master = @table.rodeo.table_new(@table[exam['設問ID'].value].conf['master_table'])
        right = exam['正解'].value.split(/[, ]/).sort.join(",")
        array = []
        @table[exam["設問ID"].value].value.each {|v|
          master.select(v)
          array << master['選択肢'].value
        }
        ans = array.sort.join(",")
        total += exam["配点"].value
        point += exam["配点"].value if(ans == right)
      }
      @table['得点'].value = point
      @table.update(roid)
    else
=end
      @table.insert_mode = true
      @table.eval_cgi()
      @table['回答者'].value = [@table.rodeo.user_record['roid'].value]
      point = 0
      total = 0
      exam = @table.rodeo.table_new(@table.name+"問題")
      exam.fetch(0,exam.count(),nil,%Q("設問ID")) {|roid2|
        master = @table.rodeo.table_new(@table[exam['設問ID'].value].conf['master_table'])
        right = exam['正解'].value.split(/[, ]/).sort.join(",")
        array = []
        @table[exam["設問ID"].value].value.each {|v|
          master.select(v)
          array << master['選択肢'].value
        }
        ans = array.sort.join(",")
        total += exam["配点"].value
        point += exam["配点"].value if(ans == right)
      }
      @table['得点'].value = point
      @table.insert()
=begin
    end
=end
    color = "green"
    color = "yellow" if(point < (total / 3 * 2))
    color = "red"    if(point < (total / 3 * 1))
    @table.rodeo.eruby(__FILE__,"exam_done",binding)
    @table.rodeo.output()
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("試験タイトル","試験のタイトルを設定します。")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.enq_title",@conf['enq_title'],{"style"=>"width:100%"})%></td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("制限時間","試験の制限時間を設定します。")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.enq_time",@conf['enq_time'],{"size"=>5})%>分</td>
</tr>

#### exam_start

<html>
  <head>
    <title><%=@conf['enq_title']%></title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
    <script type="text/javascript">
      var timeleft = <%=@conf['enq_time']%>;
      $(function() {
      	var o = $("#sidebar");
      	var offset = o.offset();
      	var topPadding = 10;
      	$(window).scroll(function() {
      		if($(window).scrollTop() > offset.top) {
      			o.stop().animate({
              marginTop: $(window).scrollTop() - offset.top + topPadding
      			},"fast");
      		} else {
      			o.stop().animate({
      				marginTop: 0
      			});
      		};
      	});
      });
      function timer() {
        window.setTimeout(function(){
          timeleft = timeleft - 1;
          $("#sidebar").text("残り時間：" + timeleft + "分");
          if(timeleft > 0) {
            timer();
          } else {
            alert("試験時間終了です");
            $("#exam_form").submit();
          }
        },1000 * 60);
      }
    </script>
  </head>
  <body onload="timer();">
    <div id="sidebar" style="font-size: 150%; padding: 10px; float: right; background: lightyellow; border: 1px solid red;">
    残り時間：<%=@conf['enq_time']%>分
    </div>
    <center><h1><%=@conf['enq_title']%>問題</h1></center>
    <hr>
    <h3>下記の設問に答えなさい。</h3>
    <hr>
    <%@table.rodeo.html_form(@table.rodeo.appname,{"id"=>"exam_form"}) {%>
      <%@table.rodeo.html_hidden("action","exam_done")%>
      <%@table.rodeo.html_hidden("uid",@table.rodeo.user_record['アカウント'].id.value)%>
      <%exam = @table.rodeo.table_new(@table.name+"問題")%>
      <%exam.fetch(0,exam.count(),nil,%Q("roid")) {|roid|%>
        <div style="border: solid 1px gray; padding: 10px; margin-top: 10px; margin-bottom: 10px;">
          [<%=exam['設問ID'].value%>]&nbsp;
          <%exam['設問内容'].value.each_line {|line|%>
            <%=line%><br>
          <%}%>
          <%if(exam.fields['設問画像'] != nil and exam['設問画像'].value)%>
            <br>
            <%exam['設問画像'].display()%>
            <br>
          <%end%>
          <%if(exam.fields['設問動画'] != nil and exam['設問動画'].value)%>
            <br>
            <object width="300" height="200">
              <param name="movie" value="<%=exam['設問動画'].value%>"></param>
              <embed src="<%=exam['設問動画'].value%>" width="320" height="180" />
              <noembed>プラグインが必要です。</noembed>
            </object>
            <!-- object data="<%=exam['設問動画'].value%>" type="<%=exam['設問動画'].conf['description']%>" width="320" height="180">
              <param name="movie" value="<%=exam['設問動画'].value%>"></param>
            </object -->
            <br>
          <%end%>
          <br>
          <%fld = @table[exam['設問ID'].value]%>
          <%master = @table.rodeo.table_new(fld.conf['master_table'])%>
          <%@table.rodeo.html_check(fld.cgi_name(),fld.value) {%>
            <%index = "a"%>
            <%order = %Q("#{master[fld.conf['master_sort']].order()}" asc)%>
            <%master.fetch(0,master.count(fld.conf['where']),fld.conf['where'],order) {|roid2|%>
              <%if(exam["選択肢#{index}"].value != nil and exam["選択肢#{index}"].value != "")%>
                <%label = " #{master[fld.conf['master_field']].value}. #{exam["選択肢#{index}"].value}"%>
                <%@table.rodeo.html_item(roid2,label)%><br>
              <%end%>
              <%index.succ!%>
            <%}%>
          <%}%>
        </div>
      <%}%>
      <hr>
      <span style="color: red;">
      問題はこれで終了です。<br>
      解答内容を確認して、良ければ下のボタンを押してください。<br>
      </span>
      <hr>
      <center><%@table.rodeo.html_submit("answer","解答する",{"style"=>"font-size: 400%; width: 90%; height: 1.5em;"})%></center>
    <%}%>
    <hr>
  </body>
</html>

#### exam_done

<html>
  <head>
    <title><%=@conf['enq_title']%></title>
    <script>
			window.location.hash="no-back";
			window.location.hash="no-back-button";
			window.onhashchange=function(){
			    window.location.hash="no-back";
			}
    </script>
  </head>
  <body>
    <center><h1><%=@conf['enq_title']%>結果</h1></center>
    <hr>
    <center>
    <h1>試験お疲れ様でした。</h1>
    <h1>あなたの得点は<%=total%>点満点中<span style="color: <%=color%>;"><%=point%>点</span>です。</h1>
    </center>
    <hr>
  </body>
</html>


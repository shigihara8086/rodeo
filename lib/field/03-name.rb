# -*- coding: utf-8 -*-

#
# 氏名型
#

class RFname < RFBase
  attr_accessor :fname
  attr_accessor :lname
  def self.name(); "氏名" end
  def self.type(); [RFstring.type(),RFstring.type()] end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
    @conf['fname'] = {} unless(@conf['fname'])
    @conf['lname'] = {} unless(@conf['lname'])
    @fname = RFstring.new(name+"_0",table,@conf['fname'])
    @lname = RFstring.new(name+"_1",table,@conf['lname'])
  end
  #
  def value()
    "#{@fname.value} #{@lname.value}"
  end
  def to_s()
    "#{@fname.value} #{@lname.value}"
  end
  #
  def clear()
    @fname.clear()
    @lname.clear()
  end
  #
  def order()
    @fname.db_name()
  end
  # 設定
  def config_entry(su=true)
    super(true) {
      Rodeo.print %Q(<th class="field_conf" colspan="2">姓の設定</th>); @fname.config_entry(false)
      Rodeo.print %Q(<th class="field_conf" colspan="2">名の設定</th>); @lname.config_entry(false)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @fname.eval_config()
    @lname.eval_config()
    @conf['fname']['valid'] = @conf['valid']
    @conf['lname']['valid'] = @conf['valid']
  end
  # 絞りこみ
  def where_entry(su=true)
    @fname.where_entry(su)
  end
  # 絞りこみの評価
  def eval_where()
  	@fname.eval_where()
  end
  # DBフィールド名
  def db_name()
    [@fname.db_name(),@lname.db_name()]
  end
  # データベースからの取り込み
  def from_db(db)
    @fname.from_db(db)
    @lname.from_db(db)
  end
  # データベースへの書き込みデータの作成
  def to_db()
    [@fname.to_db(),@lname.to_db()]
  end
  # CGIからの取り込み
  def from_cgi()
    super() {
      @fname.from_cgi()
      @lname.from_cgi()
      @value = "#{@fname.value} #{@lname.value}"
      @error = @fname.error || @lname.error
    }
  end
  def from_csv(data)
    if(data =~ /^(.*)\s+(.*)$/)
      @fname.value = $1
      @lname.value = $2 #<------------ < GR0010 2008.08.21 Chg tr > $1 → $2
    else
      @fname.value = data
      @lname.value = ""
    end
  end
  def to_csv()
    %Q("#{@fname.value} #{@lname.value}")
  end
  # 入力
  def entry(opt={})
    opt = {} unless(opt)
    opt['accesskey'] = @conf['accesskey'] if(@conf['accesskey'] != "")
    super() {
      Rodeo.print "&nbsp;姓&nbsp;"; @fname.entry(opt)
      Rodeo.print "<br>"
      Rodeo.print "&nbsp;名&nbsp;"; @lname.entry()
    }
  end
  # 入力
  def entry1()
    @fname.entry()
  end
  # 入力
  def entry2()
    @lname.entry()
  end
  # 表示
  def display(list=false)
    super(list) {|list|
      @fname.display(list)
      Rodeo.print " "
      @lname.display(list)
    }
  end
  #
  def hidden()
    @fname.hidden()
    @lname.hidden()
  end
end

__END__



# -*- coding: utf-8 -*-

#
# 郵便番号検索型
#

class RFzipfinder < RFPlugin
  def self.name(); "郵便番号検索" end
  # 設定
  def config_entry(su=true)
    super(su,["RFzipcode"]) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @conf['pref_field']  = @table.rodeo["#{@fid}.pref_field"]
    @conf['address_field']  = @table.rodeo["#{@fid}.address_field"]
  end
  # CGIからの取り込み
  def from_cgi()
  end
  # 入力
  def entry()
    @table.rodeo.eruby(__FILE__,"zipfinder_entry",binding)
  end
  # 表示
  def display(list=false)
    super(list) {|list|
    }
  end
end

class RFzipfinder < RFPlugin
  def action_zipsearch()
    code = @table.rodeo['zipcode'].gsub(/-/,"")
    pref = nil
    city = nil
    area = nil
    @table.rodeo.sql.exec(
      %Q(select "pref","city","area" from "zipcode" where "zipcode" ~ '^#{code}$' order by "zipcode")) {|res|
      if(res.num_tuples > 0)
        pref = res[0]['pref']
        city = res[0]['city']
        area = res[0]['area']
        if(@conf['pref_field'] != nil and @conf['pref_field'] != "")
          # 都道府県選択を変更する場合
          pref_field = @table[@conf['pref_field']]
          address_field = @table[@conf['address_field']]
          Rodeo.puts <<-"EOS"
            var pref = document.getElementById("#{pref_field.cgi_name()}");
            for(var i = 0; i < pref.length; i ++) {
              if(pref.options[i].text == "#{pref}") {
                pref.options[i].selected = true;
              } else {
                pref.options[i].selected = false;
              }
            }
          EOS
          # 住所の設定(市町村＋地域)
          Rodeo.puts <<-"EOS"
            var addr = document.getElementById("#{address_field.cgi_name()}");
            addr.value = "#{city}#{area}";
          EOS
        else
          # 住所の設定(都道府県＋市町村＋地域)
          address_field = @table[@conf['address_field']]
          Rodeo.puts <<-"EOS"
            var addr = document.getElementById("#{address_field.cgi_name()}");
            addr.value = "#{pref}#{city}#{area}";
          EOS
        end
        @table.rodeo.output()
      else
        Rodeo.puts <<-"EOS"
          window.alert("住所が見つかりません");
        EOS
        @table.rodeo.output()
      end
    }
  end
  # アップローダ
  def action_uploader()
    @table.rodeo.eruby(__FILE__,"uploader",binding)
    @table.rodeo.output()
  end
  def action_do_upload()
    file = @table.rodeo[cgi_name("__csv__")]
    name = file.original_filename
    if(name =~ /\\/)
      name = name.split(/\\/)[-1]
    end
    if(name != nil and name != "")
      su = Iconv.new("UTF8","CP932")
      File.open("upload/ken_all.lzh","w") {|fd| fd.write(file.read()) }
      system("lha ew=upload -fq upload/ken_all.lzh")
      @table.rodeo.sql.exec(%Q(delete from "zipcode"))
      CSV.open("upload/ken_all.csv","r") {|row|
        sql = %Q[insert into "zipcode" ("zipcode","pref","city","area") values (]
        row.each_index {|i|
          case i
          when 2 then sql << su.iconv("'#{row[i].data}',")
          when 6 then sql << su.iconv("'#{row[i].data}',")
          when 7 then sql << su.iconv("'#{row[i].data}',")
          when 8
            data = su.iconv(row[i].data)
            if(data =~ /^以下に掲載がない場合$/)
              sql << "NULL)"
            else
              sql << "'#{data}')"
            end
          end
        }
        @table.rodeo.sql.exec(sql)
      }
      File.unlink("upload/ken_all.lzh")
      File.unlink("upload/ken_all.csv")
    end
    @table.rodeo.eruby(__FILE__,"uploader",binding)
    @table.rodeo.output()
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("都道府県フィールド","住所を検索した時に変更する都道府県型フィールドを選択します。")%></th>
  <td class="field_conf">
    <%
      @table.rodeo.html_select("#{@fid}.pref_field",@conf['pref_field']) {
        @table.rodeo.html_item(nil,"[変更しない]")
        @table.farray.each {|h|
          if(h['type'] == "RFselpref")
            @table.rodeo.html_item(h['name'],h['name'])
          end
        }
      }
    %>
  </td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("住所フィールド","住所を検索した時に変更するフィールドを選択します。")%></th>
  <td class="field_conf">
    <%
      @table.rodeo.html_select("#{@fid}.address_field",@conf['address_field']) {
        @table.rodeo.html_item(nil,"[変更しない]")
        @table.farray.each {|h|
          if(h['type'] == "RFstring")
            @table.rodeo.html_item(h['name'],h['name'])
          end
        }
      }
    %>
  </td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("データ取り込み",
    %Q(郵便番号データベースを更新します。日本郵便のページから「全国一括」をダウンロードして、展開したcsvファイルをアップロードしてください。))%></th>
  <td class="field_conf">
    <script type="text/javascript">
      var f = function() {
        var arg = sysargs({
          "action":"uploader",
          "field":"<%=@name%>",
          "fid":"<%=@fid%>"
        });
        frame = document.getElementById("uploader_<%=@fid%>");
        frame.src = rodeo_appname+"?"+arg;
      }
      on_load_fook[on_load_fook.length] = f;
    </script>
    <iframe class="uploader" id="uploader_<%=@fid%>" frameborder="0">
    </iframe>
    <br>
    <a href="http://www.post.japanpost.jp/zipcode/dl/kogaki.html" target="_blank">日本郵便のページ</a>
  </td>
</tr>

#### uploader

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@table.rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_load_func() {
        var frame = parent.document.getElementById("uploader_<%=@table.rodeo['fid']%>");
        var h = document.body.offsetHeight;
        if(h == 0) {
          h = document.body.scrollHeight + 4;
        }
        frame.style.height = h;
      }
      function show_loading() {
        var msg = document.getElementById("message");
        message.innerHTML = '<blink><font color="red">送信中(しばらくお待ちください)...</font></brink>';
        on_load_func();
        return(true);
      }
    </script>
  </head>
  <body class="margin0" onload="on_load_func();">
    <%@table.rodeo.html_form(@table.rodeo.appname,{
        "enctype"=>"multipart/form-data","style"=>"margin:0px;",
        "onsubmit"=>"show_loading();"
      }) {%>
      <%@table.rodeo.html_hidden("action","do_upload")%>
      <%@table.rodeo.html_hidden("fid",@table.rodeo['fid'])%>
      <%@table.rodeo.html_file(cgi_name("__csv__"),"",{"size"=>60})%>
      <%@table.rodeo.html_submit("submit","アップロード")%>
      <div id="message"></div>
    <%}%>
  </body>
</html>

#### zipfinder_entry

<script type="text/javascript"><!--
  function zipfinder_<%=@fid%>() {
    var zipcode = document.getElementById("<%=@main.cgi_name()%>").value.replace("-","");
    document.getElementById("<%=@table[@conf['address_field']].cgi_name()%>").value = "";
    var arg = sysargs({
      "action":"zipsearch",
      "table":"<%=@table.name%>",
      "field":"<%=@name%>",
      "zipcode":zipcode
    });
	  document.body.style.cursor = "wait";
    postFormFunc(rodeo_appname,arg,function(resp) {
	    document.body.style.cursor = "default";
      var text = resp.responseText;
      eval(text);
    });
  }
--></script>
<%@table.rodeo.html_button(cgi_name(""),"住所検索",{"onclick"=>"zipfinder_#{@fid}()"})%>


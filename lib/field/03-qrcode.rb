# -*- coding: utf-8 -*-

#
# QRcode
#

class RFqrcode < RFBase
  def self.name(); "QRコード生成" end
  def self.type(); nil end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
  end
  # 設定
  def config_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @conf['url'] = @table.rodeo["#{@fid}.url"]
  end
  # 表示
  def display(list)
    super() {
      line = @conf['url']
      if(line and line != "")
        line.gsub!(/(%[^%]*%)/) {
          key = $1[1..-2]
          if(@table.fields[key])
            text = CGI::escape(@table[key].value.to_s)
          else
            text = "%"
          end
          text
        }
        if(list)
          Rodeo.puts CGI::escapeHTML(line)
        else
          @table.rodeo.eruby(__FILE__,"display_qrcode",binding)
        end
      end
    }
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("URLテンプレート","QRコード生成用のテンプレートを設定します。")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.url",@conf['url'],{"style"=>"width:100%"})%></td>
</tr>

#### display_qrcode

<img src="lib/qrcode/qrimg.cgi?d=<%=CGI::escape(line)%>"><br>
<a href="<%=line%>"><%=line%></a>


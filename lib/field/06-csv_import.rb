# -*- coding: utf-8 -*-

#
# csv取り込み型
#

require 'csv'

class Iconv
	def initialize(incode,outcode)
  end
  def iconv(data)
  	data
  end
end

class RFcsv_import < RFPlugin
  def self.name(); "CSVインポート" end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
  end
  # 実行用アイコン
  def icon()
    "applications-system.png"
  end
  # 設定
  def config_entry(su=true)
    super(su,[]) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # アプリケーション画面の表示
  def action_app_exec()
    super() {
      @table.rodeo.eruby(__FILE__,"app_exec",binding)
      @table.rodeo.output()
    }
  end
  # テーブルの更新
  def csv_update(rec)
    if(rec['roid'])
      @table.select(rec['roid'])
      rec.each {|k,v|
        if(k != "roid")
          v = nil if(v == "")
          @table[k].from_csv(v)
        end
      }
      @table.insert_mode = false
      @table.update(rec['roid'])
      @message << "roid:#{rec['roid']}を更新しました。<br>\n"
    else
      @table.clear()
      rec.each {|k,v|
        if(k != "roid")
          v = nil if(v == "")
          @table[k].from_csv(v)
        end
      }
      @table.insert_mode = true
      roid = @table.insert()
      @message << "roid:#{roid}を追加しました。<br>\n"
    end
  end
  # CSV取り込み
  def action_csv_upload()
    @error = ""
    @message = ""
    file = @table.rodeo[cgi_name("__image__").force_encoding("ASCII-8BIT")]
    name = file.original_filename
    if(name =~ /\\/)
      name = name.split(/\\/)[-1]
    end
    if(name != nil and name != "")
      File.open("upload/#{name.untaint}","w") {|fd|
        file.each_line {|line|
	        fd.write(line)
        }
      }
      item = []
      data = {}
      first = true
      # CSVファイルのオープン
      CSV.open("upload/#{name.untaint}","r:cp932") {|row|
        row.each {|col|
          if(first)
          	col.each {|c|
	            c.encode!(__ENCODING__)
	            item << c
	            if(@table.fields[c] == nil)
	              @error << "項目「#{c}」が不正です。<br>\n"
	            end
	          }
            first = false
	        else
	          data = {}
	          i = 0
          	col.each {|c|
		          c.encode!(__ENCODING__) if(c)
	            data[item[i]] = c if(item[i])
	            i += 1
            }
	          # テーブルのアップデート
	          if(@error == "")
	            csv_update(data)
	          end
	        end
	      }
      }
    end
    if(@error != "")
      @table.rodeo.eruby(__FILE__,"upload_error",binding)
      @table.rodeo.output()
    else
      @table.rodeo.eruby(__FILE__,"upload_done",binding)
      @table.rodeo.output()
    end
  end
end

__END__

#### config_entry

<!-- config -->

#### app_exec

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%@table.rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_close() {
        parent.list_shrink(null);
        parent.document.getElementById("rodeo_work").src = "blank.html";
      }
    </script>
  </head>
  <body>
    <table class="record_spec" width="100%"><tr>
      <th class="record_spec" width="100%"></th>
      <th class="record_spec" nowrap><%@table.rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
      <th class="record_spec"><%@table.rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
      <th class="record_spec"><%@table.rodeo.html_submit("exec","実行(R)",{"accesskey"=>"R"})%></th>
    </tr></table>
    CSVファイルを取り込みます。<br>
    CSVファイルを選択してください。<br>
    <%@table.rodeo.html_form(@table.rodeo.appname,{"enctype"=>"multipart/form-data","style"=>"margin:0px;"}) {%>
      <%@table.rodeo.html_hidden("action","csv_upload")%>
      <%@table.rodeo.html_file(cgi_name("__image__"),"",{"size"=>60})%>
      <table class="record_spec" width="100%"><tr>
        <th class="record_spec" width="100%"></th>
        <th class="record_spec" nowrap><%@table.rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
        <th class="record_spec"><%@table.rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
        <th class="record_spec"><%@table.rodeo.html_submit("exec","実行(R)",{"accesskey"=>"R"})%></th>
      </tr></table>
    <%}%>
  </body>
</html>

#### upload_done

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%@table.rodeo.html_rodeo_javascript()%>
  </head>
  <body onload="parent.record_list_update(true,true,true,true,null);">
    CSVファイルの取り込みを完了しました。<br>
    [[[ 更新結果 ]]]<br>
    <%=@message%>
  </body>
</html>

#### upload_error

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%@table.rodeo.html_rodeo_javascript()%>
  </head>
  <body>
    CSVファイルの取り込みに失敗しました。<br>
    [[[ エラー内容 ]]]<br>
    <font color="red"><%=@error%></font><br>
  </body>
</html>


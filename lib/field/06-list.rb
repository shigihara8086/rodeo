# -*- coding: utf-8 -*-

#
# 一覧表示型
#

class RFlist < RFPlugin
  def self.name(); "一覧表示" end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
    @conf['page_lines'] = 60 unless(@conf['page_lines'])
  end
  # 実行用アイコン
  def icon()
    "applications-system.png"
  end
  # 設定
  def config_entry(su=true)
    super(su,nil) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @conf['page_lines'] = @table.rodeo["#{@fid}.page_lines"].to_i
  end
  # 表示
  def display(list)
  end
  # アプリケーション画面の表示
  def action_app_exec()
    super() {
      if(@table.rodeo['flags'] != "")
        # メニューから
        @table.rodeo.eruby(__FILE__,"where_config",binding)
      else
        # テーブル一覧から
        @java_prefix = "parent.parent."
        @where_string = @table.rodeo.where
        if(@table.count(@where_string) <= 2000)
          @table.rodeo.eruby(__FILE__,"list",binding)
        else
          @table.rodeo.eruby(__FILE__,"warn_over",binding)
        end
      end
      @table.rodeo.output()
    }
  end
  # リスト表示
  def action_do_list()
    @java_prefix = "parent."
    where = []
    @table.farray.each {|f|
      if(f['name'] != "roid" and f['fobj'].class.superclass != RFPlugin)
        if(@table.rodeo["#{f['name']}"] != "")
          w = f['fobj'].eval_where()
          where << w if(w)
        end
      end
    }
    @where_string = where.join(" and ")
    @where_string = nil if(@where_string == "")
    if(@table.count(@where_string) <= 2000)
      @table.rodeo.eruby(__FILE__,"list",binding)
    else
      @table.rodeo.eruby(__FILE__,"warn_over",binding)
    end
    @table.rodeo.output()
  end
  # リストの強制表示
  def action_force_list()
    @where_string = @table.rodeo['where']
    @where_string = nil if(@where_string == "")
    @table.rodeo.eruby(__FILE__,"list",binding)
    @table.rodeo.output()
  end
  
end

__END__

#### config_entry

<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("行数","１ページに表示する行数を設定します。")%></th>
  <td class="field_conf"><%@table.rodeo.html_number("#{@fid}.page_lines",@conf['page_lines'],:size=>10)%>行</td>
</tr>

#### where_config

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%@table.rodeo.html_rodeo_javascript()%>
    <title>Rodeo:<%=@table.rodeo.db%>/<%=@table.name%>/<%=name%></title>
  </head>
  <body class="margin0">
    <%@table.rodeo.html_form(@table.rodeo.appname) {%>
      <%@table.rodeo.html_hidden("action","do_list")%>
      <table class="record_spec" width="100%" style="margin-bottom:1px">
        <th class="record_spec" width="100%">一覧表示の条件を設定してください。</th>
        <th class="record_spec"><%@table.rodeo.html_submit("exec","一覧表示")%></th>
      </table>
      <table class="record_spec" width="100%" style="margin-bottom:1px">
        <%@table.farray.each {|f|%>
          <%if(f['name'] != "roid" and f['fobj'].conf['where_enable'] == true and f['fobj'].class.superclass != RFPlugin)%>
            <tr>
              <th class="record_spec" width="0%" nowrap>
                <%@table.rodeo.html_checkbox("#{f['name']}","",false)%>
                <%=f['name']%>
              </th>
              <%f['fobj'].where_entry(false)%>
            </tr>
          <%end%>
        <%}%>
      </table>
      <table class="record_spec" width="100%" style="margin-bottom:1px">
        <th class="record_spec" width="100%"></th>
        <th class="record_spec"><%@table.rodeo.html_submit("exec","一覧表示")%></th>
      </table>
    <%}%>
  </body>
</html>

#### list_header

<%if(line_count != 0 and (line_count % @conf['page_lines']) == 0)%>
<!--
  <tr style="page-break-before: always;">
    <td style="border-width:0px;" colspan="#{@flds}" nowrap><font size="+1">≪<%=@table.name%>≫</font></td>
  </tr>
  <tr><td style="border-width:0px;" colspan="#{@flds}" nowrap></td></tr>
-->
<%else%>
  <%@flds = 0%>
  <%@table.farray.each {|f|%>
    <%#if((f['fobj'].class.superclass != RFPlugin and f['delf'] == true) or f['name'] == "roid")%>
      <%@flds += 1%>
    <%#end%>
  <%}%>
<!--
  <tr><td style="border-width:0px;" colspan="#{@flds}" nowrap><font size="+1">≪<%=@table.name%>≫</font></td></tr>
  <tr><td style="border-width:0px;" colspan="#{@flds}" nowrap></td></tr>
-->
<%end%>
<tr>
  <%@table.farray.each {|f|%>
    <%#if((f['fobj'].class.superclass != RFPlugin and f['delf'] == true) or f['name'] == "roid")%>
      <th class="record_spec"><%=f['name']%></th>
    <%#end%>
  <%}%>
</tr>

#### list

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%@table.rodeo.html_rodeo_javascript()%>
    <title>Rodeo:<%=@table.rodeo.db%>/<%=@table.name%>/<%=name%></title>
  </head>
  <body class="margin0">
    <table class="record_spec" style="border-width:0px;">
      <%line_count = 0%>
      <%@table.rodeo.eruby(__FILE__,"list_header",binding)%>
      <%if(@where_string)%>
        <script type="text/javascript"><%=@java_prefix%>rodeo_where = "<%=@where_string.gsub(/\"/,%Q(\\\"))%>";</script>
      <%end%>
      <%sort = nil%>
      <%sort = @table.rodeo['sort'] if(@table.rodeo['sort'])%>
      <%sort << " "+@table.rodeo['sort_dir'] if(@table.rodeo['sort_dir'])%>
      <%@table.fetch(0,@table.count(@where_string),@where_string,sort) {|id|%>
        <tr>
          <%@table.farray.each {|f|%>
            <%#if(f['fobj'].class.superclass != RFPlugin and f['delf'] == true)%>
              <td class="record_spec" style="<%=f['fobj'].style%>" nowrap><%f['fobj'].display(true)%></td>
            <%#end%>
            <%#if(f['name'] == "roid")%>
              <!--
              <td class="record_spec" style="<%=f['fobj'].style%>" nowrap>
                <a href="javascript:<%=@java_prefix%>make_iframe('<%=CGI::escape(@table.name)%>',<%=f['fobj'].value%>);">
                <%f['fobj'].display(true)%></a>
              </td>
              -->
            <%#end%>
          <%}%>
        </tr>
        <%line_count += 1%>
        <%if((line_count % @conf['page_lines']) == 0)%>
          <%@table.rodeo.eruby(__FILE__,"list_header",binding)%>
        <%end%>
      <%}%>
    </table>
  </body>
</html>

#### warn_over

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%@table.rodeo.html_rodeo_javascript()%>
    <title>Rodeo:<%=@table.rodeo.db%>/<%=@table.name%>/<%=name%></title>
  </head>
  <body>
    [[ 警告 ]]<br>
    <font color="red">一覧の件数が2000件を越えています。一覧の表示に時間がかかる場合があります。<br>
    一覧を表示しますか？</font><br>
    <%@table.rodeo.html_form(@table.rodeo.appname) {%>
      <%@table.rodeo.html_hidden("action","force_list")%>
      <%@table.rodeo.html_hidden("where",@where_string)%>
      <hr>
      <%@table.rodeo.html_submit("exec","表示する")%>
    <%}%>
  </body>
</html>


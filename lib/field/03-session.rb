# -*- coding: utf-8 -*-

#
# セッション型
#

class RFsession < RFString
  def self.name(); "セッション" end
  # 入力
  def entry()
    super() {
      @table.rodeo.html_hidden(cgi_name(),@value)
    }
  end
  # CGIデータの評価
  def from_cgi()
  end
end

__END__



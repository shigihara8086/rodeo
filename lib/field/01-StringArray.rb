# -*- coding: utf-8 -*-

#
# 文字列配列型
#

class RFStringArray < RFBase
  def self.name(); "文字列配列" end
  def self.type(); "text[]" end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
  end
  # データのクリア
  def clear()
    @value = []
  end
  # ソート用フィールド名を返す
  def order()
    db_name()
  end
  # データベースからの取り込み
  def parse_array(str)
    ary = str.sub(/^\{/,"").sub(/\}$/,"").split(//)
    res = []
    e = ""
    mode = "p"
    ary.each {|c|
      case mode
      when "p"
        case c
        when ","
          res << e
          e = ""
        when "\""
          mode = "q"
        else
          e << c
        end
      when "q"
        case c
        when "\\"
          mode = "e"
        when "\""
          mode = "p"
        else
          e << c
        end
      when "e"
        e << c
        mode = "q"
      end
    }
    res << e if(e != "")
    res
  end
  # データベースからの取り込み
  def from_db(db)
    if(db[db_name()])
      @value = parse_array(db[db_name()])
    else
      @value = []
    end
  end
  # データベースへの書き込みデータの作成
  def to_db()
    if(@value.size > 0)
      "ARRAY["+@value.collect{|s| "'"+s.gsub(/\'/,"''").gsub(/\\/,"\\\\\\\\")+"'"}.join(",")+"]"
    else
      nil
    end
  end
  # CGIからの取り込み
  def from_cgi()
    super() {
      if(@table.rodeo[cgi_name("hidden")] != "")
        @value = parse_array(@table.rodeo[cgi_name("hidden")])
      else
        yield() if(block_given?)
      end
		}
  end
  # CSVからの取り込み
  def from_csv(data)
    @value = parse_array(data)
  end
  def to_csv()
    "\"{"+@value.collect{|s| "'"+s.gsub(/\'/,"''").gsub(/\\/,"\\\\\\\\")+"'"}.join(",")+"}\""
  end
  #
  def hidden()
    @table.rodeo.html_hidden(cgi_name("hidden"),to_db())
  end
end

__END__


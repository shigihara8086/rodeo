# -*- coding: utf-8 -*-

#
# 実数型
#

class RFReal < RFBase
  def self.name(); "実数" end
  def self.type(); "double precision" end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
  end
  # ソート用フィールド名を返す
  def order()
    db_name()
  end
  # 設定
  def config_entry(su=true)
    super(su) {
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
  end
  # データベースからの取り込み
  def from_db(db)
    if(db[db_name])
      @value = db[db_name()].to_f
    else
      @value = nil
    end
  end
  # データベースへの書き込みデータの作成
  def to_db()
    if(@value)
      @value.to_s
    else
      nil
    end
  end
  # CGIからの取り込み
  def from_cgi()
    super() {
      if(@table.rodeo[cgi_name("hidden")] != "")
        @value = @table.rodeo[cgi_name("hidden")].to_f
      else
        yield() if(block_given?)
      end
		}
  end
  # データベースからの取り込み
  def from_csv(data)
    @value = data.to_f
  end
  def to_csv()
    %Q("#{@value.to_s}")
  end
  #
  def hidden()
    @table.rodeo.html_hidden(cgi_name("hidden"),@value.to_s)
  end
  # SUM
  def sum()
    @value = nil
    where = ""
    if(@table.where_string()) then where = " where #{@table.where_string()}"; end
    @table.rodeo.sql.exec(%Q(select sum("#{@name}") from "rd_#{@table.name}"#{where})) {|res|
      @value = res[0]['sum'].to_f
    }
    @value
  end
  def max()
    @value = nil
    where = ""
    if(@table.where_string()) then where = " where #{@table.where_string()}"; end
    @table.rodeo.sql.exec(%Q(select max("#{@name}") from "rd_#{@table.name}"#{where})) {|res|
      @value = res[0]['max'].to_f
    }
    @value
  end
  def min()
    @value = nil
    where = ""
    if(@table.where_string()) then where = " where #{@table.where_string()}"; end
    @table.rodeo.sql.exec(%Q(select min("#{@name}") from "rd_#{@table.name}"#{where})) {|res|
      @value = res[0]['min'].to_f
    }
    @value
  end
  def ave()
    @value = nil
    where = ""
    if(@table.where_string()) then where = " where #{@table.where_string()}"; end
    @table.rodeo.sql.exec(%Q(select avg("#{@name}") from "rd_#{@table.name}"#{where})) {|res|
      @value = res[0]['avg'].to_f
    }
    @value
  end
  def dev()
    @value = nil
    where = ""
    if(@table.where_string()) then where = " where #{@table.where_string()}"; end
    @table.rodeo.sql.exec(%Q(select stddev("#{@name}") from "rd_#{@table.name}"#{where})) {|res|
      @value = res[0]['stddev'].to_f
    }
    @value
  end
end

__END__


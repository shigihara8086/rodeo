# -*- coding: utf-8 -*-

#
# レコード参照型
#

class RFrecref < RFIntArray
  def self.name(); "レコード参照" end
  def self.type(); RFIntArray.type() end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
  end
  # 設定
  def config_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
  end
  # 絞りこみ
  def where_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"where_entry",binding)
      yield() if(block_given?)
    }
  end
  # 絞りこみの評価
  def eval_where()
    where = nil
    case @table.rodeo['operator']
    when "="  then where = %Q("#{@name}" =  #{@table.rodeo['data']})
    when "!=" then where = %Q("#{@name}" != #{@table.rodeo['data']})
    when "<"  then where = %Q("#{@name}" <  #{@table.rodeo['data']})
    when "<=" then where = %Q("#{@name}" <= #{@table.rodeo['data']})
    when ">"  then where = %Q("#{@name}" >  #{@table.rodeo['data']})
    when ">=" then where = %Q("#{@name}" >= #{@table.rodeo['data']})
    end
    where
  end
  # CGIからの取り込み
  def from_cgi()
    @conf['elements'].times {|i|
      n = i.to_s
      if(@table.rodeo.html_valid?(cgi_name(n)))
        if(@table.rodeo[cgi_name(n)] != "")
          @value[i] = @table.rodeo[cgi_name(n)].to_i
        end
      end
    }
  end
  # 入力
  def entry(opt={})
    opt = {} unless(opt)
    opt['size'] = @conf['cols']
    opt['accesskey'] = @conf['accesskey'] if(@conf['accesskey'] != "")
    super() {
      @conf['elements'].times {|i|
        @table.rodeo.html_number(cgi_name(i.to_s),@value[i].to_s,opt)
      }
    }
  end
  # 表示
  def display(list=false)
    super(list) {|list|
      if(@value)
        Rodeo.print "#{@value.collect{|n| n.to_s}.join(",")}"
      end
    }
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("要素数","配列の要素数です。")%></th>
  <td class="field_conf"><%@table.rodeo.html_number("#{@fid}.elements",@conf['elements'],{"size"=>10})%></td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("初期値","初期値をカンマ(\",\")区切りで書いてください。初期値が要素数より少ない場合には最後の初期値で初期化します。空欄の場合には初期値は空欄になります。")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.default",@conf['default'],{"size"=>60})%></td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("入力桁数")%></th>
  <td class="field_conf"><%@table.rodeo.html_number("#{@fid}.cols",@conf['cols'],{"size"=>10})%></td>
</tr>

#### where_entry

<tr>
  <td class="field_conf">
    <%=@name%>が
    <%@table.rodeo.html_number("data","",{"size"=>@conf['cols']})%>
    <br>
    <%
      @table.rodeo.html_radio("operator","=") {
        @table.rodeo.html_item("=" ,"等しい")
        @table.rodeo.html_item("!=","等しくない")
        @table.rodeo.html_item("<" ,"小さい")
        @table.rodeo.html_item("<=","以下")
        @table.rodeo.html_item(">" ,"大きい")
        @table.rodeo.html_item(">=","以上")
      }
    %>
  </td>
</tr>


# -*- coding: utf-8 -*-

#
# メール送信型
#

require "#{File.dirname(__FILE__)}/../email.rb"

class RFpasswdmailsend < RFPlugin
  def self.name(); "パスワード変更通知" end
  # 実行用アイコン
  def icon()
    nil
  end
  # 設定
  def config_entry(su=true)
    super(su,[]) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @conf['mailto_field'] = @table.rodeo["#{@fid}.mailto_field"]
    @conf['mail_title'] = @table.rodeo["#{@fid}.mail_title"]
    @conf['mail_from'] = @table.rodeo["#{@fid}.mail_from"]
    @conf['info_text'] = @table.rodeo["#{@fid}.info_text"]
  end
  #
  def display(list)
    Rodeo.print %Q(<a href="javascript:app_exec('#{@name}',#{@table.roid});">再発行</a>)
  end
  # アプリケーション画面の表示
  def action_app_exec()
    super()
    if(@table[@conf['mailto_field']].value and @table[@conf['mailto_field']].value.strip != "")
      if(@table['仮パスワード'].value != nil and @table['仮パスワード'].value.strip != "")
        @table.rodeo.eruby(__FILE__,"pwchg_start",binding)
      else
        @table.rodeo.eruby(__FILE__,"pwchg_nopasswd",binding)
      end
    else
      @table.rodeo.eruby(__FILE__,"pwchg_nomailaddr",binding)
    end
    @table.rodeo.output()
  end
  # 仮パスワード再発行
  def action_pwchg_exec()
    mail = Email.new()
    mail.smtpServer = "smtp.sgmail.jp"
    mail['To'] = (@table[@conf['mailto_field']].value).untaint
#    mail['To'] = "a-shigi@sd21.co.jp"
    mail['From'] = @conf['mail_from'].untaint
    mail['Subject'] = @conf['mail_title'].mime_encode
    mail['Content-Type'] = "text/plain; charset=ISO-2022-JP"
    @conf['info_text'].each_line {|line|
      line.chomp!
      line.gsub!(/\r/,"")
      line.gsub!(/(%[^%]*%)/) {
        key = $1[1..-2]
        if(@table[key])
          text = @table[key].value.to_s
        else
          text = "?"
        end
        text
      }
      line.gsub(/㈱/,"(株)").gsub(/㈲/,"(有)")
      mail << line.tojis
    }
    mail.send()
    @table.rodeo.eruby(__FILE__,"pwchg_done",binding)
    @table.rodeo.output()
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("メール送信先フィールド","パスワード再設定通知を送信するメールアドレスのフィールドを選択します。")%></th>
  <td class="field_conf">
    <%
      @table.rodeo.html_select("#{@fid}.mailto_field",@conf['mailto_field']) {
        @table.farray.each {|h|
          if(h['type'] == "RFstring")
            @table.rodeo.html_item(h['name'],h['name'])
          end
        }
      }
    %>
  </td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("送信タイトル","メールのタイトルを設定します。")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.mail_title",@conf['mail_title'],:size=>60)%></td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("送信者","メールFromアドレスを設定します。")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.mail_from",@conf['mail_from'],:size=>60)%></td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("案内文","案内文を設定します。")%></th>
  <td class="field_conf"><%@table.rodeo.html_textarea("#{@fid}.info_text",@conf['info_text'],60,10,{"style"=>"width:100%"})%></td>
</tr>

#### pwchg_start

<html>
  <head>
    <%@table.rodeo.html_rodeo_javascript()%>
    <title>パスワード再発行</title>
  </head>
  <body>
    [[ パスワード再発行 ]]
    <hr>
    下記の再発行通知を<%=@table[@conf['mailto_field']].value%>宛に送信します。<br>
    よろしいですか？<br>
    <hr>
    <tt>
    Subject: <%=@conf['mail_title']%><br>
    To: <%=@table[@conf['mailto_field']].value%><br>
    From: <%=@conf['mail_from']%><br>
    <br>
    <%
      @conf['info_text'].each_line {|line|
        line.chomp!
        line.gsub!(/\r/,"")
        line.gsub!(/(%[^%]*%)/) {
          key = $1[1..-2]
          if(@table[key])
            text = @table[key].value.to_s
          else
            text = "?"
          end
          text
        }
        line.gsub(/㈱/,"(株)").gsub(/㈲/,"(有)")
        Rodeo.print line
        Rodeo.puts "<br>"
      }
    %>
    </tt>
    <hr>
    <%@table.rodeo.html_form("#{@appname}") {%>
      <%@table.rodeo.html_hidden("action","pwchg_exec")%>
      <%@table.rodeo.html_submit("submit","再発行")%><br>
    <%}%>
  </body>
</html>

#### pwchg_done

<html>
  <head>
    <%@table.rodeo.html_rodeo_javascript()%>
    <title>パスワード再発行</title>
  </head>
  <body>
    [[ パスワード再発行終了 ]]
    <hr>
    パスワードの再発行を行ないました。<br>
    <hr>
  </body>
</html>

#### pwchg_nomailaddr

<html>
  <head>
    <%@table.rodeo.html_rodeo_javascript()%>
    <title>パスワード再発行</title>
  </head>
  <body>
    [[ メールアドレスエラー ]]
    <hr>
    メールアドレスがありません。<br>
    <hr>
  </body>
</html>

#### pwchg_nopasswd

<html>
  <head>
    <%@table.rodeo.html_rodeo_javascript()%>
    <title>パスワード再発行</title>
  </head>
  <body>
    [[ パスワードエラー ]]
    <hr>
    パスワードがありません。<br>
    <hr>
  </body>
</html>



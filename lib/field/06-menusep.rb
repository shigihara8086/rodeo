# -*- coding: utf-8 -*-

#
# メニューセパレータ型
#

class RFmenusep < RFPlugin
  def self.name(); "メニューセパレータ" end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
    @exec = false
  end
  # 実行用アイコン
  def icon()
    "stock_data-next.png"
  end
  # 設定
  def config_entry(su=true)
    super(su,nil) {
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
  end
end

__END__


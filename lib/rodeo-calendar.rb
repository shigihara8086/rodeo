#!/usr/bin/ruby -Ku

class Rodeo
  # メイン
  def main()
    @user_record = table_new("ユーザ")
    if(@user_record['アカウント'].session_valid?())
      @admin = false
      group = table_new("グループ")
      @user_record['グループ'].value.each {|v|
        if(group.select(v))
          @admin = true if(group['管理者'].value == true)
        end
      }
      run()
      exit()
    else
      eruby(__FILE__,"login",binding)
      output()
      exit()
    end
  end
end

class Rodeo
  class Table
    # デフォルト(テーブル一覧)
    def action_rodeo_browser()
      @rodeo.eruby(__FILE__,"rodeo_calendar",binding)
      @rodeo.output()
    end
    # 表示制限用where句生成
    def where_string()
      where = @rodeo.where
      if(not @rodeo.admin)
        case self['roid'].conf['record_display']
        when "OWNER"
          if(@rodeo.where)
            where = %Q((#{@rodeo.where}) and #{@rodeo.user_record['roid'].value} = ANY("roid_1"))
          else
            where = %Q(#{@rodeo.user_record['roid'].value} = ANY("roid_1"))
          end
        when "GROUP"
          ws = []
          @rodeo.user_record['グループ'].value.each {|g|
            ws << %Q(#{g} = ANY("roid_2"))
          }
          if(@rodeo.where)
            where = %Q((#{@rodeo.where}) and (#{ws.join(" or ")}))
          else
            where = %Q(#{ws.join(" or ")})
          end
        end
      end
      where
    end
    def order_string()
      ord = @fields[@rodeo.sort]['fobj'].order()
      if(ord)
        %Q("#{ord}" #{@rodeo.sort_dir})
      else
        nil
      end
    end
    # フィールド一覧のアップデート
    def action_update_field()
      @rodeo.eruby(__FILE__,"rodeo_field_list",binding)
      @rodeo.output()
    end
    # レコード一覧のアップデート
    def action_update_record()
      @rodeo.eruby(__FILE__,"rodeo_record_table",binding)
      @rodeo.output()
    end
    # 操作フィールドのアップデート
    def action_update_function()
      @rodeo.eruby(__FILE__,"rodeo_record_function",binding)
      @rodeo.output()
    end
    # ステータスのアップデート
    def action_update_status()
      @rodeo.eruby(__FILE__,"rodeo_record_status",binding)
      @rodeo.output()
    end
    # レコード詳細
    def action_record_spec()
      select(@rodeo.roid)
      @rodeo.eruby(__FILE__,"rodeo_record_spec",binding)
      @rodeo.output()
    end
    # レコード追加
    def action_record_new()
      insert_mode = true
      clear()
      @rodeo.eruby(__FILE__,"rodeo_record_new",binding)
      @rodeo.output()
    end
    def action_do_record_new()
      insert_mode = true
      # エラーチェック
      if(eval_cgi() > 0)
        # エラーの時は再入力
        @rodeo.eruby(__FILE__,"rodeo_record_new",binding)
        @rodeo.output()
      else
        roid = insert()
        @rodeo.eruby(__FILE__,"rodeo_record_new_done",binding)
        @rodeo.output()
      end
    end
    # レコード編集
    def action_record_edit()
      insert_mode = false
      select(@rodeo.roid)
      @rodeo.eruby(__FILE__,"rodeo_record_edit",binding)
      @rodeo.output()
    end
    def action_do_record_edit()
      select(@rodeo.roid)
      insert_mode = false
      # エラーチェック
      if(eval_cgi() > 0)
        # エラーの時は再入力
        @rodeo.eruby(__FILE__,"rodeo_record_edit",binding)
        @rodeo.output()
      else
        update(@rodeo.roid)
        @rodeo.eruby(__FILE__,"rodeo_record_edit_done",binding)
        @rodeo.output()
      end
    end
    # レコード削除
    def action_record_delete()
      code = "ERR"
      rc = "不明なエラーです"
      if(@rodeo.html_valid?("roid"))
        delete(@rodeo.roid)
        code = "OK"
        rc = "レコード「#{@rodeo.roid}」を削除しました"
      end
      Rodeo.puts "#{code}:#{rc}"
      @rodeo.output()
    end
    # フィールド追加
    def action_field_add()
      code = "ERR"
      rc = "不明なエラーです"
      if(@rodeo.html_valid?("ftype"))
        if(@rodeo.html_valid?("fname") and @rodeo['fname'].strip != "")
          if(not @fields[@rodeo['fname']])
            add_field(@rodeo['fname'],@rodeo['ftype'],true)
            code = "OK"
            rc = "フィールド「#{@rodeo['fname']}」を追加しました"
          else
            rc = "フィールド「#{@rodeo['fname']}」は既に存在します"
          end
        else
          rc = "フィールド名を指定してください"
        end
      end
      Rodeo.puts "#{code}:#{rc}"
      @rodeo.output()
    end
    # フィールド削除
    def action_field_delete()
      code = "ERR"
      rc = "不明なエラーです"
      if(@rodeo.html_valid?("fname"))
        if(@fields[@rodeo['fname']])
          delete_field(@rodeo['fname'])
          code = "OK"
          rc = "フィールド「#{@rodeo['fname']}」を削除しました"
        else
          code = "ERR"
          rc = "フィールド「#{@rodeo['fname']}」がありません"
        end
      end
      Rodeo.puts "#{code}:#{rc}"
      @rodeo.output()
    end
    # フィールド順の変更
    def action_field_reorder()
      code = "OK"
      rc = "フィールド「#{@rodeo.field}」を#{@rodeo['to']}番目に移動しました"
      reorder_field(@rodeo.field,@rodeo['to'].to_i)
      Rodeo.puts "#{code}:#{rc}"
      @rodeo.output()
    end
    # フィールド設定
    def action_field_config()
      @rodeo.eruby(__FILE__,"rodeo_field_config",binding)
      @rodeo.output()
    end
    def action_do_field_config()
      self[@rodeo.field].eval_config()
      config_field(@rodeo.field)
      @rodeo.eruby(__FILE__,"rodeo_field_config_done",binding)
      @rodeo.output()
    end
    # オンデマンド追加
    def action_ondemand_new()
      insert_mode = true
      @rodeo.eruby(__FILE__,"rodeo_ondemand_new",binding)
      @rodeo.output()
    end
    def action_do_ondemand_new()
      insert_mode = true
      if(eval_cgi() > 0)
        @rodeo.eruby(__FILE__,"rodeo_ondemand_new",binding)
        @rodeo.output()
      else
        roid = insert()
        @rodeo.eruby(__FILE__,"rodeo_ondemand_new_done",binding)
        @rodeo.output()
      end
    end
    # オンデマンド詳細表示
    def action_ondemand_spec()
      if(@rodeo['oids'])
        roids = @rodeo['oids'].split(/,/)
        roids.each {|roid|
          if(select(roid))
            @rodeo.eruby(__FILE__,"view_body",binding)
          end
        }
      end
      @rodeo.output()
    end
    def action_detail_new()
      insert_mode = true
      sw = false
      @rodeo.eruby(__FILE__,"rodeo_detail_new",binding)
      @rodeo.output()
    end
    def action_do_detail_new()
      insert_mode = true
      if(eval_cgi() > 0)
        @rodeo.eruby(__FILE__,"rodeo_detail_new",binding)
        @rodeo.output()
      else
        roid = insert()
        sw = true
        @rodeo.eruby(__FILE__,"rodeo_detail_new",binding)
        @rodeo.output()
      end
    end
    # 絞りこみ設定
    def action_where_config()
      @rodeo.eruby(__FILE__,"rodeo_where_config",binding)
      @rodeo.output()
    end
    def action_do_where_config()
      where = self[@rodeo.field].eval_where()
      @rodeo.eruby(__FILE__,"rodeo_where_config_done",binding)
      @rodeo.output()
    end
  end
end

__END__

#### login

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_load_func() { document.location = "admin.rbx"; }
    </script>
  </head>
  <body onload="on_load_func()">
  </body>
</html>

#### rodeo_calendar

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%@rodeo.html_rodeo_javascript()%>
    <title>Rodeo:<%=name%></title>
  </head>
<!--  <body class="margin0"
   onload="table_adjust()"
   onresize="height_adjusted = false;height_adjust()"> -->
  <body class="margin0">
    <table id="rodeo_browser" class="record_frame" style="border-width:1px;border-style:solid" width="100%">
      <tr>
        <td class="record_frame">
          <div id="rodeo_record_list" class="calendar" style="width:100%">
            <%@rodeo.eruby(__FILE__,"rodeo_calendar_body",binding)%>
          </div>
        </td>
      </tr>
      <tr>
        <td class="record_frame" colspan="2">
          <div id="rodeo_record_status" class="status" style="width:100%">
            <%@rodeo.eruby(__FILE__,"rodeo_record_status",binding)%>
          </div>
        </td>
      </tr>
      <tr>
        <td class="record_frame" colspan="2">
          <div id="rodeo_record_tools" class="tools" style="width:100%">
            <%@rodeo.eruby(__FILE__,"rodeo_record_tools",binding)%>
          </div>
        </td>
      </tr>
    </table>
    <%@rodeo.eruby(__FILE__,"rodeo_browse_work",binding)%>
  </body>
</html>

#### rodeo_browse_work

<iframe id="rodeo_work" class="work" src="" width="1" height="1" frameborder="0" style="overflow-x:auto; overflow-y:auto">
</iframe>

#### rodeo_calendar_body

<table id="if_body" class="record_list">
  <%
    n = 0
    order = order_string()
    where = where_string()
  %>
  <%
    now = Time.now()
    y = now.year
    m = now.month
    ny = y
    nm = m + 1
    if(nm > 12)
      ny += 1
      nm = 1
    end
    days = Time.local(ny,nm,1).yday - Time.local(y,m,1).yday
    days += 365 if(days < 0)
    week = ["日","月","火","水","木","金","土"]
    wcol = ["red","black","black","black","black","black","blue"]
    s = -Time.local(y,m,1).wday()
  %>
  <tr>
    <%(0..6).each {|i|%>
      <th class="record_list" width="100"><font color="<%=wcol[i]%>"><%=week[i]%></font></th>
    <%}%>
  </tr>
  <%j = 0%>
  <%(s .. days-1).each {|i|%>
    <%if((j % 7) == 0)%>
      <tr>
    <%end%>
    <%w = i % 7%>
    <%if(i < 0)%>
      <td height="30" class="record_list"><br></td>
    <%else%>
      <td height="30" class="record_list" style="text-align:right;"><%=i+1%></td>
    <%end%>
    <%if((j % 7) == 7)%>
      </tr>
    <%end%>
    <%j += 1%>
  <%}%>
</table>

#### rodeo_record_tools

<%disable = ["RFroid","RFid","RFsession","RFpasswd"]%>
<%insert_acct = (@rodeo.admin or (self['roid'].conf['record_insert'] & @rodeo.user_record['グループ'].value != []))%>
<%field_insert = (@rodeo.admin or (self['roid'].conf['field_insert'] & @rodeo.user_record['グループ'].value != []))%>
<table class="record_tools">
  <tr>
    <td class="record_status" nowrap>
        [レコード：<%=@rodeo.offset + 1%>-<%=@rodeo.offset + @rodeo.record_list_limit%>/<%=reccount%>件]
    </td>
    <%if(@rodeo.sort)%>
      <%if(@rodeo.sort_dir == "asc")%>
        <td class="record_status" nowrap>[並べ替え：<%=@rodeo.sort%>/正順]</td>
      <%else%>
        <td class="record_status" nowrap>[並べ替え：<%=@rodeo.sort%>/逆順]</td>
      <%end%>
    <%else%>
      <td class="record_status" nowrap>[並べ替え：roid/正順]</td>
    <%end%>
    <%if(count() == 0)%>
      <%if(realcount() == 0)%>
        <script type="text/javascript">
          parent.message_show("レコードがありません");
        </script>
      <%else%>
        <script type="text/javascript">
          parent.message_show("条件に一致するレコードがありません");
        </script>
      <%end%>
    <%end%>
    </span></font></td>
    <td width="100%" style="padding:0px"></td>
    <%@rodeo.html_form("javascript:field_add('field_data');void(0)",:id=>"field_data") {%>
      <td>
        <%opt = {}%>
        <%opt['disabled'] = "true" unless(field_insert)%>
        <%@rodeo.html_select("ftype","",opt) {%>
          <%i = 0%>
          <%rodeo.flist.each {|hash|%>
            <%rodeo.html_optgroup("#{@rodeo.flabel[i]}",{"class"=>"flist#{i}"}) {%>
              <%hash.sort{|a,b|a[1]<=>b[1]}.each {|k,v|%>
                <%if(not disable.include?(k))%>
                  <%@rodeo.html_item("#{k}","#{v}")%>
                <%end%>
              <%}%>
            <%}%>
            <%i += 1%>
          <%}%>
        <%}%>
      </td>
      <td>型</td>
      <%opt = {'size'=>20}%>
      <%opt['disabled'] = "true" unless(field_insert)%>
      <td><%@rodeo.html_text("fname","",opt)%></td>
      <%opt = {}%>
      <%opt['disabled'] = "true" unless(field_insert)%>
      <td><%@rodeo.html_submit("submit","フィールド追加",opt)%></td>
    <%}%>
  </tr>
</table>

#### rodeo_record_status

<table class="record_status">
  <%
    where = where_string()
    reccount = count(where)
    pages = reccount / @rodeo.record_list_limit
    pages += 1 if((reccount % @rodeo.record_list_limit) != 0)
    now_page = @rodeo.offset / @rodeo.record_list_limit
  %>
  <tr>
<!--
    <td class="record_status" nowrap>
        [レコード：<%=@rodeo.offset + 1%>-<%=@rodeo.offset + @rodeo.record_list_limit%>/<%=reccount%>件]
    </td>
-->
<!--
    <%if(@rodeo.sort)%>
      <%if(@rodeo.sort_dir == "asc")%>
        <td class="record_status" nowrap>[並べ替え：<%=@rodeo.sort%>/正順]</td>
      <%else%>
        <td class="record_status" nowrap>[並べ替え：<%=@rodeo.sort%>/逆順]</td>
      <%end%>
    <%else%>
      <td class="record_status" nowrap>[並べ替え：roid/正順]</td>
    <%end%>
-->
<!--
    <%if(@rodeo.where)%>
      <td class="record_status" nowrap>[絞りこみ：<%=@rodeo.where%>]</td>
    <%else%>
      <td class="record_status" nowrap>
        <%if   (@rodeo.admin != true and self['roid'].conf['record_display'] == "OWNER")%>
          [絞りこみ：オーナのみ表示]
        <%elsif(@rodeo.admin != true and self['roid'].conf['record_display'] == "GROUP")%>
          [絞りこみ：グループのみ表示]
        <%else%>
          [絞りこみ：全て表示]
        <%end%>
      </td>
    <%end%>
-->
    <td class="record_status" width="100%" nowrap></td>
    <!-- 先頭 -->
    <%sw = ((now_page == 0) ? false : true)%>
    <td class="record_status">
      <%@rodeo.html_imglink("javascript:page_jump(0)","stock_data-first.png","",nil,sw)%>
    </td>
    <!-- 前へ -->
    <%sw = ((now_page == 0) ? false : true)%>
    <td class="record_status">
      <%@rodeo.html_imglink("javascript:page_jump(#{@rodeo.offset - @rodeo.record_list_limit})","stock_data-previous.png","",nil,sw)%>
    </td>
    <!-- 各ページ -->
    <%
      delta = 5
      if(pages > delta * 2)
        lo = now_page - delta
        hi = now_page + delta
        if(lo < 0)
          hi -= lo
          lo = 0
        end
        if(hi >= pages)
          hi = pages - 1
          lo = hi - delta * 2
        end
      else
        lo = 0
        hi = pages - 1
        if(hi < 0)
          hi = 0
        end
      end
    %>
    <%(lo..hi).each {|i|%>
      <%sw = ((now_page == i) ? false : true)%>
      <td class="record_status">
        <%@rodeo.html_imglink("javascript:page_jump(#{i * @rodeo.record_list_limit})",nil,"#{i+1}",nil,sw)%>
      </td>
    <%}%>
    <!-- 次へ -->
    <%sw = ((now_page >= pages - 1) ? false : true)%>
    <td class="record_status">
      <%@rodeo.html_imglink("javascript:page_jump(#{@rodeo.offset + @rodeo.record_list_limit})","stock_data-next.png","",nil,sw)%>
    </td>
    <!-- 末尾 -->
    <%sw = ((now_page >= pages - 1) ? false : true)%>
    <td class="record_status">
      <%@rodeo.html_imglink("javascript:page_jump(#{@rodeo.record_list_limit * (pages - 1)})","stock_data-last.png","",nil,sw)%>
    </td>
    <!-- 畳む/展開 -->
    <td class="record_status" nowrap>
      <%@rodeo.html_imglink("javascript:list_shrink()","object-flip-vertical.png","",nil,true)%>
    </td>
  </tr>
</table>

#### view_body

<!-- table class="record_spec" width="100%" style="margin-bottom:1px" -->
  <%self.farray.each {|fld|%>
    <%if(fld['fobj'].plugin != true)%>
      <tr>
        <th class="record_spec" nowrap>
          <%@rodeo.html_tooltip(fld['name'],fld['fobj'].conf['description'])%>
        </th>
        <td class="record_spec" colspan="2" width="100%">
          <%fld['fobj'].display()%>
        </td>
      </tr>
    <%end%>
  <%}%>
<!-- /table -->

#### edit_body

<!-- table class="record_spec" width="100%" style="margin-bottom:1px" -->
  <%self.farray.each {|fld|%>
    <%if(fld['fobj'].plugin != true)%>
      <tr>
        <th class="record_spec" nowrap>
          <%if(fld['fobj'].conf['valid'])%>
            <font color="red">※</font>
          <%end%>
          <%@rodeo.html_tooltip(fld['name'],fld['fobj'].conf['description'])%>
        </th>
        <td class="record_edit" colspan="2" width="100%">
          <%fld['fobj'].table.insert_mode = insert_mode%>
          <%fld['fobj'].entry()%>
        </td>
      </tr>
    <%end%>
  <%}%>
<!-- /table -->

#### rodeo_record_spec

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@rodeo.html_rodeo_javascript()%>
    <style type="text/css" media="screen"><!--
      .noscreen {
        visibility:hidden
      }
    --></style>
    <style type="text/css" media="print"><!--
      .noprint {
        visibility:hidden
      }
    --></style>
    <script type="text/javascript"><!--
      function on_load_func() {
        for(i=0; i < on_load_fook.length; i ++) {
          on_load_fook[i]();
        }
      }
    --></script>
  </head>
  <body class="margin0" style="padding:1px" onload="on_load_func();">
    <table class="record_spec" width="100%" style="margin-bottom:1px">
      <tr>
        <th class="record_spec" nowrap>テーブル</th>
        <th class="record_spec" width="100%" nowrap><%=@name%></th>
        <th class="record_spec" nowrap><input type="button" onclick="window.print()" value="印刷"></th>
      </tr>
      <%@rodeo.eruby(__FILE__,"view_body",binding)%>
      <tr>
        <th class="record_spec" colspan="2" width="100%"></th>
        <th class="record_spec"><input type="button" onclick="window.print()" value="印刷"></th>
      </tr>
    </table>
  </body>
</html>

#### rodeo_record_new

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@rodeo.html_rodeo_javascript()%>
    <script type="text/javascript"><!--
      function on_load_func() {
        for(i=0; i < on_load_fook.length; i ++) {
          on_load_fook[i]();
        }
      }
    --></script>
  </head>
  <body class="margin0" style="padding:1px" onload="on_load_func();">
    <table class="record_spec" width="100%" style="margin-bottom:1px">
      <%@rodeo.html_form(@rodeo.appname) {%>
        <%@rodeo.html_hidden("action","do_record_new")%>
        <tr>
          <th class="record_spec">レコード追加</th>
          <th class="record_spec" width="100%">
            <%self.farray.each {|fld|%>
              <%if(fld['fobj'].error)%>
                <font color="red"><%=fld['fobj'].error%></font>
                <%break%>
              <%end%>
            <%}%>
          </th>
          <th class="record_spec" nowrap><%@rodeo.html_submit("submit","追加")%></th>
        </tr>
        <%@rodeo.eruby(__FILE__,"edit_body",binding)%>
        <tr>
          <th class="record_spec" colspan="2" width="100%"><font color="red">※</font>の項目は必須項目です</th>
          <th class="record_spec"><%@rodeo.html_submit("submit","追加")%></th>
        </tr>
      <%}%>
    </table>
  </body>
</html>

#### rodeo_record_new_done

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@rodeo.html_rodeo_javascript()%>
  </head>
  <%
    reccount = count()
    pages = reccount / @rodeo.record_list_limit
    pages += 1 if((reccount % @rodeo.record_list_limit) != 0)
    now_page = @rodeo.offset / @rodeo.record_list_limit
    last_page = @rodeo.record_list_limit * (pages - 1)
  %>
  <body onload="parent.record_new_done(<%=roid%>,<%=last_page%>)"></body>
</html>

#### rodeo_record_edit

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@rodeo.html_rodeo_javascript()%>
    <script type="text/javascript"><!--
      function on_load_func() {
        for(i=0; i < on_load_fook.length; i ++) {
          on_load_fook[i]();
        }
      }
    --></script>
  </head>
  <body class="margin0" style="padding:1px" onload="on_load_func();">
    <table class="record_spec" width="100%" style="margin-bottom:1px">
      <%@rodeo.html_form(@rodeo.appname) {%>
        <%@rodeo.html_hidden("action","do_record_edit")%>
        <tr>
          <th class="record_spec">レコード編集</th>
          <th class="record_spec" width="100%">
            <%self.farray.each {|fld|%>
              <%if(fld['fobj'].error)%>
                <font color="red"><%=fld['fobj'].error%></font>
                <%break%>
              <%end%>
            <%}%>
          </th>
          <th class="record_spec" nowrap><%@rodeo.html_submit("submit","更新")%></th>
        </tr>
        <%@rodeo.eruby(__FILE__,"edit_body",binding)%>
        <tr>
          <th class="record_spec" colspan="2" width="100%"><font color="red">※</font>の項目は必須項目です</th>
          <th class="record_spec"><%@rodeo.html_submit("submit","更新")%></th>
        </tr>
      <%}%>
    </table>
  </body>
</html>

#### rodeo_record_edit_done

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@rodeo.html_rodeo_javascript()%>
  </head>
  <body
  onload="parent.record_list_update(false,true,true,false,null); parent.parent.message_show('レコード「<%=@rodeo.roid%>」を更新しました')">
  </body>
</html>

#### rodeo_field_config

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@rodeo.html_rodeo_javascript()%>
  </head>
  <body class="margin0" style="padding:1px">
    <table class="record_spec" width="100%" style="margin-bottom:1px">
      <%@rodeo.html_form(@rodeo.appname) {%>
        <%@rodeo.html_hidden("action","do_field_config")%>
        <tr>
          <th class="record_spec" width="100%" nowrap>
            [<%=@rodeo['field']%>/<%=instance_eval("#{@fields[@rodeo['field']]['type']}.name()".untaint)%>型]の設定
          </th>
          <th class="record_spec"><%@rodeo.html_submit("exec","設定")%></th>
        </tr>
      </table>
      <table class="record_spec" width="100%" style="margin-bottom:1px">
        <%@fields[@rodeo['field']]['fobj'].config_entry()%>
      </table>
      <table class="record_spec" width="100%">
        <tr>
          <th class="record_spec" width="100%"></th>
          <th class="record_spec"><%@rodeo.html_submit("exec","設定")%></th>
        </tr>
      <%}%>
    </table>
  </body>
</html>

#### rodeo_field_config_done

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@rodeo.html_rodeo_javascript()%>
  </head>
  <body
  onload="parent.record_list_update(false,true,true,false,null); parent.parent.message_show('フィールド「<%=@rodeo.field%>」を設定しました');">
  </body>
</html>

#### rodeo_ondemand_new

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@rodeo.html_rodeo_javascript()%>
    <script type="text/javascript"><!--
      function on_load_func() {
        frame = parent.document.getElementById("master_area_<%=@rodeo['fid']%>");
        h = document.body.offsetHeight;
        if(h == 0) {
          h = document.body.scrollHeight + 4;
        }
        frame.style.height = h;
        for(i=0; i < on_load_fook.length; i ++) {
          on_load_fook[i]();
        }
      }
    --></script>
  </head>
  <body class="margin0" style="padding:0px" onload="on_load_func();">
    <table class="record_spec" width="100%" style="margin-bottom:1px">
      <%@rodeo.html_form(@rodeo.appname) {%>
        <%@rodeo.html_hidden("action","do_ondemand_new")%>
        <%@rodeo.html_hidden("fid",@rodeo['fid'])%>
        <%@rodeo.html_hidden("callback",@rodeo['callback'])%>
        <tr>
          <th class="record_spec" width="100%">レコード追加</th>
          <th class="record_spec" nowrap><%@rodeo.html_submit("submit","追加")%></th>
        </tr>
      </table>
      <%@rodeo.eruby(__FILE__,"edit_body",binding)%>
      <table class="record_spec" width="100%">
        <tr>
          <th class="record_spec" width="100%">
            <%@farray.each {|fld|%>
              <%if(fld['fobj'].error)%>
                <font color="red"><%=fld['fobj'].error%></font>
                <%break%>
              <%end%>
            <%}%>
          </th>
          <th class="record_spec"><%@rodeo.html_submit("submit","追加")%></th>
        </tr>
      <%}%>
    </table>
  </body>
</html>

#### rodeo_ondemand_new_done

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@rodeo.html_rodeo_javascript()%>
    <script type="text/javascript"><!--
      function on_load_func() {
        parent.<%=@rodeo['callback']%>();
      }
    --></script>
  </head>
  <body onload="on_load_func();">
  </body>
</html>

#### rodeo_detail_new

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@rodeo.html_rodeo_javascript()%>
    <script type="text/javascript"><!--
      function on_load_func() {
        frame = parent.document.getElementById("master_area_<%=@rodeo['fid']%>");
        h = document.body.offsetHeight;
        if(h == 0) {
          h = document.body.scrollHeight + 4;
        }
        frame.style.height = h;
        for(i=0; i < on_load_fook.length; i ++) {
          on_load_fook[i]();
        }
      }
      function on_load_func2(sw) {
        if(sw) {
          parent.<%=@rodeo['callback']%>(<%=roid%>);
        }
      }
    --></script>
  </head>
  <body class="margin0" style="padding:0px" onload="on_load_func();on_load_func2(<%=sw%>);">
    <table class="record_spec" width="100%" style="margin-bottom:1px">
      <%@rodeo.html_form(@rodeo.appname) {%>
        <%@rodeo.html_hidden("action","do_detail_new")%>
        <%@rodeo.html_hidden("fid",@rodeo['fid'])%>
        <%@rodeo.html_hidden("callback",@rodeo['callback'])%>
        <tr>
          <th class="record_spec" width="100%">レコード追加</th>
          <th class="record_spec" nowrap><%@rodeo.html_submit("submit","追加")%></th>
        </tr>
      </table>
      <%@rodeo.eruby(__FILE__,"edit_body",binding)%>
      <table class="record_spec" width="100%">
        <tr>
          <th class="record_spec" width="100%">
            <%@farray.each {|fld|%>
              <%if(fld['fobj'].error)%>
                <font color="red"><%=fld['fobj'].error%></font>
                <%break%>
              <%end%>
            <%}%>
          </th>
          <th class="record_spec"><%@rodeo.html_submit("submit","追加")%></th>
        </tr>
      <%}%>
    </table>
  </body>
</html>

#### rodeo_where_config

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@rodeo.html_rodeo_javascript()%>
  </head>
  <body class="margin0" style="padding:1px">
    <table class="record_spec" width="100%" style="margin-bottom:1px">
      <%@rodeo.html_form(@rodeo.appname) {%>
        <%@rodeo.html_hidden("action","do_where_config")%>
        <tr>
          <th class="record_spec" width="100%" nowrap>
            [<%=@rodeo['field']%>/<%=instance_eval("#{@fields[@rodeo['field']]['type']}.name()".untaint)%>型]絞りこみ条件の設定
          </th>
          <th class="record_spec"><%@rodeo.html_submit("exec","設定")%></th>
        </tr>
      </table>
      <table class="record_spec" width="100%" style="margin-bottom:1px">
        <%@fields[@rodeo['field']]['fobj'].where_entry()%>
      </table>
      <table class="record_spec" width="100%">
        <tr>
          <th class="record_spec" width="100%"></th>
          <th class="record_spec"><%@rodeo.html_submit("exec","設定")%></th>
        </tr>
      <%}%>
    </table>
  </body>
</html>

#### rodeo_where_config_done

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@rodeo.html_rodeo_javascript()%>
    <%if(where)%>
      <script type="text/javascript"><!--
        if(parent.rodeo_where == "") {
          parent.rodeo_where = '<%=where.gsub(/\'/,"\\\\'")%>';
        } else {
          parent.rodeo_where = parent.rodeo_where.concat(' and ');
          parent.rodeo_where = parent.rodeo_where.concat('<%=where.gsub(/\'/,"\\\\'")%>');
        }
        parent.record_list_update(false,true,true,true,null);
      --></script>
    <%end%>
  </head>
  <%if(where)%>
    <body onload="parent.parent.message_show('絞りこみ条件を追加しました');">
  <%else%>
    <body onload="parent.parent.message_show('絞りこみ条件が不正です');">
  <%end%>
  </body>
</html>


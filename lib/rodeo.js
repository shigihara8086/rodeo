//
// Rodeo Ajaxサポート
//

// フォームデータの取得
function getFormData(form) {
  var array = new Array();
  for(var i = 0; i < form.elements.length; i++) {
    array.push(encodeURIComponent(form.elements[i].name)+'='+encodeURIComponent(form.elements[i].value));
  }
  return array.join("&");
}

// 
function postCGI(cgi,args,func) {
  /* XMLHttpRequestオブジェクト作成 */
  if(binfo.ie) {
    var xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    if(!xmlhttp) {
      var xmlhttp = new XMLHttpRequest();
      if(!xmlhttp) {
        window.alert("XMLHttpRequest非対応のブラウザです。");
        return true;
      }
    }
  } else {
    var xmlhttp = new XMLHttpRequest();
    if(!xmlhttp) {
      window.alert("XMLHttpRequest非対応のブラウザです。");
      return true;
    }
  }
  /* レスポンスデータ処理方法の設定 */
  xmlhttp.onreadystatechange = function() {
    if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
      //レスポンスデータを表示します。
      if(func) { func(xmlhttp.responseText); }
    }
  }
  /* HTTPリクエスト実行 */
  xmlhttp.open("POST",cgi,true);
  xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  xmlhttp.send(args);
  return false;
}

// 
function postCommand(cgi,args,func) {
  /* XMLHttpRequestオブジェクト作成 */
  if(binfo.ie) {
    var xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    if(!xmlhttp) {
      var xmlhttp = new XMLHttpRequest();
      if(!xmlhttp) {
        window.alert("XMLHttpRequest非対応のブラウザです。");
        return true;
      }
    }
  } else {
    var xmlhttp = new XMLHttpRequest();
    if(!xmlhttp) {
      window.alert("XMLHttpRequest非対応のブラウザです。");
      return true;
    }
  }
  /* レスポンスデータ処理方法の設定 */
  xmlhttp.onreadystatechange = function() {
    if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
      //レスポンスデータを表示します。
      resp = xmlhttp.responseText.split(":",2);
      if(func) { func(resp[0],resp[1]); }
    }
  }
  /* HTTPリクエスト実行 */
  xmlhttp.open("POST",cgi,true);
  xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  xmlhttp.send(args);
  return false;
}

//
function postForm(cgi,args,id1,func) {
  /* XMLHttpRequestオブジェクト作成 */
  if(binfo.ie) {
    var xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    if(!xmlhttp) {
      var xmlhttp = new XMLHttpRequest();
      if(!xmlhttp) {
        window.alert("XMLHttpRequest非対応のブラウザです。");
        return true;
      }
    }
  } else {
    var xmlhttp = new XMLHttpRequest();
    if(!xmlhttp) {
      window.alert("XMLHttpRequest非対応のブラウザです。");
      return true;
    }
  }
  /* レスポンスデータ処理方法の設定 */
  xmlhttp.onreadystatechange = function() {
    if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
      //レスポンスデータを表示します。
      if(id1) {
        document.getElementById(id1).innerHTML = xmlhttp.responseText;
        if(func) { func(); }
      }
    }
  }
  /* HTTPリクエスト実行 */
  xmlhttp.open("POST",cgi,true);
  xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  xmlhttp.send(args);
  return false;
}

//
function postFormFunc(cgi,args,func) {
  /* XMLHttpRequestオブジェクト作成 */
  if(binfo.ie) {
    var xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    if(!xmlhttp) {
      var xmlhttp = new XMLHttpRequest();
      if(!xmlhttp) {
        window.alert("XMLHttpRequest非対応のブラウザです。");
        return true;
      }
    }
  } else {
    var xmlhttp = new XMLHttpRequest();
    if(!xmlhttp) {
      window.alert("XMLHttpRequest非対応のブラウザです。");
      return true;
    }
  }
  /* レスポンスデータ処理方法の設定 */
  xmlhttp.onreadystatechange = function() {
    if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
      if(func) { func(xmlhttp); }
    }
  }
  /* HTTPリクエスト実行 */
  xmlhttp.open("POST",cgi,true);
  xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  xmlhttp.send(args);
  return false;
}

// システム引数の作成
function sysargs(hash) {
  if(!("appname"           in hash)) { hash['appname']           = rodeo_appname;           }
  if(!("user"              in hash)) { hash['user']              = rodeo_user;              }
  if(!("table"             in hash)) { hash['table']             = rodeo_table;             }
  if(!("session"           in hash)) { hash['session']           = rodeo_session;           }
  if(!("where"             in hash)) { hash['where']             = rodeo_where;             }
  if(!("sort"              in hash)) { hash['sort']              = rodeo_sort;              }
  if(!("sort_dir"          in hash)) { hash['sort_dir']          = rodeo_sort_dir;          }
  if(!("offset"            in hash)) { hash['offset']            = rodeo_offset;            }
  if(!("roid"              in hash)) { hash['roid']              = rodeo_roid;              }
  if(!("field"             in hash)) { hash['field']             = rodeo_field;             }
  if(!("elems"             in hash)) { hash['elems']             = rodeo_elems;             }
  if(!("fakeuser"          in hash)) { hash['fakeuser']          = rodeo_fakeuser;          }
  var arg = new Array();
  for(key in hash) {
    arg.push(encodeURIComponent(key)+'='+encodeURIComponent(hash[key]));
  }
  return arg.join("&");
}

/**
 テーブルブラウザ
**/

// ドキュメントの表示
function rodeo_help() {
  iframe_stack[iframe_stack.length] = window.open("doc/index.html","Rodeo:help");
}

// タブ切り替え用iframeの作成
//
active_iframe = null;
tab_font_size = "85%";
//
function make_iframe(name,roid) {
  if(roid) {
    arg = sysargs({
      "action":"record_edit",
      "table":name,
      "roid":roid
    });
    iframe_stack[iframe_stack.length] = window.open(rodeo_appname+"?"+arg,name);
  } else {
    arg = sysargs({
      "action":"rodeo_browser",
      "table":name,
      "where":""
    });
//    iframe_stack[iframe_stack.length] = window.open(rodeo_appname+"?"+arg,name);
    window.open(rodeo_appname+"?"+arg,name);
  }
}

// アプリケーション用iframeの作成
function exec_iframe(table,name,opt) {
  arg = sysargs({
    "action":"app_exec",
    "table":table,
    "field":name,
    "where":"",
    "flags":"direct"
  });
//  iframe_stack[iframe_stack.length] = window.open(rodeo_appname+"?"+arg,name+":"+opt);
  iframe_stack[iframe_stack.length] = window.open(rodeo_appname+"?"+arg,opt);
}

// タブの削除
function delete_iframe(name) {
  var array = new Array();
  for(var i = 0; i < iframe_stack.length; i ++) {
    if(iframe_stack[i] != name) {
      array[array.length] = iframe_stack[i];
    }
  }
  iframe_stack = array;
}

// レイアウトの調整
function adjust_layout() {
  var body = document.body;
  var tools = document.getElementById("rodeo_tools");
  var tabs = document.getElementById("rodeo_tabs");
  var work = document.getElementById("rodeo_work");
  if(binfo.ie) {
    tabs.style.width = tools.offsetWidth - 0;
    work.style.width = tools.offsetWidth - 0;
    work.style.height = body.clientHeight - tools.offsetHeight - tabs.offsetHeight - 6;
    window.setTimeout(function() {
      var frame = document.getElementById("iframe_work_frame");
      var w = work.clientWidth;
      var h = work.clientHeight;
      frame.style.width = w;
      frame.style.height = h;
      table_frame_width = w;
      table_frame_height = h;
    },1);
  } else {
    if(binfo.firefox) {
      tabs.style.width = tools.offsetWidth - 2;
      work.style.width = tools.offsetWidth - 2;
      work.style.height = body.clientHeight - tools.offsetHeight - tabs.offsetHeight - 6;
      window.setTimeout(function() {
        var frame = document.getElementById("iframe_work_frame");
        var w = work.clientWidth;
        var h = work.clientHeight;
        frame.style.width = w;
        frame.style.height = h;
        table_frame_width = w;
        table_frame_height = h;
      },1);
    } else {
      tabs.style.width = tools.offsetWidth - 2;
      work.style.width = tools.offsetWidth - 2;
      work.style.height = body.clientHeight - tools.offsetHeight - tabs.offsetHeight - 6;
      window.setTimeout(function() {
        var frame = document.getElementById("iframe_work_frame");
        var w = work.clientWidth;
        var h = work.clientHeight;
        frame.style.width = w;
        frame.style.height = h;
        table_frame_width = w;
        table_frame_height = h;
      },1);
    }
  }
}

// メッセージの表示
var message_timer = null;
function message_show(msg) {
  var msgx = document.getElementById("rodeo_message");
  msgx.style.visibility = "visible";
  msgx.innerHTML = msg;
  window.setTimeout(function() {
    if(message_timer) { window.clearTimer(message_timer); }
    message_timer = message_clear();
  },10000);
}

// メッセージの消去
function message_clear() {
  var msgx = document.getElementById("rodeo_message");
  message_timer = null;
  msgx.style.visibility = "hidden";
  msgx.innerHTML = "";
}

// リストの再読み込み
function table_update() {
//  document.body.style.cursor = "wait";
  document.getElementById("iframe_work_frame").src = "?action=table_list";
//  args = "action=update";
//  postForm(rodeo_appname,args,"rodeo_tables",function() {
//    document.body.style.cursor = "default";
//  });
}

// テーブル作成
function table_create(form) {
  document.body.style.cursor = "wait";
  args = getFormData(document.getElementById(form))+"&action=create";
  postCommand(rodeo_appname,args,function(code,msg) {
    document.body.style.cursor = "default";
    message_show(msg);
//    alert(msg);
    if(code == "OK") {
      var arg = sysargs({"action":"table_list"});
      postForm(rodeo_appname+"?",arg,"table_browser_list",null);
      // 作成したテーブルを開く
      make_iframe(document.getElementById("name").value);
    }
  });
}

// テーブル削除
function table_delete(name) {
  if(window.confirm("テーブル「"+name+"」を削除します。\nよろしいですか？")) {
    document.body.style.cursor = "wait";
    args = "action=delete&name="+encodeURIComponent(name);
    postCommand(rodeo_appname,args,function(code,msg) {
      document.body.style.cursor = "default";
      message_show(msg);
      if(code == "OK") {
        var arg = sysargs({"action":"table_list"});
        postForm(rodeo_appname+"?",arg,"table_browser_list",null);
        // 削除したタブを消す
        delete_iframe(name);
      }
    });
  }
}

// パスワード変更
function user_password() {
  var arg = sysargs({"action":"user_password"});
  window.open(rodeo_appname+"?"+arg);
}
function user_password_done(msg) {
  window.close();
}

// ログアウト
function user_logout() {
  for(var i = 0; i < iframe_stack.length; i ++) {
    try {
      iframe_stack[i].close();
    } catch(e) {
//      alert("既に閉じられています");
    }
  }
  var arg = sysargs({"action":"logout"});
  document.location = rodeo_appname+"?"+arg;
}

/**
 レコードブラウザ
**/

// マウスホイール
wheel_timer = null;
function wheel_handler(delta) {
  if(delta < 0) {
    if(rodeo_records > rodeo_offset + rodeo_elems) {
      rodeo_offset = rodeo_offset + rodeo_elems;
      if(wheel_timer) { window.clearTimeout(wheel_timer); }
      document.body.style.cursor = "wait";
      wheel_timer = window.setTimeout(function() {
        page_jump(rodeo_offset);
      },300);
    }
  } else {
    if(rodeo_offset >= rodeo_elems) {
      rodeo_offset = rodeo_offset - rodeo_elems;
      if(wheel_timer) { window.clearTimeout(wheel_timer); }
      document.body.style.cursor = "wait";
      wheel_timer = window.setTimeout(function() {
        page_jump(rodeo_offset);
      },300);
    }
  }
}
function wheel_event(event) {
  var delta = 0;
  if(!event) event = window.event;  /* IE */
  if(event.wheelDelta) {            /* IE or Opera */
    delta = event.wheelDelta / 120;
    if(window.opera) delta = -delta;
  } else if(event.detail) {         /* Mozilla */
    delta = -event.detail / 3;
  }
  if(delta) wheel_handler(delta);
  if(event.preventDefault) {
    event.preventDefault();
  }
  event.returnValue = false;
}
function set_wheel_handler() {
  var obj = document.getElementById("rodeo_record_list");
  if(obj.addEventListener) obj.addEventListener("DOMMouseScroll",wheel_event,false);
  obj.onmousewheel = wheel_event;
}

// DIVの高さ調整
height_adjusted = false;
function height_adjust() {
  var logo = document.getElementById("rodeo_logo");
  var br = document.getElementById("rodeo_browser");
  var wk = document.getElementById("rodeo_work");
  // 大きさの調整
  if(binfo.ie) {
//    wk.style.height = document.body.offsetHeight - br.offsetHeight - logo.offsetHeight - 34;
    wk.style.height = 0;
    wk.style.width = logo.offsetWidth;
  } else {
    if(binfo.firefox) {
//      wk.style.height = document.body.clientHeight - br.offsetHeight - logo.offsetHeight - 10;
      wk.style.height = 0;
      wk.style.width = logo.clientWidth;
    } else {
//      wk.style.height = document.body.offsetHeight - br.offsetHeight - logo.offsetHeight - 8;
      wk.style.height = 0;
      wk.style.width = logo.clientWidth;
    }
  }
  height_adjusted = true;
}

// テーブル要素の大きさを合わせる
table_adjusted = false;
old_scroll_posx = null;
old_scroll_posy = null;
function table_adjust() {
  var logo = document.getElementById("rodeo_logo");
  // 機能部分の幅を設定
  var ctool = document.getElementById("if_func");
  var dtool = document.getElementById("rodeo_record_function");
  var cal = document.getElementById("rodeo_calendar")
  if(cal) {
    var w = ctool.offsetWidth + cal.offsetWidth + 1;
  } else {
    var w = ctool.offsetWidth;
  }
  // IEの場合
  if(binfo.ie) {
    var body_width = document.body.clientWidth;
    document.getElementById("rodeo_field_list").style.width = body_width - w - 7;
    document.getElementById("rodeo_record_list").style.width = body_width - w - 7;
  } else {
    if(binfo.firefox) {
      var body_width = document.body.clientWidth;
      document.getElementById("rodeo_field_list").style.width = body_width - w - 7;
      document.getElementById("rodeo_record_list").style.width = body_width - w - 7;
    } else {
      var body_width = document.body.clientWidth;
      document.getElementById("rodeo_field_list").style.width = body_width - w - 7;
      document.getElementById("rodeo_record_list").style.width = body_width - w - 7;
    }
  }
  // セル幅を合わせる
  var chead = document.getElementById("if_head");
  var cbody = document.getElementById("if_body");
  var cfunc = document.getElementById("if_func");
  // 高さを合わせる
  for(var i = 0;i < cbody.rows.length;i ++) {
    var src = cbody.rows.item(i);
    var dst = cfunc.rows.item(i);
    var src_td = src.cells.item(0);
    var dst_td = dst.cells.item(0);
    if(!src) { break; }
    if(!dst) { break; }
    var sh = src_td.offsetHeight;
    var dh = dst_td.offsetHeight;
    if(sh >= dh) {
      dst_td.style.height = sh;
      src_td.style.height = sh;
    } else {
      src_td.style.height = dh;
      dst_td.style.height = dh;
    }
    // 行の背景色を設定
    if((i % 2) == 0) {
      src.style.backgroundColor = "ghostwhite";
      dst.style.backgroundColor = "ghostwhite";
    } else {
      src.style.backgroundColor = "white";
      dst.style.backgroundColor = "white";
    }
  }
  // 幅を合わせる
  var ffv = binfo.firefoxMVersion;
  var srow = cbody.rows.item(0);
  var drow = chead.rows.item(0);
  for(var i = 0;i < srow.cells.length;i ++) {
    var src = srow.cells.item(i);
    var dst = drow.cells.item(i);
    if(!src) { break; }
    if(!dst) { break; }
    var sw = src.offsetWidth;
    var dw = dst.offsetWidth;
    if(sw > dw) {
      if(ffv == 3) {
        dst.style.width = sw - 1;
        src.style.width = sw - 1;
      } else {
        dst.style.width = sw;
        src.style.width = sw;
      }
    } else {
      if(ffv == 3) {
        src.style.width = dw - 5;
        dst.style.width = dw - 5;
      } else {
        src.style.width = dw;
        dst.style.width = dw;
      }
    }
  }
  // スクロール位置が保存されていたら戻す
  if(old_scroll_posx) {
    var chead = document.getElementById("rodeo_field_list");
    var cbody = document.getElementById("rodeo_record_list");
    cbody.scrollLeft = old_scroll_posx;
    chead.scrollLeft = old_scroll_posx;
  }
  // 表示オフセットを先頭にする
  var cbody = document.getElementById("rodeo_record_list");
  var ctool = document.getElementById("rodeo_record_function");
  ctool.scrollTop = 0;
  cbody.scrollTop = 0;
  table_adjusted = true;
//  set_wheel_handler();
}

// スクロールの同期
scroll_sync_enable = true;
function scroll_sync() {
  if(scroll_sync_enable) {
    var cbody = document.getElementById("rodeo_record_list");
    var chead = document.getElementById("rodeo_field_list");
    var hx = chead.scrollLeft;
    old_scroll_posx = cbody.scrollLeft;
    if(old_scroll_posx != hx) {
      chead.scrollLeft = old_scroll_posx;
    }
    var ctool = document.getElementById("rodeo_record_function");
    var hy = ctool.scrollTop;
    old_scroll_posy = cbody.scrollTop;
    if(old_scroll_posy != hy) {
      ctool.scrollTop = old_scroll_posy;
    }
  }
}

// IE用再表示パッチ
function ie_patch() {
  var wf = document.getElementById("rodeo_work");
  var wh = wf.offsetHeight;
  wf.onreadystatechange = function() {
    if(wf.readyState == "complete") {
      wf.style.height = 0;
      window.setTimeout(function() {
        wf.style.height = wh;
      },1);
    }
  }
}

// レコードリストのアップデート
//
function record_list_update_func(cgi,args,id1,cnt,func) {
  /* XMLHttpRequestオブジェクト作成 */
  var xmlhttp = new XMLHttpRequest();
  if(!xmlhttp) {
    window.alert("XMLHttpRequest非対応のブラウザです。");
    return true;
  }
  /* レスポンスデータ処理方法の設定 */
  xmlhttp.onreadystatechange = function() {
    if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
      //レスポンスデータを表示します。
      if(id1) {
        document.getElementById(id1).innerHTML = xmlhttp.responseText;
        if(func) { func(cnt); }
      }
    }
  }
  /* HTTPリクエスト実行 */
  xmlhttp.open("POST",cgi,true);
  xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  xmlhttp.send(args);
  return false;
}

// レコードリストのアップデート
//
function record_list_update_json(cgi,args,func) {
  /* XMLHttpRequestオブジェクト作成 */
  var xmlhttp = new XMLHttpRequest();
  if(!xmlhttp) {
    window.alert("XMLHttpRequest非対応のブラウザです。");
    return true;
  }
  /* レスポンスデータ処理方法の設定 */
  xmlhttp.onreadystatechange = function() {
    if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
      //レスポンスデータを表示します。
      text = xmlhttp.responseText;
      data = eval(text);
      if(data) {
        if(data["field"])    { document.getElementById("rodeo_field_list").innerHTML      = data["field"];    }
        if(data["table"])    { document.getElementById("rodeo_record_list").innerHTML     = data["table"];    }
        if(data["function"]) { document.getElementById("rodeo_record_function").innerHTML = data["function"]; }
        if(data["status"])   { document.getElementById("rodeo_record_status").innerHTML   = data["status"];   }
        if(func) { func(); }
      }
    }
  }
  /* HTTPリクエスト実行 */
  xmlhttp.open("POST",cgi,true);
  xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  xmlhttp.send(args);
  return false;
}

function record_list_update(field,list,func,stat,fook) {
  if(binfo.ie) {
    // IEの場合リロードする
    var arg = sysargs({"action":"rodeo_browser"});
    document.location = rodeo_appname+"?"+arg;
  } else {
    done_count = 0;
    var loaded = function() {
      document.body.style.cursor = "default";
      table_adjusted = false;
      while(table_adjusted == false) { table_adjust(); }
      if(fook) { fook(); }
    }
    var arg = sysargs({
      "update_field" : (field) ? "true" : "false",
      "update_list"  : (list)  ? "true" : "false",
      "update_func"  : (func)  ? "true" : "false",
      "update_stat"  : (stat)  ? "true" : "false"
    });
    document.body.style.cursor = "wait";
    record_list_update_json(rodeo_appname,arg+"&action=update",loaded);
  }
}

// フィールドの更新
function field_update() {
  row_selected = null;
  record_list_update(true,true,true,true,null);
}

// ページ切替
function page_jump(offset) {
  row_selected = null;
  rodeo_offset = offset;
  record_list_update(false,true,true,true,null);
}

// 正順ソートの設定
function sort_asc(name) {
  row_selected = null;
  rodeo_offset = 0;
  rodeo_sort = name;
  rodeo_sort_dir = "asc";
  record_list_update(false,true,true,true,null);
}

// 逆順ソートの設定
function sort_dsc(name) {
  row_selected = null;
  rodeo_offset = 0;
  rodeo_sort = name;
  rodeo_sort_dir = "desc";
  record_list_update(false,true,true,true,null);
}

// チェックボックスのON/OFF
function check_onoff(name) {
  var arg = sysargs({
    "action":"check_onoff",
    "fname":name
  });
  postCommand(rodeo_appname,arg,function(code,msg) {
    record_list_update(true,true,false,false,null);
  });
}

// マークのON/OFF
function mark_onoff(roid) {
  var arg = sysargs({
    "action":"mark_onoff",
    "roid":roid
  });
  postCommand(rodeo_appname,arg,function(code,msg) {
    if(code == "OK") {
      message_show(msg);
    } else {
      message_show(msg);
    }
  });
}

// 全マークのON
function all_mark_on() {
  if(window.confirm("すべてのマークをONにします。\nよろしいですか？")) {
    var arg = sysargs({
      "action":"all_mark_on"
    });
    postCommand(rodeo_appname,arg,function(code,msg) {
      if(code == "OK") {
        record_list_update(false,false,true,false,null);
        message_show(msg);
      } else {
        message_show(msg);
      }
    });
  }
}

// 全マークのOFF
function all_mark_off() {
  if(window.confirm("すべてのマークをOFFにします。\nよろしいですか？")) {
    var arg = sysargs({
      "action":"all_mark_off"
    });
    postCommand(rodeo_appname,arg,function(code,msg) {
      if(code == "OK") {
        record_list_update(false,false,true,false,null);
        message_show(msg);
      } else {
        message_show(msg);
      }
    });
  }
}

// 全マークのREV
function all_mark_rev() {
  if(window.confirm("すべてのマークを反転します。\nよろしいですか？")) {
    var arg = sysargs({
      "action":"all_mark_rev"
    });
    postCommand(rodeo_appname,arg,function(code,msg) {
      if(code == "OK") {
        record_list_update(false,false,true,false,null);
        message_show(msg);
      } else {
        message_show(msg);
      }
    });
  }
}

// フィールド追加
function field_add(form) {
  var args = getFormData(document.getElementById(form));
  postCommand(rodeo_appname,args+"&action=field_add",function(code,msg) {
    if(code == "OK") {
      row_selected = null;
      record_list_update(true,true,true,false,function() { message_show(msg) });
    } else {
      message_show(msg);
    }
  });
}

// フィールド削除
function field_delete(name) {
  if(window.confirm("フィールド「"+name+"」を削除します。\nよろしいですか？")) {
    var arg = sysargs({
      "action":"field_delete",
      "fname":name
    });
    postCommand(rodeo_appname,arg,function(code,msg) {
      if(code == "OK") {
        row_selected = null;
        record_list_update(true,true,true,false,function() { message_show(msg) });
      } else {
        message_show(msg);
      }
    });
  }
}

// 絞りこみの設定
function where_config(name) {
  list_shrink(function() {
    var arg = sysargs({"action":"where_config","field":name});
    document.getElementById("rodeo_work").src = rodeo_appname+"?"+arg;
    if(binfo.ie) { ie_patch(); }
  });
}

// フィールド設定
function field_config(name) {
  list_shrink(function() {
    var arg = sysargs({"action":"field_config","field":name});
    document.getElementById("rodeo_work").src = rodeo_appname+"?"+arg;
    if(binfo.ie) { ie_patch(); }
  });
}

// テーブル設定
function table_config(name) {
  var arg = sysargs({"action":"table_config","table":name});
  document.getElementById("rodeo_work").src = rodeo_appname+"?"+arg;
  if(binfo.ie) { ie_patch(); }
}

// フィールドの並べ替え
function field_reorder(name,to) {
  var arg = sysargs({
    "action":"field_reorder",
    "field":name,
    "to":to.value
  });
  postCommand(rodeo_appname,arg,function(code,msg) {
    message_show(msg);
    if(code == "OK") {
      row_selected = null;
      record_list_update(true,true,false,false,null);
    }
  });
}

// 絞りこみの解除
function where_clear() {
  row_selected = null;
  rodeo_where = "";
  rodeo_offset = rodeo_old_offset;
  rodeo_sort = rodeo_old_sort;
  rodeo_sort_dir = rodeo_old_sort_dir;
  record_list_update(true,true,true,true,null);
  var arg = sysargs({"action":"do_where_clear"});
  document.getElementById("rodeo_work").src = rodeo_appname+"?"+arg;
  if(binfo.ie) { ie_patch(); }
}

// 行選択
row_selected = null;
row_selected_color = null;
function table_click(obj,roid) {
  if(obj != row_selected) {
    if(row_selected != null) {
      row_selected.style.backgroundColor = row_selected_color;
    }
    row_selected_color = obj.style.backgroundColor;
    obj.style.backgroundColor = '#cccccc';
    row_selected = obj;
//    if(roid) record_show(roid);
  }
}

// 行選択(レコード詳細表示)
function table_dblclick(obj,roid,edit) {
  if(row_selected != null) {
    row_selected.style.backgroundColor = row_selected_color;
  }
  row_selected_color = obj.style.backgroundColor;
  obj.style.backgroundColor = '#cccccc';
  row_selected = obj;
  if(roid) {
    list_shrink(function() {
      var work = document.getElementById("rodeo_work");
      if(edit) {
        var arg = sysargs({"action":"record_edit","roid":roid});
      } else {
        var arg = sysargs({"action":"record_spec","roid":roid});
      }
      work.src = rodeo_appname+"?"+arg;
      if(binfo.ie) { ie_patch(); }
    });
  }
}

// レコード削除
function record_delete(roid) {
  if(window.confirm("レコード「"+roid+"」を削除します。\nよろしいですか？")) {
    var arg = sysargs({
      "action":"record_delete",
      "roid":roid
    });
    postCommand(rodeo_appname,arg,function(code,msg) {
      message_show(msg);
      if(code == "OK") {
        row_selected = null;
        record_list_update(true,true,true,false,null);
      }
    });
  }
}

// マークされたレコードの削除
function mark_delete() {
  if(window.confirm("マークされたレコードを削除します。\nよろしいですか？")) {
    var arg = sysargs({
      "action":"mark_delete"
    });
    postCommand(rodeo_appname,arg,function(code,msg) {
      parent.message_show(msg);
      if(code == "OK") {
        row_selected = null;
        record_list_update(true,true,true,false,null);
      }
    });
  }
}

// マークされたレコードの編集
function mark_edit() {
  if(window.confirm("マークされたレコードを一括編集します。\nよろしいですか？")) {
    var work = document.getElementById("rodeo_work");
    var arg = sysargs({"action":"mark_edit"});
    work.src = rodeo_appname+"?"+arg;
    if(binfo.ie) { ie_patch(); }
  }
}

// レコード表示
function record_show(roid) {
  list_shrink(function() {
    var work = document.getElementById("rodeo_work");
    var arg = sysargs({"action":"record_spec","roid":roid});
    work.src = rodeo_appname+"?"+arg;
    if(binfo.ie) { ie_patch(); }
  });
}

// レコード表示(他テーブル)
function record_refer(table,roid) {
  list_shrink(function() {
    var work = document.getElementById("rodeo_work");
    var arg = sysargs({"table":table,"action":"record_spec","roid":roid});
    work.src = rodeo_appname+"?"+arg;
    if(binfo.ie) { ie_patch(); }
  });
}

// レコード編集(他テーブル)
function record_edit2(table,roid) {
  list_shrink(function() {
    var work = document.getElementById("rodeo_work");
    var arg = sysargs({"table":table,"action":"record_edit","roid":roid});
    work.src = rodeo_appname+"?"+arg;
    if(binfo.ie) { ie_patch(); }
  });
}

// レコード追加
function record_new(roid) {
  list_shrink(function() {
    var work = document.getElementById("rodeo_work");
    var arg = sysargs({"action":"record_new"});
    work.src = rodeo_appname+"?"+arg;
    if(binfo.ie) { ie_patch(); }
  });
}

// レコード編集
function record_edit(roid) {
  list_shrink(function() {
    var work = document.getElementById("rodeo_work");
    var arg = sysargs({"action":"record_edit","roid":roid});
    work.src = rodeo_appname+"?"+arg;
    if(binfo.ie) { ie_patch(); }
  });
}

// レコード編集(一括)
function record_edit_window(roid) {
  var arg = sysargs({"action":"record_edit_window","roid":roid});
  var width = parent.window.offsetWidth;
  var height = parent.window.offsetHeight;
  window.open(rodeo_appname+"?"+arg,"_blank");
}

// レコード追加後の再表示
function record_new_done(roid,page) {
  row_selected = null;
  rodeo_offset = page;
  record_list_update(true,true,true,true,function() {
    var arg = sysargs({"action":"record_new"});
    document.getElementById("rodeo_work").src = rodeo_appname+"?"+arg;
    if(binfo.ie) { ie_patch(); }
  });
  message_show("レコード「"+roid+"」を追加しました");
}

// アプリケーション実行
function app_exec(field,roid) {
  list_shrink(function() {
    var arg = sysargs({"action":"app_exec","field":field,"roid":roid});
    document.getElementById("rodeo_work").src = rodeo_appname+"?"+arg;
    if(binfo.ie) { ie_patch(); }
  });
}

// レコードリストの畳み込み
var list_shrinked = false;
var list_height = 0;
var work_height = 0;
var work_width = 0;
var chunk_size = 0;
function list_shrink(func) {
  if(binfo.ie) {
    list_shrink_ie(func);
  } else {
    list_shrink_noneie(func);
  }
}

function list_shrink_noneie(func) {
  if(!list_shrinked) {
    list_height = document.getElementById("rodeo_record_function").offsetHeight;
    if(binfo.ie) {
      work_height = document.getElementById("rodeo_work").offsetHeight;
    } else {
      if(binfo.firefox) {
        work_height = document.getElementById("rodeo_work").offsetHeight - 2;
      } else {
        work_height = document.getElementById("rodeo_work").offsetHeight - 2;
      }
    }
    if(binfo.ie) {
      document.getElementById("rodeo_record_function").style.display = "none";
      document.getElementById("rodeo_record_list").style.display = "none";
    } else {
      document.getElementById("rodeo_record_function").style.height = chunk_size;
      document.getElementById("rodeo_record_list").style.height = chunk_size;
    }
    document.getElementById("rodeo_work").style.height = 1;
    window.setTimeout(function() {
      if(binfo.firefox) {
        var wk = document.getElementById("rodeo_work");
        wk.style.width = document.getElementById("rodeo_browser").offsetWidth - 2;
      }
      document.getElementById("rodeo_work").style.height = work_height + list_height - chunk_size;
      list_shrinked = true;
      if(func) func();
    },1);
  } else {
    if(func) {
      func();
    } else {
      document.getElementById("rodeo_work").contentWindow.document.body.innerHTML = "";
      if(binfo.ie) {
        document.getElementById("rodeo_record_list").style.display = "block";
        document.getElementById("rodeo_record_function").style.display = "block";
      } else {
        document.getElementById("rodeo_record_list").style.height = list_height;
        document.getElementById("rodeo_record_function").style.height = list_height;
      }
      document.getElementById("rodeo_work").style.height = 1;
      window.setTimeout(function() {
        if(binfo.firefox) {
          var wk = document.getElementById("rodeo_work");
          wk.style.width = document.getElementById("rodeo_browser").offsetWidth - 2;
          list_shrinked = false;
        }
        document.getElementById("rodeo_work").style.height = work_height;
        list_shrinked = false;
      },1);
    }
  }
}

function list_shrink_ie(func) {
var list = document.getElementById("rodeo_browser");
var work = document.getElementById("rodeo_work");
  if(!list_shrinked) {
    list_height = list.offsetHeight;
    work_height = work.offsetHeight - 2;
    list.style.display = "none";
    work.style.height = list_height + work_height;
    list_shrinked = true;
    if(func) func();
  } else {
    if(func) {
      func();
    } else {
      list.style.display = "block";
      work.style.height = work_height;
      list_shrinked = false;
    }
  }
}

// レコードリストの展開
function list_expand() {
  if(list_shrinked) list_shrink(null);
}

// レコード参照表示
function record_ref(table,roid,job) {
  var arg = sysargs({
    "action":"record_"+job,
    "table":table,
    "roid":roid
  });
  document.getElementById("rodeo_work").src = rodeo_appname+"?"+arg;
  if(binfo.ie) { ie_patch(); }
}

// 絞り込み条件の表示
function where_popup() {
  return overlib(rodeo_where,CAPTION,'絞り込み条件(クリックで解除します)');
}

// 特定データの複製
function where_fill(name) {
  if(rodeo_where == null || rodeo_where == "") {
    window.alert("レコードの絞り込みを行ってください");
  } else {
    if(window.confirm("マークレコードの値で「"+name+"」を補完します。\nよろしいですか？")) {
      var arg = sysargs({
        "action":"where_fill",
        "field":name
      });
      postCommand(rodeo_appname,arg,function(code,msg) {
        message_show(msg);
        if(code == "OK") {
          row_selected = null;
          record_list_update(true,true,true,false,null);
        }
      });
    }
  }
}

// ユーザ切り替え
function switch_user(roid) {
  arg = sysargs({
    "action":"rodeo_browser",
    "offset":0
  });
  document.body.style.cursor = "wait";
  document.location = rodeo_appname+"?"+arg;
}


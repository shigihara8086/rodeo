#!/usr/bin/ruby1.9
# -*- coding: utf-8 -*-

class Rodeo
  # HTML出力(改行無し)
  def self.print(str)
    if(str)
      str = str.to_s
      str.force_encoding(__ENCODING__) if(RUBY_VERSION >= "1.9.0")
      @@outbuf << "#{str.gsub(/\r/,"")}"
    end
  end
  # HTML出力(改行付き)
  def self.puts(str)
    if(str)
      str = str.to_s
      str.force_encoding(__ENCODING__) if(RUBY_VERSION >= "1.9.0")
      @@outbuf << "#{str.gsub(/\r/,"")}"
      @@outbuf << "\n"
    end
  end
  # HTMLバッファクリア
  def self.clear()
    @@outbuf = ""
  end
  #
  def self.gettext()
    @@outbuf
  end
  #
  def self.settext(text)
    @@outbuf = text
  end
  # 簡易erubyインタプリタ
  def eruby(file,tag,bind)
    rcode = ""
    prog = ""
    lcount = 0
    File.open(file,"r:utf-8") {|fd|
      if(tag)
        while(line = fd.gets())
          lcount += 1
          break if(line =~ /^####\s+#{tag}$/)
        end
      else
        tag = ""
      end
      while(line = fd.gets())
        break if(line =~ /^####.*/)
        prog << line
      end
    }
    if(prog.size == 0)
      raise "Script \"#{tag}\"not found in #{file}."
      exit
    else
      text = "A"+prog.gsub(/(<\%)(.*?)(\%>)/m) {
        code = $2
        case code
        when /^=(.*)$/
          text = "\x1bC#{$1}\x1bA"
        else
          text = "\x1bB"+code+"\x1bA"
        end
        text
      }
      js = false
      text.split(/\x1b/).each {|part|
        case part
        when /^A(.*)$/m
          code = $1
          code.each_line {|line|
            cr = "print"
            cr = "puts" if(line =~ /\n/)
            line.chomp!
            line.strip!
            if(line.size > 0)
              if(line =~ /^<.*>$/)
                rcode << "Rodeo.#{cr}(\""+line.gsub(/\"/,"\\\"").gsub(/\'/,"\\\\'")+"\")"
                js = true  if(line =~ /<script/i)
                js = false if(line =~ /<\/script/i)
              else
                if(js)
                  rcode << "Rodeo.#{cr}(\""+line.gsub(/\"/,"\\\"").gsub(/\'/,"\\\\'")+"\")"
                else
                  rcode << "Rodeo.#{cr}(\""+line.gsub(/\"/,"\\\"").gsub(/\'/,"\\\\'")+"\")"
                end
              end
              if(cr == "puts")
                rcode << "\n"
              else
                rcode << "; "
              end
            end
          }
        when /^B(.*)$/m
          code = $1
          code.each_line {|line|
            if(line !~ /^\s*$/)
              rcode << line
            end
            rcode << "\n"
          }
        when /^C(.*)$/m
          code = $1
          rcode << "Rodeo.print(#{code})\n"
        end
      }
#      begin
        rcode.gsub!(/^\s+/,"")
        eval(rcode.untaint,bind,file,lcount)
=begin
      rescue Exception
        eline = nil
        error = "<html>\r\n<body>\r\n#{CGI::escapeHTML($!.message)}\r\n<hr><pre>"
        $@.each {|ent|
          eline = ent.split(/:/)[1].to_i+1 if(eline == nil)
          error << ent+"\n"
        }
        error << "</pre><hr><pre>"
        lno = 1
        rcode.each_line {|line|
          if((lno + lcount) == eline)
            error << "<font color=\"red\">#{format("%4d",lno+lcount)}: #{CGI::escapeHTML(line)}</font>"
          else
            error << "#{format("%4d",lno+lcount)}: #{CGI::escapeHTML(line)}"
          end
          lno += 1
        }
        error << "</pre><hr></body>\r\n</html>"
        @cgidata.out({'type'=>"text/html",'charset'=>"utf-8"}) { error }
        exit
      end
=end
    end
  end
  # HTML出力
  def output(hash = {})
    hash['type'] = "text/html" unless(hash['type'])
    hash['charset'] = @kcode unless(hash['charset'])
    hash['charset'] = "Shift_JIS" if(@kcode == "KTAI")
#    hash['pragma'] = "no-cache"
#    hash['Cache-Control'] = "no-cache"
#    hash['Expires'] = "Thu, 01 Dec 1994 16:00:00 GMT"
#    hash['Connection'] = "close"
    hash['cookie'] = @cookie if(@cookie)
    html = @@outbuf.sub(/^\s+/,"")
    if(@kcode == "KTAI")
      html.gsub!(/<script.*?\/script>/m,"")
    end
    @cgidata.out(hash) { @oc.iconv(html) }
    Rodeo.clear()
  end
  # Location出力
  def location(arg,cookie=nil)
    if(cookie)
      @cgidata.out({'type'=>"text/html",'Location'=>arg,'cookie'=>cookie}) { "" }
    else
      @cgidata.out({'type'=>"text/html",'Location'=>arg}) { "" }
    end
  end
  # 有効なCGIか？
  def html_valid?(name)
    @cgi[name] != nil
  end
  # bool型の評価
  def html_bool(name)
    case @cgi[name]
    when "true" then true
    when "false" then false
    else
      nil
    end
  end
  # 文字列のescape処理
  def escape(str)
    if(str)
      str = str.to_s
#      str.force_encoding(__ENCODING__) if(RUBY_VERSION >= "1.9.0")
      CGI::escapeHTML(str)
    else
      nil
    end
  end
  # name="value"の生成
  def html_optstring(opt)
    opts = ""
    opt.each {|k,v|
      if(v == nil)
        opts << " #{k}"
      else
        opts << " #{k}=\"#{v}\""
      end
    }
    opts
  end
  # <form></form>
  def html_form(act=nil,opt={})
    acts = meth = ""
    acts = %Q(action="#{act}") if(act)
    meth = %Q(method="post") unless(opt['method'])
    opt['style'] = "margin: 0px"
    Rodeo.print(%Q(<form #{acts} #{meth} #{html_optstring(opt)}>))
    yield() if(block_given?)
    html_hidden("table",@table)
    html_hidden("session",@session)
    html_hidden("where",@where)
    html_hidden("sort",@sort)
    html_hidden("sort_dir",@sort_dir)
    html_hidden("offset",@offset)
    html_hidden("roid",@roid)
    html_hidden("field",@field)
    html_hidden("elams",@field)
    Rodeo.puts(%Q(</form>))
  end
  # <input submit>
  def html_submit(name,value,opt={})
    if(opt['disabled'])
      opt['style'] = "color:gray;"
      Rodeo.puts(%Q(<input type="submit" id="#{name}" name="#{name}" value="#{escape(value)}" #{html_optstring(opt)}>))
    else
      Rodeo.puts(%Q(<input type="submit" id="#{name}" name="#{name}" value="#{escape(value)}" #{html_optstring(opt)}>))
    end
  end
  # <input hidden>
  def html_hidden(name,value,opt={})
    Rodeo.puts(%Q(<input type="hidden" id="#{name}" name="#{name}" value="#{escape(value)}" #{html_optstring(opt)}>))
  end
  # <input text>
  def html_text(name,value,opt={})
    if(opt['disabled'])
      Rodeo.print(%Q(<input type="text" id="#{name}" name="#{name}" value="#{escape(value)}" #{html_optstring(opt)}>))
    else
      Rodeo.print(%Q(<input type="text" id="#{name}" name="#{name}" value="#{escape(value)}" #{html_optstring(opt)}>))
    end
  end
  # <input number>
  def html_number(name,value,opt={})
    opt['style'] = "text-align: right"
    Rodeo.print(%Q(<input type="text" id="#{name}" name="#{name}" value="#{escape(value)}" #{html_optstring(opt)}>))
  end
  # <input password>
  def html_password(name,value,opt={})
    Rodeo.print(%Q(<input type="password" id="#{name}" name="#{name}" value="#{escape(value)}" #{html_optstring(opt)}>))
  end
  # <input file>
  def html_file(name,value,opt={})
    Rodeo.print(%Q(<input type="file" id="#{name}" name="#{name}" value="#{escape(value)}" #{html_optstring(opt)}>))
  end
  # <textarea></textarea>
  def html_textarea(name,value,col,row,opt={})
    cols = nil
    cols = " cols=#{col}" if(col.to_i > 0)
    rows = nil
    rows = " rows=#{row}" if(row.to_i > 0)
    Rodeo.puts(%Q(<textarea id="#{name}" name="#{name}"#{cols}#{rows} #{html_optstring(opt)}>))
    Rodeo.puts(%Q(#{escape(value)}</textarea>))
  end
#  def html_textarea(name,value,col,row,opt={})
#    Rodeo.print(%Q(<textarea id="#{name}" name="#{name}" cols="#{col}" rows="#{row}" #{html_optstring(opt)}>#{escape(value)}))
#    Rodeo.puts(%Q(</textarea>))
#  end
  def html_textarea2(name,value,col,row,opt={})
    Rodeo.print(%Q(<textarea id="#{name}" name="#{name}" cols="#{col}" rows="#{row}" #{html_optstring(opt)}>#{value}))
    Rodeo.puts(%Q(</textarea>))
  end
  # <input checkbox>
  def html_checkbox(name,label,value=false,opt={})
    checked = ((value) ? "checked" : "")
    Rodeo.print(%Q(<input type="checkbox" id="#{name}" name="#{name}" value="true" #{checked} #{html_optstring(opt)}>#{label}))
  end
  # <select></select>
  def html_select(name,value,opt={})
    if(opt['disabled'])
      opt['style'] = "color:gray;"
      Rodeo.puts(%Q(<select id="#{name}" name="#{name}" #{html_optstring(opt)}>))
      @select_value = value
      @select_type = "option"
      yield()
      Rodeo.puts(%Q(</select>))
    else
      Rodeo.puts(%Q(<select id="#{name}" name="#{name}" #{html_optstring(opt)}>))
      @select_value = value
      @select_type = "option"
      yield()
      Rodeo.puts(%Q(</select>))
    end
  end
  # <input radio>
  def html_radio(name,value)
    @select_name = name
    @select_value = value
    @select_type = "radio"
    yield()
  end
  # <input checkbox>
  def html_check(name,value=[])
    @select_name = name
    @select_value = value
    @select_type = "check"
    yield()
  end
  # <optgroup></optgroup>
  def html_optgroup(label,opt={})
    Rodeo.puts(%Q(<optgroup label="#{label}" #{html_optstring(opt)}>))
    yield()
    Rodeo.puts(%Q(</optgroup>))
  end
  # <option></option>
  def html_item(value,label,opt={})
#    label.force_encoding(__ENCODING__) if(RUBY_VERSION >= "1.9.0")
    case @select_type
    when "option"
      if(@select_value == value)
        Rodeo.puts(%Q(<option value="#{escape(value)}" #{html_optstring(opt)} selected>#{label}</option>))
      else
        Rodeo.puts(%Q(<option value="#{escape(value)}" #{html_optstring(opt)}>#{label}</option>))
      end
    when "radio"
      if(@select_value == value)
        Rodeo.puts(%Q(<input type="radio" id="#{@select_name}" name="#{@select_name}" value="#{value}" #{html_optstring(opt)} checked>#{label}))
      else
        Rodeo.puts(%Q(<input type="radio" id="#{@select_name}" name="#{@select_name}" value="#{value}" #{html_optstring(opt)}>#{label}))
      end
    when "check"
      if(@select_value.include?(value))
        checked = "checked"
      else
        checked = ""
      end
      Rodeo.puts(%Q(<input type="checkbox" id="#{@select_name}.#{value}" name="#{@select_name}.#{value}" value="#{value}" #{checked} #{html_optstring(opt)}>#{label}))
    end
  end
  # <input button>
  def html_button(name,value,opt={})
    Rodeo.print(%Q(<input type="button" id="#{name}" name="#{name}" value="#{value}" #{html_optstring(opt)}>))
  end
  # ツールチップ
  def html_tooltip(label,text=nil,strong=nil)
    if(text and text.size > 0)
      Rodeo.puts  %Q(<script type="text/javascript">)
      Rodeo.puts  %Q(var tips_#{@tips} = "#{text.gsub(/\n/,"<br>").gsub(/\"/,"\\\"")}";)
      Rodeo.puts  %Q(</script>)
      if(strong)
        Rodeo.print %Q(#{label}#{strong})
      else
        Rodeo.print %Q(#{label})
      end
      Rodeo.print %Q(<span class="help" onMouseOver="return overlib(tips_#{@tips},CAPTION,'#{label}')" onMouseOut="return nd()">)
      Rodeo.puts  %Q(<tt>[?]</tt></span>)
      @tips += 1
    else
      Rodeo.puts %Q(#{label})
    end
  end
  # コマンドリンク
  def html_imglink(url,icon=nil,label="",title=nil,sw=true,opt={})
    if(icon)
      if(sw)
        ii = %Q(<img class="icon" src="image/enable/#{icon}">)
      else
        ii = %Q(<img class="icon" src="image/disable/#{icon}">)
      end
    else
      ii = ""
    end
    if(sw)
      if(title)
#        Rodeo.print(%Q(<a href="#{url}" title="#{title}">#{ii}<font size="-1">#{label}</font></a>))
        Rodeo.print(%Q(<a href="#{url}" title="#{title}" #{html_optstring(opt)}>#{ii}#{label}</a>))
      else
#        Rodeo.print(%Q(<a href="#{url}">#{ii}<font size="-1">#{label}</font></a>))
        Rodeo.print(%Q(<a href="#{url}" #{html_optstring(opt)}>#{ii}#{label}</a>))
      end
    else
#      Rodeo.print(%Q(#{ii}<font color="gray" size="-1">#{label}</font>))
      Rodeo.print(%Q(#{ii}<font color="gray">#{label}</font>))
    end
  end
end


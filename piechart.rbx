#!/usr/bin/ruby -Ku

load "#{File.dirname(__FILE__)}/lib/rodeo.rb"

rodeo = Rodeo.new(__FILE__)
table = rodeo.table_new(rodeo.table)
case rodeo['action']
when "app_exec"     then table[rodeo.field].action_app_exec()
when "app_settings" then table[rodeo.field].action_app_settings()
when "app_data"     then table[rodeo.field].action_app_data()
end


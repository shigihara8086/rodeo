#! /usr/bin/ruby -Ku

require 'cgi'
require 'calendar'

cgi = CGI.new()

year = cgi['year'].to_i || 0
month = cgi['month'].to_i || 0

text = ""
cal = TCalendar.new(year,month)
last_wday = 0

prev_year,prev_month = ((cal.month - 1) <  1) ? [cal.year - 1,cal.month + 12] : [cal.year, cal.month - 1]
next_year,next_month = ((cal.month + 1) > 12) ? [cal.year + 1,cal.month - 11] : [cal.year, cal.month + 1]

text << %Q(<html><table border="1">\n)
text << %Q(<tr>)
text << %Q(<td><a href="?year=#{prev_year}&month=#{prev_month}">≪</a></td>)
text << %Q(<td colspan="5">#{cal.year}年#{cal.month}月</td>)
text << %Q(<td><a href="?year=#{next_year}&month=#{next_month}">≫</a></td>)
text << %Q(</tr>\n)
text << %Q(<tr><td>日</td><td>月</td><td>火</td><td>水</td><td>木</td><td>金</td><td>土</td></tr>\n)
text << %Q(<tr>)
1.upto(31) {|d|
  wday = cal.wday(d)
  break unless wday
  last_wday = wday
  status = cal.status(d)
  if(d == 1)
    wday.times {|i|
      text << %Q(<td><br></td>)
    }
  elsif(wday == 0)
    text << %Q(</tr>\n<tr>)
  end
  case status
  when "weekend"
    text << %Q(<td style="text-align:right; color:red;">#{format("%2d",d)}</td>)
  when "holiday"
    text << %Q(<td style="text-align:right; color:black;"><a href="javascript:void(0);" title="#{cal.holiday_name[d]||'振替休日'}">#{format("%2d",d)}</a></td>)
  when "workday"
    text << %Q(<td style="text-align:right; color:black;">#{format("%2d",d)}</td>)
  end
}
(6 - last_wday).times {|i|
  text << "<td><br></td>"
}
text << %Q(</tr>\n)
text << %Q(</table><html>)
cgi.out() { text }


#!/usr/bin/ruby -Ku

load "#{File.dirname(__FILE__)}/lib/rodeo.rb"

rodeo = Rodeo.new(__FILE__,nil,false)
su = Iconv.new("UTF8","CP932")
rodeo.sql.exec(%Q(delete from "zipcode"))
CSV.open("./ken_all.csv","r") {|row|
  sql = %Q[insert into "zipcode" ("zipcode","pref","city","area") values (]
  row.each_index {|i|
    case i
    when 2 then sql << su.iconv("'#{row[i].data}',")
    when 6 then sql << su.iconv("'#{row[i].data}',")
    when 7 then sql << su.iconv("'#{row[i].data}',")
    when 8
      data = su.iconv(row[i].data)
      if(data =~ /^以下に掲載がない場合$/)
        sql << "NULL)"
      else
        sql << "'#{data}')"
      end
    end
  }
  rodeo.sql.exec(sql)
}

